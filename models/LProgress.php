<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_progress".
 *
 * @property string $progress_id
 * @property string|null $progress_name
 * @property string|null $is_active
 */
class LProgress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_progress';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['progress_id'], 'required'],
            [['is_active'], 'string'],
            [['progress_id'], 'string', 'max' => 2],
            [['progress_name'], 'string', 'max' => 255],
            [['progress_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'progress_id' => 'Progress ID',
            'progress_name' => 'Progress Name',
            'is_active' => 'Is Active',
        ];
    }
}
