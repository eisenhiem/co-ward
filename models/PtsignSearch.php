<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ptsign;

/**
 * PtsignSearch represents the model behind the search form of `app\models\Ptsign`.
 */
class PtsignSearch extends Ptsign
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'wbc', 'hct', 'platelets'], 'integer'],
            [['sign', 'other', 'cxr_date', 'swap_date', 'covid', 'covid_test'], 'safe'],
            [['pmn', 'lym', 'm', 'eo', 'atyp_lym', 'hb'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ptsign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
            'wbc' => $this->wbc,
            'pmn' => $this->pmn,
            'lym' => $this->lym,
            'm' => $this->m,
            'eo' => $this->eo,
            'atyp_lym' => $this->atyp_lym,
            'hct' => $this->hct,
            'platelets' => $this->platelets,
            'cxr_date' => $this->cxr_date,
            'swap_date' => $this->swap_date,
            'hb' => $this->hb,
        ]);

        $query->andFilterWhere(['like', 'sign', $this->sign])
            ->andFilterWhere(['like', 'other', $this->other])
            ->andFilterWhere(['like', 'covid_test', $this->covid_test])
            ->andFilterWhere(['like', 'covid', $this->covid]);

        return $dataProvider;
    }
}
