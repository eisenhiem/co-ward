<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "screen".
 *
 * @property int $an
 * @property string|null $q1
 * @property string|null $q2
 * @property string|null $q3
 * @property string|null $q4
 * @property string|null $q5
 * @property string|null $q6
 * @property string|null $q61
 * @property string|null $q62
 * @property string|null $q63
 * @property string|null $q64
 * @property string|null $q65
 * @property string|null $q66
 * @property string|null $q7
 * @property string|null $q8
 * @property string|null $q9
 * @property string|null $q10
 * @property string|null $q11
 * @property string|null $q12
 * @property string|null $q13
 * @property string|null $q14
 * @property string|null $q15
 * @property string|null $q16
 * @property string|null $q17
 * @property string|null $screen_date
 * @property string|null $result
 * @property int|null $user_id
 */
class Screen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'screen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'user_id'], 'integer'],
            [['screen_date'],'safe'],
            [['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q61', 'q62', 'q63', 'q64', 'q65', 'q66', 'q7', 'q8', 'q9', 'q10', 'q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17', 'result'], 'string'],
            [['an'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'screen_date' => 'วันที่',
            'q1' => '1. ผู้ติดเชื้อที่ไม่มีอาการ',
            'q2' => '2. อายุน้อยกว่า 60 ปี',
            'q3' => '3. สุขภาพร่างกายแข็งแรง',
            'q4' => '4. อยู่คนเดียวหรือที่พักอาศัยสามารถแยกห้องเพื่ออยู่คนเดียวได้',
            'q5' => '5. ไม่มีภาวะอ้วน (bmi >30 หรือน้ำหนักเกิน 90 kg)',
            'q6' => '6. ไม่มีโรคร่วม',
            'q61' => '6.1 ไม่เป็นโรคปอดอุดกลั้นเรื้อรัง (COPD)',
            'q62' => '6.2 ไม่เป็นโรคไตเรื้อรัง (CKD Stage 3,4,5)',
            'q63' => '6.3 ไม่เป็นโรคหัวใจและหลอดเลือด',
            'q64' => '6.4 ไม่เป็นโรคหลอดเลือดสมอง',
            'q65' => '6.5 ไม่เป็นโรคเบาหวานที่ความคุมไม่ได้',
            'q66' => '6.6 โรคอื่นๆตามดุลพินิจของแพทย์',
            'q7' => '7. ยินยอมกักตัวในที่พักตนเอง',
            'q8' => '1. มีผู้จัดหาอาหารและของใช้จำเป็นให้ได้',
            'q9' => '2. มีห้องส่วนตัว(กรณี หอพัก/คอนโด ควรมีห้องน้ำส่วนตัว)',
            'q10' => '3. มีผู้อยู่อาศัยร่วมสามารถปฏิบัติตามคำแนะนำเรื่องสุขอนามัยและแยกจากผู้ป่วยได้',
            'q11' => '4. ติดต่อกับโรงพยาบาลและเดินทางมาโรงพยาบาลได้สะดวก',
            'q12' => '1. ลงทะเบียนผู้ติดเชื้อ COVID-19',
            'q13' => '2. มีภาพรังสีทรวงอก (chest X-Ray):ปอดปกติ',
            'q14' => '3. มีปรอทวัดไข้ และ pulse oximeter ให้กับผู้ติดเชื้อ',
            'q15' => '4. มีระบบ/ช่องทางสื่อสารกับผู้ติดเชื้อ',
            'q16' => '5. มีช่องทางสื่อสารในกรณีฉุกเฉิน',
            'q17' => '6. มีระบบรับผู้ติดเชื้อส่งโรงพยาบาลกรณีฉุกเฉิน',
            'result' => 'ผลการประเมิน',
            'user_id' => 'ผู้บันทึก',
        ];
    }
}
