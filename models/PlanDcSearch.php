<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlanDc;

/**
 * PlanDcSearch represents the model behind the search form of `app\models\PlanDc`.
 */
class PlanDcSearch extends PlanDc
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'integer'],
            [['dc_date', 'diagnosis', 'address', 'phone_number', 'contact_name', 'concution', 'dctype'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlanDc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
            'dc_date' => $this->dc_date,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'diagnosis', $this->diagnosis])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'concution', $this->concution])
            ->andFilterWhere(['like', 'dctype', $this->dctype]);

        return $dataProvider;
    }
}
