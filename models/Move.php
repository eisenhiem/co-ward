<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "move".
 *
 * @property int|null $an
 * @property int|null $ward_id
 * @property int|null $to_ward_id
 * @property string|null $move_date
 */
class Move extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'move';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'ward_id', 'to_ward_id'], 'integer'],
            [['move_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'ward_id' => 'Ward ID',
            'to_ward_id' => 'To Ward ID',
            'move_date' => 'Move Date',
        ];
    }
}
