<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hospitals".
 *
 * @property string $hospcde
 * @property string|null $hospname
 */
class Hospitals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hospitals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospcode'], 'required'],
            [['hospcode'], 'string', 'max' => 5],
            [['hospname'], 'string', 'max' => 255],
            [['hospcode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hospcode' => 'Hospcde',
            'hospname' => 'Hospname',
        ];
    }
}
