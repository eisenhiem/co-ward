<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Notes;

/**
 * NotesSearch represents the model behind the search form of `app\models\Notes`.
 */
class NotesSearch extends Notes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['note_id', 'an', 'user_id'], 'integer'],
            [['nurse_datetime', 'activity', 'evaluate','activity_select', 'evaluate_select'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'note_id' => $this->note_id,
            'an' => $this->an,
            'nurse_datetime' => $this->nurse_datetime,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'activity_select', $this->activity_select])
            ->andFilterWhere(['like', 'evaluate_select', $this->evaluate_select])
            ->andFilterWhere(['like', 'activity', $this->activity])
            ->andFilterWhere(['like', 'evaluate', $this->evaluate]);

        return $dataProvider;
    }
}
