<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "refers".
 *
 * @property int $refer_id
 * @property int $an
 * @property string|null $refer_hosp
 * @property string|null $refer_cause
 * @property int|null $refer_by
 * @property string|null $refer_diag_icd10
 * @property string|null $refer_daig_text
 * @property string|null $refer_datetime
 */
class Refers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'refers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refer_id', 'an'], 'required'],
            [['refer_id', 'an', 'refer_by'], 'integer'],
            [['refer_datetime'], 'safe'],
            [['refer_cause'], 'string', 'max' => 2],
            [['refer_hosp'], 'string', 'max' => 5],
            [['refer_diag_icd10'], 'string', 'max' => 7],
            [['refer_daig_text'], 'string', 'max' => 255],
            [['refer_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'refer_id' => 'Refer ID',
            'an' => 'An',
            'refer_hosp' => 'รหัสหน่วยบริการส่งต่อ',
            'refer_cause' => 'สาเหตุการส่งต่อ',
            'refer_by' => 'ส่งต่อโดย',
            'refer_diag_icd10' => 'Diagnosis',
            'refer_daig_text' => 'Diagnosis เพิ่มเติม',
            'refer_datetime' => 'วันเวลาส่งต่อ',
        ];
    }

    public static function itemsAlias($key){
        $items = [
          'cause'=> ArrayHelper::map(LCause::find()->all(), 'refer_cause_id', 'refer_cause_name'),
          'refer_by' => ArrayHelper::map(LReferBy::find()->all(), 'refer_by_id', 'refer_by_name'),
          'hosp' => ArrayHelper::map(Hospitals::find()->all(), 'hospcode', 'hospname'),
          'diag'=> [
              'J128' => 'Pneumonia with COVID19',
              'J208' => 'Acute Bronchitis with COVID19',
              'J068' => 'URI with COVID19',
              'R508' => 'Fever with COVID19',
              'J960' => 'Acute Resporatory Failure',
              'I460' => 'Cardiat Arrest',
              'U071' => 'COVID19 Infection'
          ],
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemCause()
    {
      return self::itemsAlias('cause');
    }

    public function getCause()
    {
        return $this->hasOne(LCause::className(), ['refer_cause_id' => 'refer_cause']);
    }

    public function getItemHosp()
    {
      return self::itemsAlias('hosp');
    }

    public function getHospital()
    {
        return $this->hasOne(Hospitals::className(), ['refer_hosp' => 'hospcode']);
    }

    public function getItemBy()
    {
      return self::itemsAlias('refer_by');
    }

    public function getReferby()
    {
        return $this->hasOne(LReferBy::className(), ['refer_by_id' => 'refer_by']);
    }

    public function getItemDiag()
    {
      return self::itemsAlias('diag');
    }

    public function getDiagName(){
        return  ArrayHelper::getValue($this->getItemDiag(),$this->refer_diag_icd10);
    }

    public function getAdmission()
    {
        return $this->hasOne(Admission::className(), ['an' => 'an']);
    }

}
