<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Admission;

/**
 * PatientSearch represents the model behind the search form of `app\models\Admission`.
 */
class PatientSearch extends Admission
{
    public $param;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'hn', 'age', 'ward_id'], 'integer'],
            [['pname', 'fname', 'lname', 'gender', 'dob', 'cid', 'address', 'contact_number', 'contact_name', 'phistory', 'med_reconcile', 'cc', 'covid_register', 'allergy', 'is_admit', 'bed', 'insure_name', 'admission_date', 'isolation_date', 'ventilated', 'ventilated_other', 'sick_date', 'asymptomatic', 'insure_other', 'passport','other_comobit','param'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Admission::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['or',
            ['an' => $this->param],
            ['hn' => $this->param],['like', 'fname', $this->param],['like', 'lname', $this->param]
        ]);

        return $dataProvider;
    }
}
