<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "orders".
 *
 * @property int $order_id
 * @property int $an
 * @property string $order_datetime
 * @property string|null $progress_note
 * @property string|null $order_oneday
 * @property string|null $order_continue
 * @property string|null $comment
 * @property int $user_id
 * @property int $ward_id
 * @property array|null $medication
 * @property string|null $record_i_o
 * @property string|null $record_vitalsign
 * @property string|null $food
 * @property string|null $is_lab
 * @property string|null $is_xray
 * @property array|null $progress
 * @property array|null $oneday_list
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'medication',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'medication',
                ],
                'value' => function ($event) {                    
                    return $this->medication ? implode(',', $this->medication):$this->medication;
                },
            ],
            [
              'class' => AttributeBehavior::className(),
              'attributes' => [
                  ActiveRecord::EVENT_BEFORE_INSERT => 'progress',
                  ActiveRecord::EVENT_BEFORE_UPDATE => 'progress',
              ],
              'value' => function ($event) {                    
                  return $this->progress ? implode(',', $this->progress):$this->progress;
              },
            ],
            [
              'class' => AttributeBehavior::className(),
              'attributes' => [
                  ActiveRecord::EVENT_BEFORE_INSERT => 'oneday_list',
                  ActiveRecord::EVENT_BEFORE_UPDATE => 'oneday_list',
              ],
              'value' => function ($event) {                    
                return $this->oneday_list ? implode(',', $this->oneday_list):$this->oneday_list;
              },
            ],
    ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'user_id'], 'required'],
            [['an', 'user_id','ward_id'], 'integer'],
            [['order_datetime','medication','progress','oneday_list'], 'safe'],
            [['progress_note', 'order_oneday', 'order_continue', 'comment'], 'string'],
            [['record_i_o', 'food','record_vitalsign','is_lab','is_xray',], 'string', 'max' => 1],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'an' => 'AN',
            'ward_id' => 'ward_id',
            'order_datetime' => 'วันเวลาที่บันทึก Order',
            'progress_note' => 'Progress Note เพิ่มเติม',
            'progress' => 'Progress Note',
            'oneday_list' => 'Order Oneday',
            'order_oneday' => 'Order One Day เพิ่ม',
            'order_continue' => 'Order เพิ่มเติม',
            'comment' => 'หมายเหตุ',
            'user_id' => 'ผู้บันทึก',
            'medication' => 'Medication',
            'record_i_o' => 'Record I/O',
            'record_vitalsign' => 'Record V/S',
            'food' => 'อาหาร',
            'is_lab' => 'สั่ง Lab',
            'is_xray' => 'สั่ง X-Ray',
        ];
    }

    public function medToArray()
    {
      return $this->medication = explode(',', $this->medication);
    }

    public function progressToArray()
    {
      return $this->progress = explode(',', $this->progress);
    }

    public function orderToArray()
    {
      return $this->oneday_list = explode(',', $this->oneday_list);
    }

    public function foodToArray()
    {
      return $this->food = explode(',', $this->food);
    }
    
    public static function itemsAlias($key){
        $items = [
          'med'=> ArrayHelper::map(StandingOrder::find()->all(), 'med_id', 'medicine'),
          'med_use' => ArrayHelper::map(StandingOrder::find()->where(['is_active' => '1'])->all(), 'med_id', 'medicine'),
          'prog'=> ArrayHelper::map(LProgress::find()->all(), 'progress_id', 'progress_name'),
          'prog_use' => ArrayHelper::map(LProgress::find()->where(['is_active' => '1'])->all(), 'progress_id', 'progress_name'),
          'oned'=> ArrayHelper::map(OrderOneday::find()->all(), 'order_id', 'order_name'),
          'oned_use' => ArrayHelper::map(OrderOneday::find()->where(['is_active' => '1'])->all(), 'order_id', 'order_name'),
          'group' => ArrayHelper::map(LOrderGroup::find()->all(), 'order_group_id', 'order_group_name'),
          'food' => [
            '1' => 'Regular Diet',
            '2' => 'Soft Diet',
          ]
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }
    
    public function getItemMed()
    {
      return self::itemsAlias('med');
    }

    public function getOrderMed()
    {
      return self::itemsAlias('med_use');
    }

    public function getItemProg()
    {
      return self::itemsAlias('prog');
    }

    public function getOrderProg()
    {
      return self::itemsAlias('prog_use');
    }

    public function getItemOrder()
    {
      return self::itemsAlias('oned');
    }

    public function getOrderOneday()
    {
      return self::itemsAlias('oned_use');
    }

    public function getItemGroup()
    {
      return self::itemsAlias('group');
    }

    public function getItemFood()
    {
      return self::itemsAlias('food');
    }

  public function getMedName(){
        $meds = $this->getItemMed();
        $medSelected = explode(',', $this->medication);
        $medSelectedName = [];
        foreach ($meds as $key => $medName) {
          foreach ($medSelected as $medKey) {
            if($key === $medKey){
              $medSelectedName[] = $medName;
            }
          }
        }   
        return implode('<br> &emsp;', $medSelectedName);
    }

  public function getProgName(){
      $progs = $this->getItemProg();
      $progSelected = explode(',', $this->progress);
      $progSelectedName = [];
      foreach ($progs as $key => $progName) {
        foreach ($progSelected as $progKey) {
          if($key === $progKey){
            $progSelectedName[] = $progName;
          }
        }
      }   
      return implode('<br> &emsp;', $progSelectedName);
  }

  public function getOrderName(){
    $orders = $this->getItemOrder();
    $orderSelected = explode(',', $this->oneday_list);
    $orderSelectedName = [];
    foreach ($orders as $key => $orderName) {
      foreach ($orderSelected as $orderKey) {
        if($key === $orderKey){
          $orderSelectedName[] = $orderName;
        }
      }
    }   
    return implode('<br> &emsp;', $orderSelectedName);
  }

  public function getOnedayName(){
    $orders = $this->getItemOrder();
    $orderSelected = explode(',', $this->oneday_list);
    $orderSelectedName = [];
    foreach ($orders as $key => $orderName) {
      foreach ($orderSelected as $orderKey) {
        if($key === $orderKey){
          $orderSelectedName[] = $orderName;
        }
      }
    }   
    return implode(' , ', $orderSelectedName);
  }

  /**
     * Gets query for [[Profile]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    public function getAdmission()
    {
        return $this->hasOne(Admission::className(), ['an' => 'an']);
    }

    public function getWard()
    {
        return $this->hasOne(Ward::className(), ['ward_id' => 'ward_id']);
    }

    public function getPatientName(){
      return $this->admission->pname.$this->admission->fname.' '.$this->admission->lname;
    }

}