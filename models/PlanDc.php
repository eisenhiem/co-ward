<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plan_dc".
 *
 * @property int $an
 * @property string|null $dc_date
 * @property string|null $address
 * @property string|null $phone_number
 * @property string|null $contact_name
 * @property string|null $concution
 * @property string|null $dctype
 */
class PlanDc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plan_dc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an'], 'integer'],
            [['dc_date'], 'safe'],
            [['concution', 'dctype'], 'string'],
            [['address','diagnosis'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 20],
            [['contact_name'], 'string', 'max' => 100],
            [['an'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'diagnosis' => 'Diagnosis',
            'dc_date' => 'Discharge Date',
            'address' => 'Address',
            'phone_number' => 'Phone Number',
            'contact_name' => 'Contact Name',
            'concution' => 'Concution',
            'dctype' => 'Dctype',
        ];
    }

    
    public static function itemsAlias($key){
        $items = [
            'dc' => [
              '1' => 'Home',
              '2' => 'Death',
              '3' => 'Refer',
              '4' => 'Escape',
            ],
            'diag'=> [
                'J128' => 'Pneumonia with COVID19',
                'J208' => 'Acute Bronchitis with COVID19',
                'J068' => 'URI with COVID19',
                'R508' => 'Fever with COVID19',
                'J960' => 'Acute Resporatory Failure',
                'I460' => 'Cardiat Arrest',
                'U071' => 'COVID19 Infection'
            ],

        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemDc()
    {
      return self::itemsAlias('dc');
    }

    public function getDcName(){
        return ArrayHelper::getValue($this->getItemDc(),$this->dctype);
    }

    public function getItemDiag()
    {
      return self::itemsAlias('diag');
    }

    public function getDiagName(){
        return  ArrayHelper::getValue($this->getItemDiag(),$this->diagnosis);
    }

}
