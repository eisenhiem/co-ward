<?php

namespace app\models;

use Yii;
use app\models\Ward;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rep_pt".
 *
 * @property int|null $ward_id
 * @property string|null $ward_name
 * @property string|null $bed
 * @property int $an
 * @property int|null $hn
 * @property string|null $ptname
 * @property int|null $age
 * @property string|null $address
 * @property string $contact_number
 * @property int|null $days
 */
class RepPt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rep_pt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ward_id', 'an', 'hn', 'age', 'days'], 'integer'],
            [['an', 'contact_number'], 'required'],
            [['address'], 'string'],
            [['ward_name'], 'string', 'max' => 255],
            [['bed', 'contact_number'], 'string', 'max' => 10],
            [['ptname'], 'string', 'max' => 255],
        ];
    }

    public static function primaryKey()
    {
        return ['an'];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ward_id' => 'Ward ID',
            'ward_name' => 'ชื่อ Ward',
            'bed' => 'เตียง',
            'an' => 'AN',
            'hn' => 'HN',
            'ptname' => 'ชื่อ-สกุล',
            'age' => 'อายุ',
            'address' => 'ที่อยู่',
            'contact_number' => 'เบอร์โทร',
            'days' => 'รักษา Day',
        ];
    }

    public static function itemsAlias($key){
        $items = [
          'ward'=> ArrayHelper::map(Ward::find()->all(), 'ward_id', 'ward_name'),
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemWard()
    {
      return self::itemsAlias('ward');
    }

}
