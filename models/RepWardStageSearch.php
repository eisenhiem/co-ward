<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RepWardStage;

/**
 * RepWardStageSearch represents the model behind the search form of `app\models\RepWardStage`.
 */
class RepWardStageSearch extends RepWardStage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ward_type_id', 'stage_1', 'stage_2', 'stage_3_1', 'stage_3_2', 'stage_4_1', 'stage_4_2', 'stage_4_3'], 'integer'],
            [['ward_type_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepWardStage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ward_type_id' => $this->ward_type_id,
            'stage_1' => $this->stage_1,
            'stage_2' => $this->stage_2,
            'stage_3_1' => $this->stage_3_1,
            'stage_3_2' => $this->stage_3_2,
            'stage_4_1' => $this->stage_4_1,
            'stage_4_2' => $this->stage_4_2,
            'stage_4_3' => $this->stage_4_3,
        ]);

        $query->andFilterWhere(['like', 'ward_type_name', $this->ward_type_name]);

        return $dataProvider;
    }
}
