<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_order_group".
 *
 * @property string $order_group_id
 * @property string|null $order_group_name
 */
class LOrderGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_order_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_group_id'], 'required'],
            [['order_group_id'], 'string', 'max' => 5],
            [['order_group_name'], 'string', 'max' => 255],
            [['order_group_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_group_id' => 'Order Group ID',
            'order_group_name' => 'Order Group Name',
        ];
    }
}
