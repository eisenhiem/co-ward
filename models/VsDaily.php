<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vs_daily".
 *
 * @property int $note_id
 * @property string $round_id
 * @property int|null $sbp
 * @property int|null $dbp
 * @property float|null $body_temp
 * @property int|null $o2sat
 * @property int|null $pr
 * @property int|null $rr
 * @property int|null $urine
 * @property int|null $stool
 */
class VsDaily extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vs_daily';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['note_id'], 'required'],
            [['note_id', 'sbp', 'dbp', 'o2sat', 'pr', 'rr','urine','stool'], 'integer'],
            [['round_id'], 'string'],
            [['body_temp'], 'number'],
            [['note_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'note_id' => 'Note ID',
            'round_id' => 'รอบที่วัด',
            'sbp' => 'SBP',
            'dbp' => 'DBP',
            'body_temp' => 'อุณหภูมิ ( ํC)',
            'o2sat' => 'O2 Sat(%)',
            'pr' => 'PR  (/min)',
            'rr' => 'RR (/min)',
            'urine'=> 'Urine (ครั้ง)',
            'stool' => 'Stool (ครั้ง)',
        ];
    }

    public function getRound(){
        return $this->hasOne(Rounds::className(), ['round_id' => 'round_id']);
    }
}
