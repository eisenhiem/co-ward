<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order_oneday".
 *
 * @property string $order_id
 * @property string|null $order_name
 * @property string|null $order_group_id
 * @property string|null $is_active
 */
class OrderOneday extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_oneday';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['is_active','order_group_id'], 'string'],
            [['order_id'], 'string', 'max' => 2],
            [['order_name'], 'string', 'max' => 255],
            [['order_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'order_name' => 'Order Name',
            'is_active' => 'Is Active',
            'order_group_id' => 'Order Group ID',
        ];
    }

    public static function itemsAlias($key){
        $items = [
          'group' => ArrayHelper::map(LOrderGroup::find()->all(), 'order_group_id', 'order_group_name'),
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemGroup()
    {
      return self::itemsAlias('group');
    }

    public function getOrdergroup()
    {
        return $this->hasOne(LOrderGroup::className(), ['order_group_id' => 'order_group_id']);
    }

    public function getGroupName()
    {
        return $this->ordergroup->order_group_name;
    }

}
