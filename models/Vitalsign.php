<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "vitalsign".
 *
 * @property int $an
 * @property float|null $bw
 * @property float|null $height_cm
 * @property float|null $body_temp
 * @property int|null $rr
 * @property int|null $pr
 * @property float|null $sbp
 * @property float|null $dbp
 * @property int|null $o2sat
 * @property array|null $symptom
 * @property string|null $other_symptom
 * @property string|null $pain
 * @property int|null $o2sat_ra
 */
class Vitalsign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vitalsign';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'symptom',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'symptom',
                ],
                'value' => function ($event) {                    
                    return $this->symptom ? implode(',', $this->symptom ):$this->symptom;
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'rr', 'pr', 'o2sat', 'o2sat_ra'], 'integer'],
            [['bw', 'height_cm', 'body_temp', 'sbp', 'dbp'], 'number'],
            [['pain','q1','q2'], 'string'],
            [['symptom'],'safe'],
            [['other_symptom'], 'string', 'max' => 255],
            [['an'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'bw' => 'น้ำหนัก',
            'height_cm' => 'ส่วนสูง',
            'body_temp' => 'อุณหภูมิ',
            'rr' => 'การหายใจ',
            'pr' => 'ชีพจร',
            'sbp' => 'SBP',
            'dbp' => 'DBP',
            'o2sat' => 'O2Sat',
            'o2sat_ra' => 'O2Sat Room Air',
            'symptom' => 'Symptom',
            'other_symptom' => 'อาการอื่น',
            'pain' => 'Pain',
            'q1' => '1.ใน 2 สัปดาห์ที่ผ่านมารวมวันนี้ท่านรู้สึกหดหู่ เศร้าหรือท้อแท้สิ้นหวังหรือไม่',
            'q2' => '2.ใน 2 สัปดาห์ที่ผ่านมารวมวันนี้ท่านรู้สึก เบื่อ ทำอะไรก็ไม่เพลิดเพลินหรือไม่',
        ];
    }

    public static function itemsAlias($key){
        $items = [
          'symp'=> ArrayHelper::map(LSymptom::find()->all(), 'symptom_id', 'symptom_name'),
          'pain' => [
              '1' => 'Muscular',
              '2' => 'Chest',
              '3' => 'Abdominal',
              '4' => 'Joint',
          ],
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function sympToArray()
    {
      return $this->symptom = explode(',', $this->symptom);
    }

    public function getItemSymp()
    {
      return self::itemsAlias('symp');
    }

    public function getItemPain()
    {
      return self::itemsAlias('pain');
    }

    public function getSympName(){
        $symp = $this->getItemSymp();
        $sympSelected = explode(',', $this->symptom);
        $sympSelectedName = [];
        foreach ($symp as $key => $sympName) {
          foreach ($sympSelected as $sympKey) {
            if($key === $sympKey){
              $sympSelectedName[] = $sympName;
            }
          }
        }   
        return implode('<br> &emsp;', $sympSelectedName);
    }
    
    public function getPainName(){
        return ArrayHelper::getValue($this->getItemPain(),$this->pain);
    }

    public function getTwoQResult(){
        $twoq = 'ไม่ได้ประเมิน';
        if($this->q1 == '0' && $this->q2 == '0'){
            $twoq = 'ไม่มีภาวะซึมเศร้า';
        }
        if($this->q1 == '1' || $this->q2 == '1'){
            $twoq = 'มีภาวะซึมเศร้า';
        }
        return $twoq;
    }


}
