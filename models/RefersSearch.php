<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Refers;

/**
 * RefersSearch represents the model behind the search form of `app\models\Refers`.
 */
class RefersSearch extends Refers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refer_id', 'an', 'refer_by'], 'integer'],
            [['refer_hosp', 'refer_cause', 'refer_diag_icd10', 'refer_daig_text', 'refer_datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Refers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'refer_id' => $this->refer_id,
            'an' => $this->an,
            'refer_by' => $this->refer_by,
            'refer_datetime' => $this->refer_datetime,
        ]);

        $query->andFilterWhere(['like', 'refer_hosp', $this->refer_hosp])
            ->andFilterWhere(['like', 'refer_cause', $this->refer_cause])
            ->andFilterWhere(['like', 'refer_diag_icd10', $this->refer_diag_icd10])
            ->andFilterWhere(['like', 'refer_daig_text', $this->refer_daig_text]);

        return $dataProvider;
    }
}
