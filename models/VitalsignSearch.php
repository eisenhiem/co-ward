<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vitalsign;

/**
 * VitalsignSearch represents the model behind the search form of `app\models\Vitalsign`.
 */
class VitalsignSearch extends Vitalsign
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'height_cm', 'rr', 'pr', 'sbp', 'dbp', 'o2sat','o2sat_ra'], 'integer'],
            [['bw', 'body_temp'], 'number'],
            [['symptom', 'other_symptom', 'pain','q1','q2'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vitalsign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
            'bw' => $this->bw,
            'height_cm' => $this->height_cm,
            'body_temp' => $this->body_temp,
            'rr' => $this->rr,
            'pr' => $this->pr,
            'sbp' => $this->sbp,
            'dbp' => $this->dbp,
            'o2sat' => $this->o2sat,
            'o2sat_ra' => $this->o2sat_ra,
        ]);

        $query->andFilterWhere(['like', 'symptom', $this->symptom])
            ->andFilterWhere(['like', 'other_symptom', $this->other_symptom])
            ->andFilterWhere(['like', 'q1', $this->q1])
            ->andFilterWhere(['like', 'q2', $this->q2])
            ->andFilterWhere(['like', 'pain', $this->pain]);

        return $dataProvider;
    }
}
