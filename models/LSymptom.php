<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_symptom".
 *
 * @property int $symptom_id
 * @property string|null $symptom_name
 */
class LSymptom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_symptom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['symptom_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'symptom_id' => 'Symptom ID',
            'symptom_name' => 'Symptom Name',
        ];
    }
}
