<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VsDaily;

/**
 * VsDailySearch represents the model behind the search form of `app\models\VsDaily`.
 */
class VsDailySearch extends VsDaily
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['note_id', 'sbp', 'dbp', 'o2sat', 'pr', 'rr','urine','stool'], 'integer'],
            [['round_id'], 'safe'],
            [['body_temp'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VsDaily::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'note_id' => $this->note_id,
            'sbp' => $this->sbp,
            'dbp' => $this->dbp,
            'body_temp' => $this->body_temp,
            'o2sat' => $this->o2sat,
            'pr' => $this->pr,
            'rr' => $this->rr,
        ]);

        $query->andFilterWhere(['like', 'round_id', $this->round_id]);

        return $dataProvider;
    }
}
