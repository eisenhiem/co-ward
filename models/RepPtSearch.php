<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RepPt;

/**
 * RepPtSearch represents the model behind the search form of `app\models\RepPt`.
 */
class RepPtSearch extends RepPt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ward_id', 'an', 'hn', 'age', 'days'], 'integer'],
            [['ward_name', 'bed', 'ptname', 'address', 'contact_number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepPt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ward_id' => $this->ward_id,
            'an' => $this->an,
            'hn' => $this->hn,
            'age' => $this->age,
            'days' => $this->days,
        ]);

        $query->andFilterWhere(['like', 'ward_name', $this->ward_name])
            ->andFilterWhere(['like', 'bed', $this->bed])
            ->andFilterWhere(['like', 'ptname', $this->ptname])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'contact_number', $this->contact_number]);

        return $dataProvider;
    }
}
