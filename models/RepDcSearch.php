<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RepDc;

/**
 * RepDcSearch represents the model behind the search form of `app\models\RepDc`.
 */
class RepDcSearch extends RepDc
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ward_id', 'day1_6', 'day7', 'day8', 'day9', 'day10', 'day11', 'day12', 'day13', 'day14'], 'integer'],
            [['ward_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepDc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ward_id' => $this->ward_id,
            'day1_6' => $this->day1_6,
            'day7' => $this->day7,
            'day8' => $this->day8,
            'day9' => $this->day9,
            'day10' => $this->day10,
            'day11' => $this->day11,
            'day12' => $this->day12,
            'day13' => $this->day13,
            'day14' => $this->day14,
        ]);

        $query->andFilterWhere(['like', 'ward_name', $this->ward_name]);

        return $dataProvider;
    }
}
