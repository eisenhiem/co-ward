<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "office".
 *
 * @property string $hospcode
 * @property string|null $hospname
 * @property string|null $address
 * @property string|null $phone_number
 * @property string|null $fax_number
 * @property string|null $book_no
 * @property string|null $line_id
 * @property string|null $director_name
 * @property string|null $director_position1
 * @property string|null $director_position2
 * @property string|null $facebook_url
 * @property string|null $zipcode
 */
class Office extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'office';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hospcode'], 'required'],
            [['hospcode', 'zipcode'], 'string', 'max' => 5],
            [['hospname', 'address', 'book_no', 'line_id', 'facebook_url', 'director_name', 'director_position1', 'director_position2'], 'string', 'max' => 255],
            [['phone_number', 'fax_number'], 'string', 'max' => 50],
            [['hospcode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hospcode' => 'รหัสหน่วยบริการ',
            'hospname' => 'ชื่อหน่วยงาน',
            'address' => 'ที่อยู่',
            'book_no' => 'เลขที่หนังสือออก',
            'director_name' => 'ชื่อผู้อำนวยการ',
            'director_position2' => 'ตำแหน่ง 2',
            'director_position1' => 'ตำแหน่ง 1',
            'phone_number' => 'เบอร์โทรติดต่อ',
            'fax_number' => 'เบอร์โทรสาร',
            'line_id' => 'Line ID',
            'facebook_url' => 'Facebook Url',
            'zipcode' => 'รหัสไปรษณีย์',
        ];
    }
}
