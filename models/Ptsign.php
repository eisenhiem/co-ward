<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ptsign".
 *
 * @property int $an
 * @property array|null $sign
 * @property string|null $other
 * @property int|null $wbc
 * @property float|null $pmn
 * @property float|null $lym
 * @property float|null $m
 * @property float|null $eo
 * @property float|null $hb
 * @property float|null $atyp_lym
 * @property int|null $hct
 * @property int|null $platelets
 * @property string|null $cxr_date
 * @property string|null $swap_date
 * @property string|null $covid
 * @property string|null $lab_other
 */
class Ptsign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ptsign';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'sign',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'sign',
                ],
                'value' => function ($event) {                    
                    return $this->sign ? implode(',', $this->sign ):$this->sign;
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'wbc', 'hct', 'platelets'], 'integer'],
            [['pmn', 'lym', 'm', 'eo', 'atyp_lym','hb'], 'number'],
            [['cxr_date', 'swap_date','sign'], 'safe'],
            [['covid','covid_test'], 'string'],
            [['other','lab_other'], 'string', 'max' => 255],
            [['an'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'sign' => 'Sign',
            'other' => 'Other',
            'wbc' => 'WBC',
            'hb' => 'Hb',
            'pmn' => 'PMN(Neutrophil) %',
            'lym' => 'Lym %',
            'm' => 'M %',
            'eo' => 'Eo %',
            'atyp_lym' => 'Atyp Lym %',
            'hct' => 'HCT',
            'lab_other' => 'Lab อื่นๆ',
            'platelets' => 'Platelets',
            'cxr_date' => 'Cxr Date',
            'swap_date' => 'Swap Date',
            'covid' => 'ผลการตรวจ',
            'covid_test' => 'Covid Test',
        ];
    }

    public static function itemsAlias($key){
        $items = [
          'sign'=> ArrayHelper::map(LSign::find()->all(), 'sign_id', 'sign_name'),
          'covid' => [
              '0' => 'Negative',
              '1' => 'Positive',
          ],
          'covid_test' => [
              '1' => 'RT-PCR',
              '2' => 'Rapid Antigen test',
          ]
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemSign()
    {
      return self::itemsAlias('sign');
    }

    public function getItemCovid()
    {
      return self::itemsAlias('covid');
    }

    public function getItemCovidTest()
    {
      return self::itemsAlias('covid_test');
    }

    public function signToArray()
    {
      return $this->sign = explode(',', $this->sign);
    }

    public function getSignName(){
        $sign = $this->getItemSign();
        $signSelected = explode(',', $this->sign);
        $signSelectedName = [];
        foreach ($sign as $key => $signName) {
          foreach ($signSelected as $signKey) {
            if($key === $signKey){
              $signSelectedName[] = $signName;
            }
          }
        }   
        return implode('<br> &emsp;', $signSelectedName);
    }

    public function getResult(){
        return ArrayHelper::getValue($this->getItemCovid(),$this->covid);
    }

    public function getTest(){
        return ArrayHelper::getValue($this->getItemCovidTest(),$this->covid_test);
    }

}
