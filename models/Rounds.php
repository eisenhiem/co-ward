<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rounds".
 *
 * @property string $round_id
 * @property string|null $round_name
 */
class Rounds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rounds';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['round_id'], 'required'],
            [['round_id'], 'string', 'max' => 1],
            [['round_name'], 'string', 'max' => 20],
            [['round_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'round_id' => 'รอบที่',
            'round_name' => 'เวลา',
        ];
    }
}
