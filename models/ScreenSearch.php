<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Screen;

/**
 * ScreenSearch represents the model behind the search form of `app\models\Screen`.
 */
class ScreenSearch extends Screen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'user_id'], 'integer'],
            [['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q61', 'q62', 'q63', 'q64', 'q65', 'q66', 'q7', 'q8', 'q9', 'q10', 'q11', 'q12', 'q13', 'q14', 'q15', 'q16', 'q17', 'screen_date' ,'result'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Screen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'q1', $this->q1])
            ->andFilterWhere(['like', 'q2', $this->q2])
            ->andFilterWhere(['like', 'q3', $this->q3])
            ->andFilterWhere(['like', 'q4', $this->q4])
            ->andFilterWhere(['like', 'q5', $this->q5])
            ->andFilterWhere(['like', 'q6', $this->q6])
            ->andFilterWhere(['like', 'q61', $this->q61])
            ->andFilterWhere(['like', 'q62', $this->q62])
            ->andFilterWhere(['like', 'q63', $this->q63])
            ->andFilterWhere(['like', 'q64', $this->q64])
            ->andFilterWhere(['like', 'q65', $this->q65])
            ->andFilterWhere(['like', 'q66', $this->q66])
            ->andFilterWhere(['like', 'q7', $this->q7])
            ->andFilterWhere(['like', 'q8', $this->q8])
            ->andFilterWhere(['like', 'q9', $this->q9])
            ->andFilterWhere(['like', 'q10', $this->q10])
            ->andFilterWhere(['like', 'q11', $this->q11])
            ->andFilterWhere(['like', 'q12', $this->q12])
            ->andFilterWhere(['like', 'q13', $this->q13])
            ->andFilterWhere(['like', 'q14', $this->q14])
            ->andFilterWhere(['like', 'q15', $this->q15])
            ->andFilterWhere(['like', 'q16', $this->q16])
            ->andFilterWhere(['like', 'q17', $this->q17])
            ->andFilterWhere(['like', 'screen_date', $this->screen_date])
            ->andFilterWhere(['like', 'result', $this->result]);

        return $dataProvider;
    }
}
