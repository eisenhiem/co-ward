<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form of `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    public $start_date;
    public $end_date;
 
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'an', 'user_id','ward_id'], 'integer'],
            [['order_datetime', 'progress_note', 'order_oneday', 'order_continue','oneday_list' ,'comment', 'medication', 'record_i_o', 'food','progress','start_date','end_date','is_lab','is_xray'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'an' => $this->an,
            'user_id' => $this->user_id,
            'ward_id' => $this->ward_id,
        ]);

        $query->andFilterWhere(['like', 'progress_note', $this->progress_note])
            ->andFilterWhere(['like', 'oneday_list', $this->oneday_list])
            ->andFilterWhere(['like', 'order_oneday', $this->order_oneday])
            ->andFilterWhere(['like', 'order_continue', $this->order_continue])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'medication', $this->medication])
            ->andFilterWhere(['like', 'is_lab', $this->is_lab])
            ->andFilterWhere(['like', 'is_xray', $this->is_xray])
            ->andFilterWhere(['like', 'progress', $this->progress])
            ->andFilterWhere(['like', 'record_i_o', $this->record_i_o])
            ->andFilterWhere(['between', 'order_datetime', $this->start_date,$this->end_date])
            ->andFilterWhere(['like', 'food', $this->food]);

        return $dataProvider;
    }
}
