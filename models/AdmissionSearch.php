<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Admission;
use DateTime;
use DateInterval;
/**
 * AdmissionSearch represents the model behind the search form of `app\models\Admission`.
 */
class AdmissionSearch extends Admission
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'hn', 'age', 'ward_id',], 'integer'],
            [['pname', 'fname', 'lname','authen_code', 'gender', 'dob', 'cid', 'address', 'contact_number', 'contact_name', 'phistory', 'med_reconcile', 'cc', 'covid_register', 'allergy', 'is_admit', 'bed', 'insure_name', 'admission_date', 'isolation_date', 'ventilated', 'ventilated_other', 'sick_date', 'asymptomatic', 'insure_other', 'passport','other_comobit'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Admission::find(); 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'admission.an' => $this->an,
            'dob' => $this->dob,
            'covid_register' => $this->covid_register,
            'hn' => $this->hn,
            'age' => $this->age,
            'ward_id' => $this->ward_id,
            'admission_date' => $this->admission_date,
            'isolation_date' => $this->isolation_date,
            'sick_date' => $this->sick_date,
        ]);

        $query->andFilterWhere(['like', 'pname', $this->pname])
            ->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'cid', $this->cid])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'contact_number', $this->contact_number])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'phistory', $this->phistory])
            ->andFilterWhere(['like', 'med_reconcile', $this->med_reconcile])
            ->andFilterWhere(['like', 'cc', $this->cc])
            ->andFilterWhere(['like', 'allergy', $this->allergy])
            ->andFilterWhere(['like', 'is_admit', $this->is_admit])
            ->andFilterWhere(['like', 'bed', $this->bed])
            ->andFilterWhere(['like', 'authen_code', $this->authen_code])
            ->andFilterWhere(['like', 'insure_name', $this->insure_name])
            ->andFilterWhere(['like', 'ventilated', $this->ventilated])
            ->andFilterWhere(['like', 'ventilated_other', $this->ventilated_other])
            ->andFilterWhere(['like', 'asymptomatic', $this->asymptomatic])
            ->andFilterWhere(['like', 'insure_other', $this->insure_other])
            ->andFilterWhere(['like', 'other_comobit', $this->other_comobit])
            ->andFilterWhere(['like', 'passport', $this->passport]);

        return $dataProvider;
    }
}
