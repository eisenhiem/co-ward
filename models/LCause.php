<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_cause".
 *
 * @property string $refer_cause_id
 * @property string|null $refer_cause_name
 */
class LCause extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_cause';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refer_cause_id'], 'required'],
            [['refer_cause_id'], 'string', 'max' => 2],
            [['refer_cause_name'], 'string', 'max' => 255],
            [['refer_cause_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'refer_cause_id' => 'Refer Cause ID',
            'refer_cause_name' => 'เหตุผลการส่งต่อ',
        ];
    }
}
