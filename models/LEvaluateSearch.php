<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LEvaluate;

/**
 * LEvaluateSearch represents the model behind the search form of `app\models\LEvaluate`.
 */
class LEvaluateSearch extends LEvaluate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['evaluate_id', 'evaluate_name', 'is_active'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LEvaluate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'evaluate_id', $this->evaluate_id])
            ->andFilterWhere(['like', 'evaluate_name', $this->evaluate_name])
            ->andFilterWhere(['like', 'is_active', $this->is_active]);

        return $dataProvider;
    }
}
