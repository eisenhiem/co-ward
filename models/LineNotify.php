<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "line_notify".
 *
 * @property int $line_id
 * @property string|null $line_name
 * @property string|null $line_token
 * @property string|null $is_active
 */
class LineNotify extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'line_notify';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['line_token', 'is_active'], 'string'],
            [['line_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'line_id' => 'ID',
            'line_name' => 'Line Notify Name',
            'line_token' => 'Line Token',
            'is_active' => 'สถานะ',
        ];
    }
}
