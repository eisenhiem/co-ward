<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pt_stage".
 *
 * @property int $an
 * @property int|null $stage_id
 */
class PtStage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pt_stage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'stage_id'], 'integer'],
            [['an'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'stage_id' => 'ระดับ',
        ];
    }
    public static function itemsAlias($key){
        $items = [
          'stage' => [
            1 => 'Stage 1',
            2 => 'Stage 2',
            3 => 'Stage 3.1',
            4 => 'Stage 3.2',
            8 => 'stage 3.2.1',
            9 => 'stage 3.2.2',
            10 => 'stage 3.2.3',
            5 => 'Stage 4.1',
            6 => 'Stage 4.2',
            7 => 'stage 4.3',
            ]
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemStage()
    {
      return self::itemsAlias('stage');
    }

    public function getStage()
    {
        return ArrayHelper::getValue($this->getItemStage(),$this->stage_id);
    }

    public function getStageLevel()
    {
        if ($this->stage_id > 4) {
            return ['class' => 'btn btn-danger'];
        } else {
            if ($this->stage_id < 3) {
                return ['class' => 'btn btn-success'];
            } else {
                return ['class' => 'btn btn-warning'];
            }   
        }
    }

}
