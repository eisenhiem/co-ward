<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ward_type".
 *
 * @property int $ward_type_id
 * @property string|null $ward_type_name
 */
class WardType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ward_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ward_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ward_type_id' => 'Ward Type ID',
            'ward_type_name' => 'ประเภท',
        ];
    }
}
