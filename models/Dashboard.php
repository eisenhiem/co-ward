<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dashboard".
 *
 * @property int $total
 * @property int $male
 * @property int $female
 * @property int $dc
 * @property int $male_admit
 * @property int $female_admit
 */
class Dashboard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dashboard';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total', 'male', 'female', 'dc', 'male_admit', 'female_admit','quarantine'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'total' => 'ทั้งหมด',
            'male' => 'ชาย',
            'female' => 'หญิง',
            'dc' => 'จำหน่าย',
            'male_admit' => 'ชาย',
            'female_admit' => 'หญิง',
            'quarantine' => 'กักตัวหลัง DC 14 วัน'
        ];
    }
}
