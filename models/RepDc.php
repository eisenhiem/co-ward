<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rep_dc".
 *
 * @property int $ward_id
 * @property string|null $ward_name
 * @property int $day1_6
 * @property int $day7
 * @property int $day8
 * @property int $day9
 * @property int $day10
 * @property int $day11
 * @property int $day12
 * @property int $day13
 * @property int $day14
 */
class RepDc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rep_dc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ward_id', 'day1_6', 'day7', 'day8', 'day9', 'day10', 'day11', 'day12', 'day13', 'day14'], 'integer'],
            [['ward_name'], 'string', 'max' => 255],
        ];
    }

    public static function primaryKey()
    {
        return ['ward_id'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ward_id' => 'Ward ID',
            'ward_name' => 'หน่วย',
            'day1_6' => 'Day 1-6',
            'day7' => 'Day 7',
            'day8' => 'Day 8',
            'day9' => 'Day 9',
            'day10' => 'Day 10',
            'day11' => 'Day 11',
            'day12' => 'Day 12',
            'day13' => 'Day 13',
            'day14' => 'Day 14',
        ];
    }
}
