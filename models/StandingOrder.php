<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "standing_order".
 *
 * @property string $med_id
 * @property string|null $medicine
 * @property string|null $is_active
 */
class StandingOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standing_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['med_id'], 'required'],
            [['med_id'], 'string', 'max' => 5],
            [['medicine'], 'string', 'max' => 255],
            [['is_active'], 'string', 'max' => 1],
            [['med_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'med_id' => 'รหัสยา',
            'medicine' => 'ชื่อยาและวิธีใช้ยา',
            'is_active' => 'สถานะ',
        ];
    }

    public function getStatus(){
        return $this->is_active == '1' ? 'Active':'Not Active';
    }
}
