<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ward".
 *
 * @property int $ward_id
 * @property string|null $ward_name
 * @property string|null $is_active
 */
class Ward extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ward';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ward_type_id'], 'integer'],
            [['ward_name'], 'string', 'max' => 255],
            [['is_active'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ward_id' => 'Ward ID',
            'ward_name' => 'ชื่อ Ward',
            'ward_type_id' => 'ประเภทของ Ward',
            'is_active' => 'สถานะ Ward',
        ];
    }

    public static function itemsAlias($key)
    {
        $items = [
            'ward' => ArrayHelper::map(WardType::find()->all(), 'ward_type_id', 'ward_type_name'),
            'status' => [
                '1' => 'Active',
                '0' => 'Disable',
            ]
        ];
        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemWard()
    {
        return self::itemsAlias('ward');
    }

    public function getItemStatus()
    {
        return self::itemsAlias('status');
    }

    public function getType()
    {
        return $this->hasOne(WardType::className(), ['ward_type_id' => 'ward_type_id']);
    }
}
