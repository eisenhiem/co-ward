<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rep_ward_stage".
 *
 * @property int $ward_type_id
 * @property string|null $ward_type_name
 * @property int $stage_1
 * @property int $stage_2
 * @property int $stage_3_1
 * @property int $stage_3_2
 * @property int $stage_4_1
 * @property int $stage_4_2
 * @property int $stage_4_3
 */
class RepWardStage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rep_ward_stage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ward_type_id', 'stage_1', 'stage_2', 'stage_3_1', 'stage_3_2', 'stage_4_1', 'stage_4_2', 'stage_4_3'], 'integer'],
            [['ward_type_name'], 'string', 'max' => 255],
        ];
    }

    public static function primaryKey()
    {
        return ['ward_type_id'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ward_type_id' => 'Ward Type ID',
            'ward_type_name' => 'ประเภท Ward',
            'stage_1' => 'Stage 1',
            'stage_2' => 'Stage 2',
            'stage_3_1' => 'Stage 3.1',
            'stage_3_2' => 'Stage 3.2',
            'stage_4_1' => 'Stage 4.1',
            'stage_4_2' => 'Stage 4.2',
            'stage_4_3' => 'Stage 4.3',
        ];
    }

    public function getTotal(){
        $total =  $this->stage_1 + $this->stage_2 + $this->stage_3_1 + $this->stage_3_2 + $this->stage_4_1 + $this->stage_4_2  + $this->stage_4_3;
        return $total;
    }
}
