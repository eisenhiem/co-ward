<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_refer_by".
 *
 * @property int $refer_by_id
 * @property string|null $refer_by_name
 */
class LReferBy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_refer_by';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refer_by_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'refer_by_id' => 'Refer By ID',
            'refer_by_name' => 'ประเภทการส่งต่อ',
        ];
    }
}
