<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_sign".
 *
 * @property int $sign_id
 * @property string|null $sign_name
 */
class LSign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_sign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sign_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sign_id' => 'Sign ID',
            'sign_name' => 'Sign Name',
        ];
    }
}
