<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_chronic".
 *
 * @property string $chronic_id
 * @property string|null $chronic_name
 */
class LChronic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_chronic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chronic_id'], 'required'],
            [['chronic_id'], 'string', 'max' => 5],
            [['chronic_name'], 'string', 'max' => 255],
            [['chronic_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'chronic_id' => 'Chronic ID',
            'chronic_name' => 'Chronic Name',
        ];
    }
}
