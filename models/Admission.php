<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use DateTime;
use app\models\LChronic;
use app\models\PtStage;
/**
 * This is the model class for table "admission".
 *
 * @property int $an
 * @property string|null $pname
 * @property string $fname
 * @property string $lname
 * @property string $gender
 * @property string|null $dob
 * @property string|null $cid
 * @property string|null $address
 * @property string $contact_number
 * @property string|null $contact_name
 * @property array|null $phistory
 * @property string|null $med_reconcile
 * @property string|null $cc
 * @property string|null $covid_register
 * @property string|null $allergy
 * @property int|null $hn
 * @property string|null $is_admit
 * @property string|null $bed
 * @property int|null $ward_id
 * @property string|null $insure_name
 * @property string|null $admission_date
 * @property string|null $isolation_date
 * @property string|null $ventilated
 * @property string|null $ventilated_other
 * @property string|null $sick_date
 * @property string|null $asymptomatic
 * @property string|null $insure_other
 * @property string|null $passport
 * @property string|null $other_comobit
 * @property string|null $authen_code
 */
class Admission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admission';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'phistory',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'phistory',
                ],
                'value' => function ($event) {                    
                    return $this->phistory ? implode(',', $this->phistory ):$this->phistory;
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an','fname', 'lname', 'gender', 'contact_number'], 'required'],
            [['gender', 'address', 'med_reconcile', 'is_admit', 'ventilated', 'asymptomatic'], 'string'],
            [['dob', 'covid_register', 'admission_date', 'isolation_date', 'sick_date','phistory'], 'safe'],
            [['an','hn', 'age', 'ward_id'], 'integer'],
            [['pname'], 'string', 'max' => 20],
            [['fname', 'lname','authen_code'], 'string', 'max' => 50],
            [['cid'], 'string', 'max' => 13],
            [['contact_number', 'bed'], 'string', 'max' => 10],
            [['contact_name','passport', 'cc', 'allergy', 'insure_name', 'ventilated_other','insure_other','other_comobit'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'AN',
            'hn' => 'HN',
            'pname' => 'คำนำหน้า',
            'fname' => 'ชื่อ',
            'lname' => 'สกุล',
            'gender' => 'เพศ',
            'dob' => 'วันเกิด',
            'age' => 'อายุ',
            'cid' => 'เลขบัตรประชาชน',
            'address' => 'ที่อยู่',
            'contact_number' => 'เบอร์ติดต่อ',
            'contact_name' => 'ผู้ที่สามารถติดต่อได้',
            'phistory' => 'โรคเดิม',
            'med_reconcile' => 'ยาเดิม',
            'cc' => 'อาการสำคัญ',
            'covid_register' => 'วันที่ตรวจพบ',
            'allergy' => 'แพ้ยา/อาหาร',
            'is_admit' => 'สถานะ',
            'bed' => 'เตียง',
            'ward_id' => 'Ward',
            'insure_name' => 'สิทธิการรักษา',
            'admission_date' => 'Admission Date',
            'isolation_date' => 'Isolation Date',
            'ventilated' => 'Patient Ventilated',
            'ventilated_other' => 'Specify',
            'sick_date' => 'วันที่เริ่มป่วย',
            'asymptomatic' => 'Asymptomatic',
            'insure_other' => 'สิทธิ์อื่นๆ',
            'passport' => 'เลขที่ Passport',
            'other_comobit' => 'โรคร่วมอื่น ๆ ',
            'authen_code' => 'Authen CODE',
        ];
    }

    public function getPatientName(){
        return $this->pname.$this->fname.' '.$this->lname;
    }

    public function getAdmit(){
        return $this->is_admit == '0' ? 'Admit':'Discharge';
    }

    public function getDsch(){
        return $this->is_admit == '1' ? true:false;
    }

    public function getWard(){
        return $this->hasOne(Ward::className(), ['ward_id' => 'ward_id']);
    }

    public function getPlandc(){
        return $this->hasOne(PlanDc::className(), ['an' => 'an']);
    }

    public function getWardName(){
        return $this->ward->ward_name;
    }

    public function getPtsign(){
        return $this->hasOne(Ptsign::className(), ['an' => 'an']);
    }

    public function getVitalsign(){
        return $this->hasOne(Vitalsign::className(), ['an' => 'an']);
    }

    public function getHealDay(){
        if($this->ptsign){
            $visit = new DateTime($this->ptsign->swap_date);
            $now = new DateTime(date('Y-m-d'));
            $interval = date_diff($visit, $now);
    
            return $interval->format('%d');    
        }

        return 'ยังไม่มีผล swab';
    }

    public function getStage(){
        return $this->hasOne(PtStage::className(), ['an' => 'an']);
    }

    public function getDc(){
        return $this->hasOne(PlanDc::className(), ['an' => 'an']);
    }

    public function chronicToArray()
    {
      return $this->phistory = explode(',', $this->phistory);
    }

    public static function itemsAlias($key){
        $items = [
          'insurance'=> [
              'UC' => 'ประกันสุขภาพ',
              'SS' => 'ประกันสังคม',
              'OFC' => 'กรมบัญชีกลาง',
              'Other' => 'อื่นๆ',
          ],
          'ventilated' => [
              '0' => 'No',
              '1' => 'Yes',
              '9' => 'Unknown',
          ],
          'asymptomatic' => [
              '0' => 'Unknown',
              '1' => 'Asymptomatic',
              '2' => 'Symptomatic',
          ],
          'chronic' => ArrayHelper::map(LChronic::find()->all(), 'chronic_id', 'chronic_name'),
        ];
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }
    
    public function getItemInsure()
    {
      return self::itemsAlias('insurance');
    }

    public function getItemVen()
    {
      return self::itemsAlias('ventilated');
    }

    public function getItemAsymp()
    {
      return self::itemsAlias('asymptomatic');
    }

    public function getItemChronic()
    {
      return self::itemsAlias('chronic');
    }

    public function getChronicName(){
        $chronic = $this->getItemChronic();
        $chronicSelected = explode(',', $this->phistory);
        $chronicSelectedName = [];
        foreach ($chronic as $key => $chronicName) {
          foreach ($chronicSelected as $chronicKey) {
            if($key === $chronicKey){
              $chronicSelectedName[] = $chronicName;
            }
          }
        }   
        return implode('<br> &emsp;', $chronicSelectedName);
    }

    public static function getThaiDate($date)
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        if($date){
            $visit = new DateTime($date);
            $d = $visit->format('j');
            $m = $month[$visit->format('m')];
            $y = $visit->format('Y')+543;
            $result = $d.' '.$m.' '.$y;    
        } else {
            $result = 'ไม่ได้ระบุวัน';
        }
        return $result;
    }

    public static function getThaiDateTime($date)
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        if($date){
            $visit = new DateTime($date);
            $d = $visit->format('j');
            $m = $month[$visit->format('m')];
            $y = $visit->format('Y')+543;
            $h = $visit->format('H');
            $i = $visit->format('i');
            $result = $d.' '.$m.' '.$y.' '.$h.':'.$i.'  น.'  ;    
        } else {
            $result = 'ไม่ได้ระบุวัน';
        }
        return $result;
    }

    public static function changeToThaiNumber($n){
        return str_replace(array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'),array("๐", "๑", "๒", "๓", "๔", "๕", "๖", "๗", "๘", "๙"),$n);
    }

    public function getThaiCid(){
        return substr_replace(substr_replace(substr_replace(substr_replace($this->cid,'-', 1, 0),'-',6,0),'-',12,0),'-',15,0);

    }

    public static function getFullThaiDate($date)
    {
        $month = array("01"=>"มกราคม","02"=>"กุมภาพันธ์","03"=>"มีนาคม","04"=>"เมษายน","05"=>"พฤษภาคม","06"=>"มิถุนายน","07"=>"กรกฎาคม","08"=>"สิงหาคม","09"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
        if($date){
            $visit = new DateTime($date);
            $d = $visit->format('j');
            $m = $month[$visit->format('m')];
            $y = $visit->format('Y')+543;
            $result = $d.' '.$m.' พ.ศ. '.$y;    
        } else {
            $result = 'ไม่ได้ระบุวัน';
        }
        return $result;
    }

    public function getInsureName(){
        return ArrayHelper::getValue($this->getItemInsure(),$this->insure_name);
    }
    public function getVentilaterName(){
        return ArrayHelper::getValue($this->getItemVen(),$this->ventilated);
    }
    public function getAsympName(){
        return ArrayHelper::getValue($this->getItemAsymp(),$this->asymptomatic);
    }


}
