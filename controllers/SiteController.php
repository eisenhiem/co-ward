<?php

namespace app\controllers;

use app\models\Admission;
use app\models\PatientSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Dashboard;
use app\models\Ward;
use app\models\WardSearch;
use app\models\RepDc;
use app\models\RepDcSearch;
use app\models\RepStageSearch;
use app\models\RepWardStageSearch;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new WardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = Ward::find()->All();

        $searchModelRep = new RepDcSearch();
        $dataProviderRep = $searchModelRep->search(Yii::$app->request->queryParams);

        $searchModelStage = new RepStageSearch();
        $dataProviderStage = $searchModelStage->search(Yii::$app->request->queryParams);

        $searchModelWard = new RepWardStageSearch();
        $dataProviderWard = $searchModelWard->search(Yii::$app->request->queryParams);

        $dashboard = Dashboard::find()->one();

        $searchModelAdmission = new PatientSearch();
        $dataProviderAdmission = $searchModelAdmission->search(Yii::$app->request->queryParams);
        $pt = $dataProviderAdmission->getModels();
        $all = Admission::find()->count();
        $get_pt = $dataProviderAdmission->getTotalCount();
        $message = '';
        if(!($all == $get_pt)){
            foreach($pt as $p){
                $message = $message.' ['.$p->pname.$p->fname.' '.$p->lname.':('.$p->ward->ward_name.')]';
            }    
        Yii::$app->session->setFlash('success', $message);

        }
        
        $rep = new ReportController('rep',$this->module);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderRep' => $dataProviderRep,
            'dataProviderStage' => $dataProviderStage,
            'dataProviderWard' => $dataProviderWard,
            'searchModelAdmission' => $searchModelAdmission,
            'dashboard' => $dashboard,
            'model' => $model,
            'rep' => $rep,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
