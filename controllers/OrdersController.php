<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Admission;
use app\models\Office;
use kartik\mpdf\Pdf;
use app\models\OrderOneday;
use app\models\Setting;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionDetail($id)
    {
        $order = Setting::find()->where(['config_name'=>'order_sort'])->one();
        if($order->config_value =='desc'){
            $model = Orders::find()->where(['an'=>$id])->orderBy(['order_id' => SORT_DESC])->all();
        } else {
            $model = Orders::find()->where(['an'=>$id])->orderBy(['order_id' => SORT_ASC])->all();
        }
        $ward = Admission::find()->where(['an'=>$id])->one();
        $an = $id;
        return $this->render('list', [
            'model' => $model,
            'an' => $an,
            'ward' =>$ward,
            'order' => $order,
        ]);
    }

    public function actionSwitchsort($id)
    {
        $layout = Setting::find()->where(['config_name'=>'order_sort'])->one();
        if ($layout->config_value == 'asc'){
            $layout->config_value = 'desc';
            $layout->save();
        } else {
            $layout->config_value = 'asc';
            $layout->save();
        }
        return $this->redirect(['detail','id' => $id]);

    }

    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionPrint($id)
    {
        $model = Orders::find()->where(['an'=>$id])->all();
        $pt = Admission::find()->where(['an'=>$id])->one();
        $total = Orders::find()->where(['an'=>$id])->count();
        $office = Office::find()->one();

        $content = $this->renderPartial('_print',[
            'model' => $model,
            'pt' => $pt,
            'total' => $total,
            'office' => $office,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Doctor Order : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionOrder($id)
    {
        $order = $this->findModel($id);
        $pt = Admission::find()->where(['an'=>$order->an])->one();
        $office = Office::find()->one();

        $content = $this->renderPartial('_order',[
            'order' => $order,
            'pt' => $pt,
            'office' => $office,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Doctor Order : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Orders();
        $total = Orders::find()->where(['an'=>$id])->count();
        $model->an = $id;
        $model->user_id = Yii::$app->user->identity->id;
        $admit =  Admission::find()->where(['an' =>$id])->one();
        if($admit->ward_id){
            $model->ward_id = $admit->ward_id;
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->oneday_list){
                foreach($model->oneday_list as $order => $ordercode) 
                {
                    $r = OrderOneday::findOne($ordercode);
                    $model->is_lab ='0';
                    $model->is_xray ='0';
                    if($r->order_group_id == 'x') { $model->is_xray = '1'; }
                    if($r->order_group_id == 'l') { $model->is_lab = '1'; }
                }    
            }
            $model->save();
            return $this->redirect(['detail', 'id' => $id]);
        }

        return $this->render('create', [
            'model' => $model,
            'total' => $total,
        ]);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $model->medToArray();
        $model->progressToArray();
        $model->orderToArray();
        $total = Orders::find()->where(['an'=>$id])->count();

        if ($model->load(Yii::$app->request->post())) {
            if($model->oneday_list){
                foreach($model->oneday_list as $order => $ordercode) 
                {
                    $r = OrderOneday::findOne($ordercode);
                    $model->is_lab ='0';
                    $model->is_xray ='0';
                    if($r->order_group_id == 'x') { $model->is_xray = '1'; }
                    if($r->order_group_id == 'l') { $model->is_lab = '1'; }
                }    
            }
            $model->save();
            return $this->redirect(['detail', 'id' => $model->an]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'total' => $total,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
