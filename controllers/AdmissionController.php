<?php

namespace app\controllers;

use Yii;
use app\models\Admission;
use app\models\AdmissionSearch;
use app\models\Move;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Ward;
use app\models\Vitalsign;
use app\models\Ptsign;
use app\models\PlanDc;
use app\models\Screen;
use kartik\mpdf\Pdf;
use app\models\Setting;
use app\models\PtStage;

/**
 * AdmissionController implements the CRUD actions for Admission model.
 */
class AdmissionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Admission models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        $searchModel = new AdmissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['is_admit'=>'0','ward_id'=>$id]);
        $model = Admission::find()->where(['is_admit'=>'0','ward_id'=>$id])->All();
        $ward = Ward::findOne($id);
        $layout = Setting::find()->where(['config_name'=>'layout'])->one();
        if ($layout->config_value == 'grid'){
            return $this->render('index_grid', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'ward' => $ward,
            ]);    
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
                'ward' => $ward,
            ]);    
        }
    }

    public function actionStage($id){
        $model = PtStage::findOne($id);

        if(!$model){
            $model = new PtStage();
            $model->an = $id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->an]);
        }
        return $this->render('stage', [
            'model' => $model,
        ]);
    } 
    /**
     * Lists all Admission models.
     * @return mixed
     */
    public function actionTodayadmit()
    {
        $model = Admission::find()->where(['admission_date' => date('Y-m-d')])->All();
        $title = 'ผู้ป่วยรับเข้าใหม่วันนี้';
        return $this->render('index_today', [
            'model' => $model,
            'title' => $title,
        ]);
    }

    public function actionTodaydc()
    {
        $model = PlanDc::find()->where(['dc_date'=>date('Y-m-d')])->All();
        $title = 'ผู้ป่วยจำหน่ายวันนี้';
        return $this->render('index_dc', [
            'model' => $model,
            'title' => $title,
        ]);
    }

    public function actionYesterdaydc()
    {
        $model = PlanDc::find()->where(['dc_date'=>date('Y-m-d', strtotime("-1 day"))])->All();
        $title = 'ผู้ป่วยจำหน่ายเมื่อวานนี้';
        return $this->render('index_dc', [
            'model' => $model,
            'title' => $title,
        ]);
    }

    public function actionSwitchlayout($id)
    {
        $layout = Setting::find()->where(['config_name'=>'layout'])->one();
        if ($layout->config_value == 'grid'){
            $layout->config_value = 'card';
            $layout->save();
        } else {
            $layout->config_value = 'grid';
            $layout->save();
        }
        return $id == 'dc' ?  $this->redirect(['discharge']):$this->redirect(['index','id' => $id]);

    }

    public function actionCanceldc($id)
    {
        $model = $this->findModel($id);
        $model->is_admit = '0';
        $model->save();
        
        return $this->redirect(['index','id'=>$model->ward_id]);
    }

    /**
     * Lists all Admission models.
     * @return mixed
     */
    public function actionDischarge()
    {
        $searchModel = new AdmissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->joinWith(['plandc']);
        $dataProvider->query->andFilterWhere(['is_admit'=>'1']);
        $dataProvider->query->orderBy(['dc_date'=>SORT_DESC]);
        $model = Admission::find()->where(['is_admit'=>'1'])->All();
        $layout = Setting::find()->where(['config_name'=>'layout'])->one();
        if ($layout->config_value == 'grid'){
            return $this->render('dc_grid', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            } else {
            return $this->render('dc', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
            }

    }
 
    /**
     * Displays a single Admission model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $vs = Vitalsign::findOne($id);
        $sign = Ptsign::findOne($id);
        $dc = PlanDc::findOne($id);
        $stage = PtStage::findOne($id);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'vs' => $vs,
            'sign' => $sign,
            'dc' => $dc,
            'stage' => $stage,
        ]);
    }

    public function actionPrint($id)
    {
        $vs = Vitalsign::findOne($id);
        $sign = Ptsign::findOne($id);
        $dc = PlanDc::findOne($id);

        $content = $this->renderPartial('_print',[
            'model' => $this->findModel($id),
            'vs' => $vs,
            'sign' => $sign,
            'dc' => $dc,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Admission Note : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    public function actionPrintisolate($id)
    {
        $vs = Vitalsign::findOne($id);
        $sign = Ptsign::findOne($id);
        $dc = PlanDc::findOne($id);

        $content = $this->renderPartial('_print_isolate',[
            'model' => $this->findModel($id),
            'vs' => $vs,
            'sign' => $sign,
            'dc' => $dc,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'ใบกักตัว : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }
    /**
     * Creates a new Admission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Admission();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(Screen::findOne($model->an)){
                return $this->redirect(['screen/update', 'id' => $model->an]);
            }else{
                return $this->redirect(['screen/create', 'id' => $model->an]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Admission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->chronicToArray();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(Screen::findOne($model->an)){
                return $this->redirect(['screen/update', 'id' => $model->an]);
            }else{
                return $this->redirect(['screen/create', 'id' => $model->an]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionMoveward($id)
    {
        $model = $this->findModel($id);
        $move = new Move();
        $move->an = $model->an; 
        $move->ward_id = $model->ward_id;
        $move->move_date = date('Y-m-d');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $move->to_ward_id = $model->ward_id;
            $move->save();

            return $this->redirect(['index',
                'id' => $model->ward_id,
            ]);
        }

        return $this->render('move', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Admission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDc($id)
    {
        $model = $this->findModel($id);
        $model->is_admit = '1';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(PlanDc::findOne($id)){
                return $this->redirect(['plan/update', 'id' => $id]);
            } else {
                return $this->redirect(['plan/create', 'id' => $id]);
            }
        }

        return $this->render('discharge', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Admission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Admission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Admission::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
