<?php

namespace app\controllers;

use Yii;
use app\models\RepDc;
use app\models\RepDcSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
Use app\models\RepStageSearch;
use app\models\OrdersSearch;
use app\models\RepWardStageSearch;
use app\models\LineNotify;
use app\models\RepWardStage;
use app\models\RepStage;
use app\models\RepPtSearch;
use yii\helpers\Json;


/**
 * ReportController implements the CRUD actions for RepDc model.
 */
class ReportController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RepDc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $repStage = RepStage::find()->all();
        $repWardStage = RepWardStage::find()->all();
        $repStay = RepDC::find()->all();

        return $this->render('index',[
            'repstage' => $repStage,
            'repwardstage' => $repWardStage,
            'repstay' => $repStay,
        ]);
    }

    /**
     * Lists all RepDc models.
     * @return mixed
     */
    public function actionStay()
    {
        $searchModel = new RepDcSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('stay', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all RepDc models.
     * @return mixed
     */
    public function actionDays()
    {
        $searchModel = new RepPtSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('days', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all RepDc models.
     * @return mixed
     */
    public function actionStage()
    {
        $searchModel = new RepStageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('stage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all RepDc models.
     * @return mixed
     */
    public function actionWard()
    {
        $searchModel = new RepWardStageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $model = $dataProvider->getModels();
    
        return $this->render('ward', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
//            '$model' => $model,

        ]);
    }


    public function actionLabxray()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['or',['is_lab'=>'1'],['is_xray'=>'1']]);
        
        return $this->render('labxray', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the RepDc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RepDc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RepDc::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSend($id,$message)
    {
        $this->sendToLine($id,$message);

        return $this->redirect(['index']);

    }
    
    public function actionSendall($message)
    {
        $line = LineNotify::find()->where(['is_active' => '1'])->all();
        foreach($line as $l)
        {
            $this->sendToLine($$l->line_id,$message);
        }

        return $this->redirect(['index']);
    }

    protected function sendToLine($id,$message){    

        $line_api = 'https://notify-api.line.me/api/notify';
        $line = LineNotify::findOne($id);
        $line_token = $line->line_token;
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://notify-api.line.me/api/notify");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'message='.$message);
        // follow redirects
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-type: application/x-www-form-urlencoded',
            'Authorization: Bearer '.$line_token,
        ]);
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        $server_output = curl_exec ($ch);
    
        curl_close ($ch);
    }

}
