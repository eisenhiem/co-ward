<?php

namespace app\controllers;

use Yii;
use app\models\Notes;
use app\models\NotesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\Setting;
use app\models\Admission;
use app\models\Office;
use app\models\Orders;
use app\models\PtStage;
use app\models\VsDaily;
use kartik\mpdf\Pdf;



/**
 * NotesController implements the CRUD actions for Notes model.
 */
class NotesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Notes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

        /**
     * Lists all Notes where AN .
     * @return mixed
     */
    public function actionDetail($id)
    {
        $note = Setting::find()->where(['config_name'=>'note_sort'])->one();
        if($note->config_value =='desc'){
            $model = Notes::find()->where(['an'=>$id])->orderBy(['note_id' => SORT_DESC])->all();
        } else {
            $model = Notes::find()->where(['an'=>$id])->orderBy(['note_id' => SORT_ASC])->all();
        }
        $ward = Admission::find()->where(['an'=>$id])->one();
        $an = $id;
        return $this->render('list', [
            'model' => $model,
            'an' => $an,
            'ward' =>$ward,
            'note' => $note,
        ]);
    }

    public function actionSwitchsort($id)
    {
        $layout = Setting::find()->where(['config_name'=>'note_sort'])->one();
        if ($layout->config_value == 'asc'){
            $layout->config_value = 'desc';
            $layout->save();
        } else {
            $layout->config_value = 'asc';
            $layout->save();
        }
        return $this->redirect(['detail','id' => $id]);
    }

    /**
     * Displays a single Notes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Notes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Notes();
        $model->an = $id;
        $model->user_id = Yii::$app->user->identity->id;
        $vs = new VsDaily();
        $model->is_save = 0;


        if ($model->load(Yii::$app->request->post())&& $vs->load(Yii::$app->request->post()) && $model->save()) {
            $vs->note_id = $model->note_id;
            if($vs->round_id){
                $vs->save();
                if($model->is_save == 1){
                    $order = new Orders();
                    $order->an = $model->an;
                    $admit =  Admission::find()->where(['an' =>$id])->one();
                    $stage = PtStage::find()->where(['an' =>$id])->one();
                    $stage_name = '';
                    if($stage){
                        $stage_name = 'COVID-19 : '.$stage->getStageLevel();
                    }
                    if($admit->ward_id){
                        $order->ward_id = $admit->ward_id;
                    }
                    $order->progress_note = $vs->round->round_name.' BT:'.$vs->body_temp.' ํC , O2Sat:'.$vs->o2sat.' %'."\n".'&emsp;'.$stage_name;
                    $order->user_id = $model->user_id;
                    $order->save(false);
                }
            }
            return $this->redirect(['detail', 'id' => $id]);
        }

        return $this->render('create', [
            'model' => $model,
            'vs' => $vs,
        ]);
    }

    /**
     * Updates an existing Notes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $vs = VsDaily::findOne($model->note_id);
        $model->actToArray();
        $model->evaToArray();
        
        if(!$vs){
            $vs = new VsDaily();
            $vs->note_id = $model->note_id;
        }

        if ($model->load(Yii::$app->request->post()) && $vs->load(Yii::$app->request->post()) && $model->save()) {
            if ($vs->round_id){
                $vs->save();
            }
            return $this->redirect(['detail', 'id' => $model->an]);
        }

        return $this->render('update', [
            'model' => $model,
            'vs' => $vs,
        ]);
    }

    /**
     * Deletes an existing Notes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Notes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Lists all Orders where AN .
     * @return mixed
     */
    public function actionPrint($id)
    {
        $model = Notes::find()->where(['an'=>$id])->all();
        $pt = Admission::find()->where(['an'=>$id])->one();
        $total = Notes::find()->where(['an'=>$id])->count();
        $office = Office::find()->one();

        $content = $this->renderPartial('_print',[
            'model' => $model,
            'pt' => $pt,
            'total' => $total,
            'office' => $office,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Nurse Note : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionVitalsign($id)
    {
        $bt = [];
        $pr = [];
        $rr = [];
        $o2sat = [];
        $sbp = [];
        $dbp = [];
        $date = [];
        $an=$id;
        $model = Notes::find()->where(['an'=>$id])->all();
        foreach($model as $note){
            $vs = VsDaily::findOne($note->note_id);
            if($vs->body_temp){
                array_push($bt,intVal($vs->body_temp));
            } else {
                array_push($bt,0);
            }
            if($vs->sbp){
                array_push($sbp,intVal($vs->sbp));
            } else {
                array_push($sbp,0);
            }
            if($vs->dbp){
                array_push($dbp,intVal($vs->dbp));
            } else {
                array_push($dbp,0);
            }
            if($vs->pr){
                array_push($pr,intVal($vs->pr));
            } else {
                array_push($pr,0);
            }
            if($vs->rr){
                array_push($rr,intVal($vs->rr));
            } else {
                array_push($rr,0);
            }
            if($vs->o2sat){
                array_push($o2sat,intVal($vs->o2sat));
            } else {
                array_push($o2sat,0);
            }
            array_push($date,substr($note->nurse_datetime,0,10).' '.$vs->round->round_name);
        }

        return $this->render('vitalsign',[
            'bt' => $bt,
            'sbp' => $sbp,
            'dbp' => $dbp,
            'pr' => $pr,
            'rr' => $rr,
            'o2sat' => $o2sat,
            'date' => $date,
            'an' => $an,
        ]);
    }

}
