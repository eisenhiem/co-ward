<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlanDcSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Plan Dcs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-dc-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Plan Dc', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'an',
            'dc_date',
            'address',
            'phone_number',
            'contact_name',
            //'concution:ntext',
            //'dctype',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
