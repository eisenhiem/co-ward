<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PlanDc */

$this->title = 'Create Plan Dc';
$this->params['breadcrumbs'][] = ['label' => 'Plan Dcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-dc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
