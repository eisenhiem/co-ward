<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PlanDc */

$this->title = 'Update Plan Dc: ' . $model->an;
$this->params['breadcrumbs'][] = ['label' => 'Plan Dcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->an, 'url' => ['view', 'id' => $model->an]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="plan-dc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
