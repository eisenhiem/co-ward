<?php 
use yii\helpers\Html;
use app\models\Office;
use app\models\Admission;

$hospital = Office::find()->one();
?>

<table width="100%">
    <tr>
        <td valign="bottom" width="250">
        ที่ <?= $hospital->book_no ?>/.........
        </td>
        <td align="center">
            <?= Html::img(Yii::getAlias('@app').'/web/images/crut.png', ['width' => 80]) ?>
        </td>
        <td valign="bottom" width="250"><?= $hospital->hospname ?>
        </td>
    </tr>
    <tr>
        <td colspan="2"></td>
        <td><?= $hospital->address ?> <?= $hospital->zipcode ?></td>
    </tr>
</table>

<h3 align="center">หนังสือรับรอง</h3>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;หนังสือฉบับนี้ขอรับรองว่า <?= $admission->getPatientName() ?> หมายเลขบัตรประชาชน <?= Admission::changeToThaiNumber($admission->getThaiCid()) ?> ตรวจพบเชื้อไวรัสโคโรนา 2019 (COVID-19) และได้รับการรักษาและกักตัวครบ ๑๔ วัน ตามกำหนดที่ <?= $hospital->hospname ?> ในระหว่าง วันที่ <?= Admission::changeToThaiNumber($admission->getFullThaiDate($admission->admission_date)) ?> ถึงวันที่ <?= Admission::changeToThaiNumber($admission->getFullThaiDate($model->dc_date)) ?> และผู้ป่วยอาการดีขึ้น หายป่วยจากโรค COVID-19 เมื่อพิจารณา จากอาการเป็นหลัก โดยขอให้ท่านปฏิบัติตัวต่อ ดังนี้<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;๑. ขอให้สวมหน้ากากผ้า หรือหน้ากากอนามัย เมื่ออยู่ร่วมกับผู้อื่นอย่างเข้มข้น เว้นระยะห่างจากผู้อื่น หลีกเลี่ยงการเข้าไปสถานที่มีคนหนาแน่น ต่อเนื่องอย่างน้อยอีก ๑๔ วัน จนครบ ๒๘ วัน<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;๒. ล้างมือด้วยสบู่และน้ำเป็นประจำ โดยเฉพาะหลังจากถ่ายปัสสาวะหรืออุจจาระ หรือถูมือด้วยเจลแอลกอฮอล์<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;๓. ไม่ใช้อุปกรณ์รับประทานอาหารและแก้วน้ำร่วมกับผู้อื่น<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;๔. ดื่มน้ำสะอาดให้เพียงพอ รับประทานอาหารสุก สะอาด และมีประโยชน์ตามหลักโภชนาการ<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;๕. ทำความสะอาดพื้นผิวสัมผัส ประตู ลูกบิด บริเวณที่พักอาศัยบ่อยๆ ด้วยน้ำยาฆ่าเชื้อ หรือแอลกอฮอล์ ๗๐% หรือน้ำยาซักผ้าขาว ๑ ส่วนต่อน้ำ ๙ ส่วน<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;๖. หากมีอาการป่วยเกิดขึ้นใหม่หรือมีอาการเดิมมากขึ้น เช่น ไข้สูง ไอมาก เหนื่อยแน่นหน้าอก หอบ หายใจไม่สะดวก เบื่ออาหาร ให้ติดต่อสถานพยาบาล หากต้องเดินทางมาสถานพยาบาลแนะนำให้สวมหน้ากาก ระหว่างการเดินทางตลอดเวลา<br>
<br>
&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;ให้ไว้ ณ วันที่ <?= Admission::changeToThaiNumber($admission->getFullThaiDate(date('y-m-d'))) ?>
<br>
<br>
<br>
<table width= "100%">
    <tr>
        <td width="50%" rowspan="4">

        </td>
        <td width="50%">
            ลงชื่อ<br>&emsp;
        </td>
    </tr>
    <tr>
        <td align="center">
            (<?= $hospital->director_name ?>)
        </td> 
    </tr>
    <tr>
        <td align="center">
            <?= $hospital->director_position1 ?>
        </td> 
    </tr>
    <tr>
        <td align="center">
            <?= $hospital->director_position2 ?>
        </td> 
    </tr>
</table>
<sethtmlpagefooter name="sign" value="on"/>
<htmlpagefooter name="sign">
<?= $hospital->hospname ?> โทร <?= $hospital->phone_number ?>
</htmlpagefooter>