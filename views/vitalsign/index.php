<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VitalsignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vitalsigns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vitalsign-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Vitalsign', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'an',
            'bw',
            'height',
            'body_temp',
            'rr',
            //'pr',
            //'sbp',
            //'dbp',
            //'o2sat',
            //'symptom',
            //'other_symptom',
            //'pain',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
