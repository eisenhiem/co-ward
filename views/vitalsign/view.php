<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vitalsign */

$this->title = $model->an;
$this->params['breadcrumbs'][] = ['label' => 'Vitalsigns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vitalsign-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->an], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->an], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'an',
            'bw',
            'height_cm',
            'body_temp',
            'rr',
            'pr',
            'sbp',
            'dbp',
            'o2sat',
            'symptom',
            'other_symptom',
            'pain',
        ],
    ]) ?>

</div>
