<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Vitalsign */
/* @var $form yii\widgets\ActiveForm */
$r = ['0' => 'ไม่ใช่' , '1'=> 'ใช่'];
?>

<div class="vitalsign-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">Vital Sign</div>
        <div class="panel-body">
        <div class="col-md-1 col-sm-3">
            <?= $form->field($model, 'bw')->textInput() ?>
        </div>
        <div class="col-md-1 col-sm-3">
            <?= $form->field($model, 'height_cm')->textInput() ?>
        </div>
        <div class="col-md-1 col-sm-3">
            <?= $form->field($model, 'body_temp')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-3">
            <?= $form->field($model, 'rr')->textInput() ?>
        </div>
        <div class="col-md-1 col-sm-3">
            <?= $form->field($model, 'pr')->textInput() ?>
        </div>
        <div class="col-md-1 col-sm-3">
            <?= $form->field($model, 'sbp')->textInput() ?>
        </div>
        <div class="col-md-1 col-sm-3">
            <?= $form->field($model, 'dbp')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-3">
            <?= $form->field($model, 'o2sat')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-3">
            <?= $form->field($model, 'o2sat_ra')->textInput() ?>
        </div>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10">
            <?= $form->field($model, 'symptom')->checkboxList($model->getItemSymp(),['item' => function($index, $label, $name, $checked, $value) {
            return "<label class='col-md-4'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
        }]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'pain')->radioList($model->getItemPain()) ?>
        </div>
    </div>

    <h3>การประเมินสภาพจิตใจและภาวะซึมเศร้า </h3>
    <?= $form->field($model, 'q1')->radioList($r) ?>
    <?= $form->field($model, 'q2')->radioList($r) ?>
    <?= $form->field($model, 'other_symptom')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึกอาการแรกรับ', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
