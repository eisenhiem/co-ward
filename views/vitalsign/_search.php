<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VitalsignSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vitalsign-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'an') ?>

    <?= $form->field($model, 'bw') ?>

    <?= $form->field($model, 'height') ?>

    <?= $form->field($model, 'body_temp') ?>

    <?= $form->field($model, 'rr') ?>

    <?php // echo $form->field($model, 'pr') ?>

    <?php // echo $form->field($model, 'sbp') ?>

    <?php // echo $form->field($model, 'dbp') ?>

    <?php // echo $form->field($model, 'o2sat') ?>

    <?php // echo $form->field($model, 'symptom') ?>

    <?php // echo $form->field($model, 'other_symptom') ?>

    <?php // echo $form->field($model, 'pain') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
