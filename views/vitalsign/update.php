<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vitalsign */

$this->title = 'Update Vitalsign: ' . $model->an;
$this->params['breadcrumbs'][] = ['label' => 'Vitalsigns', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->an, 'url' => ['view', 'id' => $model->an]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vitalsign-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
