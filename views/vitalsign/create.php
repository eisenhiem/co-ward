<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vitalsign */

$this->title = 'บันทึกอาการแรกรับ';
$this->params['breadcrumbs'][] = ['label' => 'อาการแรกรับ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vitalsign-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
