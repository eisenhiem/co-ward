<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WardType */

$this->title = 'Create Ward Type';
$this->params['breadcrumbs'][] = ['label' => 'Ward Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ward-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
