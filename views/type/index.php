<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WardTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประเภท Ward';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ward-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มประเภทของ Ward', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ward_type_name',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Html::a('แก้ไข',['standingorder/update','id'=>$model->ward_type_id],['class' => 'btn btn-primary']);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
