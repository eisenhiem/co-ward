<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Refers */

$this->title = 'แก้ไขการส่งต่อ: ' . $model->refer_id;
$this->params['breadcrumbs'][] = ['label' => 'การส่งต่อ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->refer_id, 'url' => ['view', 'id' => $model->refer_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="refers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
