<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RefersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="refers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'refer_id') ?>

    <?= $form->field($model, 'an') ?>

    <?= $form->field($model, 'refer_hosp') ?>

    <?= $form->field($model, 'refer_cause') ?>

    <?= $form->field($model, 'refer_by') ?>

    <?php // echo $form->field($model, 'refer_diag_icd10') ?>

    <?php // echo $form->field($model, 'refer_daig_text') ?>

    <?php // echo $form->field($model, 'refer_datetime') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
