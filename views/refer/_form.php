<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Refers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="refers-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'refer_hosp')->dropDownList($model->getItemHosp()) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'refer_cause')->dropDownList($model->getItemCause()) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        <?= $form->field($model, 'refer_by')->radioList($model->getItemBy()) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'refer_diag_icd10')->dropDownList($model->getItemDiag()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'refer_daig_text')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
        <?php 
                echo '<label class="control-label">วัน เวลาส่งต่อ</label>';
                echo DateTimePicker::widget([
                    'model' => $model,
                    'attribute' => 'refer_datetime',
	                'value' => date('Y-m-d H:i:s'),
	                'pluginOptions' => [
		                'autoclose' => true,
		                'format' => 'yyyy-mm-dd hh:ii:ss'
	                ]
                ]); 
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
