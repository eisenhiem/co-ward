<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RefersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Refers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Refers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'refer_id',
            'an',
            'refer_hosp',
            'refer_cause',
            'refer_by',
            //'refer_diag_icd10',
            //'refer_daig_text',
            //'refer_datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
