<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Refers */

$this->title = 'บันทึกการส่งต่อ';
$this->params['breadcrumbs'][] = ['label' => 'การส่งต่อ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
