<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Screen */

$this->title = 'คัดกรอง';
$this->params['breadcrumbs'][] = ['label' => 'คัดกรอง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="screen-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
