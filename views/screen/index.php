<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScreenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Screens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="screen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Screen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'an',
            'q1',
            'q2',
            'q3',
            'q4',
            //'q5',
            //'q6',
            //'q61',
            //'q62',
            //'q63',
            //'q64',
            //'q65',
            //'q66',
            //'q7',
            //'q8',
            //'q9',
            //'q10',
            //'q11',
            //'q12',
            //'q13',
            //'q14',
            //'q15',
            //'q16',
            //'q17',
            //'result',
            //'user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
