<h5 align="center">รายการตรวจสอบ(Checklist) สำหรับการพิจารณาผู้ติดเชื้อ COVID-19 เพื่อการแยกกักตัวที่บ้าน(Home Isolation)</h5>
&emsp;&emsp;ชื่อ-สกุลผู้ป่วย <?= $pt->getPatientName() ?> อายุ <?= $pt->age ?> ปี HN <?= $pt->hn ?> วันที่ <?= $pt->getThaiDate($model->screen_date) ?>
<table width="100%" border="1" cellpadding="2" cellspacing="0">
<tr>
    <th>ข้อ</th>
    <th>รายการ</th>
    <th align="center" width="50">ใช่</th>
    <th align="center" width="50">ไม่ใช่</th>
</tr>
<tr>
    <td colspan="4">เกณฑ์การพิจารณาผู้ติดเชื้อ COVID-19 เพื่อแยกตัวที่บ้าน</td>
</tr>
<tr>
    <td align="center">1</td>
    <td>ผู้ติดเชื้อที่ไม่มีอาการ</td>
    <td align="center"><?= $model->q1 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q1 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">2</td>
    <td>อายุน้อยกว่า 60 ปี</td>
    <td align="center"><?= $model->q2 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q2 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">3</td>
    <td>สุขภาพร่างกายแข็งแรง</td>
    <td align="center"><?= $model->q3 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q3 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">4</td>
    <td>อยู่คนเดียวหรือที่พักอาศัยสามารถแยกห้องเพื่ออยู่คนเดียวได้</td>
    <td align="center"><?= $model->q4 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q4 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">5</td>
    <td>ไม่มีภาวะอ้วน (bmi >30 หรือน้ำหนักเกิน 90 kg)</td>
    <td align="center"><?= $model->q5 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q5 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">6</td>
    <td>ไม่มีโรคร่วม</td>
    <td align="center"><?= $model->q6 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q6 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center"></td>
    <td>6.1 ไม่เป็นโรคปอดอุดกลั้นเรื้อรัง (COPD)</td>
    <td align="center"><?= $model->q61 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q61 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center"></td>
    <td>6.2 ไม่เป็นโรคไตเรื้อรัง (CKD Stage 3,4,5)</td>
    <td align="center"><?= $model->q62 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q62 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center"></td>
    <td>6.3 ไม่เป็นโรคหัวใจและหลอดเลือด</td>
    <td align="center"><?= $model->q63 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q63 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center"></td>
    <td>6.4 ไม่เป็นโรคหลอดเลือดสมอง</td>
    <td align="center"><?= $model->q64 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q64 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center"></td>
    <td>6.5 ไม่เป็นโรคเบาหวานที่ความคุมไม่ได้</td>
    <td align="center"><?= $model->q65 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q65 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center"></td>
    <td>6.6 โรคอื่นๆตามดุลพินิจของแพทย์</td>
    <td align="center"><?= $model->q66 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q66 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">7</td>
    <td>ยินยอมกักตัวในที่พักตนเอง</td>
    <td align="center"><?= $model->q7 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q7 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td colspan="4">ความพร้อมของสถานที่แยกกักตัว</td>
</tr>
<tr>
    <td align="center">1</td>
    <td>มีผู้จัดหาอาหารและของใช้จำเป็นให้ได้</td>
    <td align="center"><?= $model->q8 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q8 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">2</td>
    <td>มีห้องส่วนตัว(กรณี หอพัก/คอนโด ควรมีห้องน้ำส่วนตัว)</td>
    <td align="center"><?= $model->q9 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q9 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">3</td>
    <td>มีผู้อยู่อาศัยร่วมสามารถปฏิบัติตามคำแนะนำเรื่องสุขอนามัยและแยกจากผู้ป่วยได้</td>
    <td align="center"><?= $model->q10 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q10 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">4</td>
    <td>ติดต่อกับโรงพยาบาลและเดินทางมาโรงพยาบาลได้สะดวก</td>
    <td align="center"><?= $model->q11 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q11 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td colspan="4">การดำเนินการโรงพยาบาล</td>
</tr>
<tr>
    <td align="center">1</td>
    <td>ลงทะเบียนผู้ติดเชื้อ COVID-19</td>
    <td align="center"><?= $model->q12 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q12 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">2</td>
    <td>มีภาพรังสีทรวงอก (chest X-Ray):ปอดปกติ</td>
    <td align="center"><?= $model->q13 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q13 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">3</td>
    <td>มีปรอทวัดไข้ และ pulse oximeter ให้กับผู้ติดเชื้อ</td>
    <td align="center"><?= $model->q14 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q14 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">4</td>
    <td>มีระบบ/ช่องทางสื่อสารกับผู้ติดเชื้อ</td>
    <td align="center"><?= $model->q15 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q15 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">5</td>
    <td>มีช่องทางสื่อสารในกรณีฉุกเฉิน</td>
    <td align="center"><?= $model->q16 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q16 =='0' ? '/':''; ?></td>
</tr>
<tr>
    <td align="center">6</td>
    <td>มีระบบรับผู้ติดเชื้อส่งโรงพยาบาลกรณีฉุกเฉิน</td>
    <td align="center"><?= $model->q17 =='1' ? '/':''; ?></td>
    <td align="center"><?= $model->q17 =='0' ? '/':''; ?></td>
</tr>
</table>
<b>ผลการประเมิน ใช่ทุกข้อ</b> พิจารณาให้ผู้ติดเชื้อCOVID-19 แยกกักตัวในที่พักอาศัย(Home Isolation)ได้<br>
    &emsp; &emsp; &emsp;&emsp;&emsp;&emsp; <b>ไม่ใช่ ข้อใดข้อหนึ่ง</b> ควรพิจารณารับผู้ป่วยไว้รักษาที่โรงพยาบาลหรือโรงพยาบาลสนามหรือศูนย์กักตัวชุมชน(CI) หรือ Hospitel<br>
<b>โรงพยาบาลพิจารณาแล้วเห็นว่า</b> <?= $model->result =='1' ? 'ผู้ป่วยสามารถแยกกักตัวที่บ้านได้':'ผู้ป่วยไม่สามารถแยกกักตัวที่บ้านได้' ; ?><br><br>
&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;
ลงชื่อ&emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;ผู้พิจารณา<br><br>
&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;
ลงชื่อ&emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;&emsp;&emsp;&emsp;ผู้ดำเนินการสถานพยาบาล
