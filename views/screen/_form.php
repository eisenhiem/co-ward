<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

$result = [ 1 => 'ใช่', 0 => 'ไม่ใช่',];
/* @var $this yii\web\View */
/* @var $model app\models\Screen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="screen-form">

    <?php $form = ActiveForm::begin(); ?>
    <h3>เกณฑ์การพิจารณาผู้ติดเชื้อ COVID-19 เพื่อแยกตัวที่บ้าน</h3>
    <?= $form->field($model, 'q1')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q2')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q3')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q4')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q5')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q6')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q61')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q62')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q63')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q64')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q65')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q66')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q7')->radioList($result, ['prompt' => '']) ?>

    <h3>ความพร้อมของสถานที่แยกกักตัว</h3>

    <?= $form->field($model, 'q8')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q9')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q10')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q11')->radioList($result, ['prompt' => '']) ?>

    <h3>การดำเนินการโรงพยาบาล</h3>

    <?= $form->field($model, 'q12')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q13')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q14')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q15')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q16')->radioList($result, ['prompt' => '']) ?>

    <?= $form->field($model, 'q17')->radioList($result, ['prompt' => '']) ?>

    <h4><b>ผลการประเมิน ใช่ทุกข้อ</b> พิจารณาให้ผู้ติดเชื้อCOVID-19 แยกกักตัวในที่พักอาศัย(Home Isolation)ได้<br>
    &emsp; &emsp; &emsp;&emsp;&emsp;&emsp; <b>ไม่ใช่ ข้อใดข้อหนึ่ง</b> ควรพิจารณารับผู้ป่วยไว้รักษาที่โรงพยาบาลหรือโรงพยาบาลสนามหรือศูนย์กักตัวชุมชน</h4>

    <?= $form->field($model, 'result')->radioList([ 1 => 'ผู้ป่วยสามารถแยกกักตัวที่บ้านได้', 0 => 'ผู้ป่วยไม่สามารถแยกกักตัวที่บ้านได้',], ['prompt' => '']) ?>
    
    <?= $form->field($model, 'screen_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'วันที่', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวัน'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
