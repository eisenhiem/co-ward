<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScreenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="screen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'an') ?>

    <?= $form->field($model, 'q1') ?>

    <?= $form->field($model, 'q2') ?>

    <?= $form->field($model, 'q3') ?>

    <?= $form->field($model, 'q4') ?>

    <?php // echo $form->field($model, 'q5') ?>

    <?php // echo $form->field($model, 'q6') ?>

    <?php // echo $form->field($model, 'q61') ?>

    <?php // echo $form->field($model, 'q62') ?>

    <?php // echo $form->field($model, 'q63') ?>

    <?php // echo $form->field($model, 'q64') ?>

    <?php // echo $form->field($model, 'q65') ?>

    <?php // echo $form->field($model, 'q66') ?>

    <?php // echo $form->field($model, 'q7') ?>

    <?php // echo $form->field($model, 'q8') ?>

    <?php // echo $form->field($model, 'q9') ?>

    <?php // echo $form->field($model, 'q10') ?>

    <?php // echo $form->field($model, 'q11') ?>

    <?php // echo $form->field($model, 'q12') ?>

    <?php // echo $form->field($model, 'q13') ?>

    <?php // echo $form->field($model, 'q14') ?>

    <?php // echo $form->field($model, 'q15') ?>

    <?php // echo $form->field($model, 'q16') ?>

    <?php // echo $form->field($model, 'q17') ?>

    <?php // echo $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
