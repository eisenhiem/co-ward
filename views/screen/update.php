<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Screen */

$this->title = 'Update Screen: ' . $model->an;
$this->params['breadcrumbs'][] = ['label' => 'Screens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->an, 'url' => ['view', 'id' => $model->an]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="screen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
