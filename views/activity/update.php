<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LActivity */

$this->title = 'Update L Activity: ' . $model->activity_id;
$this->params['breadcrumbs'][] = ['label' => 'L Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->activity_id, 'url' => ['view', 'id' => $model->activity_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lactivity-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
