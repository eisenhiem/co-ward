<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LActivity */

$this->title = 'Create L Activity';
$this->params['breadcrumbs'][] = ['label' => 'L Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lactivity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
