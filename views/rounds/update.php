<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rounds */

$this->title = 'แก้ไขรอบวัด Vital Sign: ' . $model->round_id;
$this->params['breadcrumbs'][] = ['label' => 'รอบวัด Vital Sign', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->round_id, 'url' => ['view', 'id' => $model->round_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rounds-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
