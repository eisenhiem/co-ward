<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rounds */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rounds-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'round_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'round_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
