<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoundsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รอบวัด Vital Sign';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rounds-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มรอบวัด Vital Sign', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'round_id',
            'round_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
