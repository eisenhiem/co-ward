<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Ptsign;
use app\models\Vitalsign;

/* @var $this yii\web\View */
/* @var $model app\models\Admission */

$this->title = "AN : ".$model->an;
$this->params['breadcrumbs'][] = ['label' => 'Admissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="admission-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('กลับ', ['index','id' => $model->ward_id], ['class' => 'btn btn-danger']) ?> &emsp; 
        <?php // Vitalsign::findOne($model->an) ? Html::a('อาการแรกรับ', ['vitalsign/view', 'id' => $model->an], ['class' => 'btn btn-info']) : Html::a('บันทึกอาการแรกรับ', ['vitalsign/create', 'id' => $model->an], ['class' => 'btn btn-warning']); ?>  &emsp;
        <?php // Ptsign::findOne($model->an) ? Html::a('อาการผู้ป่วย', ['ptsign/view', 'id' => $model->an], ['class' => 'btn btn-info']) : Html::a('บันทึกอาการผู้ป่วย', ['ptsign/create', 'id' => $model->an], ['class' => 'btn btn-danger']); ?>  &emsp;
        <?= Html::a('แก้ไขข้อมูลทั่วไป', ['update', 'id' => $model->an], ['class' => 'btn btn-success']) ?>  &emsp;
        <?= Html::a('แก้ไขอาการแรกรับ', ['vitalsign/update', 'id' => $model->an], ['class' => 'btn btn-info']) ?>  &emsp;
        <?= Html::a('แก้ไขผลตรวจ', ['ptsign/update', 'id' => $model->an], ['class' => 'btn btn-warning']) ?>  &emsp;
        <?= Html::a('แก้ไข DC', ['plan/update', 'id' => $model->an], ['class' => 'btn btn-danger']) ?>  &emsp;
        <?= Html::a('พิมพ์ใบกักตัว', ['printisolate', 'id' => $model->an], ['class' => 'btn btn-warning']) ?>  &emsp;
        <?= Html::a('พิมพ์คัดกรอง', ['screen/print', 'id' => $model->an], ['class' => 'btn btn-success']) ?>  &emsp;
        <?= Html::a('พิมพ์ประวัติ', ['print', 'id' => $model->an], ['class' => 'btn btn-primary']) ?>  &emsp;
        <?= Html::a('พิมพ์ใบรับรอง', ['plan/print', 'id' => $model->an], ['class' => 'btn btn-info']) ?>  &emsp;
    </p>
    <p>
    <div class="panel panel-info">
        <div class="panel-heading">
            <u><b>Ward</u> :</b> <?= $model->ward->ward_name ?> &emsp; <b>เตียง : </b><?= $model->bed ? $model->bed:'ยังไม่ระบุเตียง' ?>
            &emsp; 
            <?php if($stage) {
                    echo Html::a('[ '.$stage->getStage().' ]', ['stage', 'id' => $model->an], $stage->getStageLevel());
                } else {
                    echo Html::a('ยังไม่ระบุ Stage', ['stage', 'id' => $model->an], ['class' => 'btn btn-primary']);
                }
                echo '&emsp; '.Html::a('ย้าย ward', ['moveward', 'id' => $model->an], ['class' => 'btn btn-primary']);
        ?> 
        </div>
        <div class="panel-body">
        <ul class="list-group">
            <li class="list-group-item list-group-item-heading list-group-item-success "><u><b>ข้อมูลทั่วไป</b></u>
            <p class="list-group-item-text">
            <b> ชื่อ-สกุล : </b><?= $model->getPatientName() ?><br>
            <b> เลขบัตรประชาชน : </b><?= $model->cid ?>&emsp;<b>Passport : </b><?= $model->passport ?><br>
        <b> เพศ : </b><?= $model->gender == '1' ? 'ชาย':'หญิง' ?>&emsp;<b> วันเกิด : </b><?php $dob= date_create($model->dob); echo date_format($dob,'j/n/Y'); ?> &emsp;  
        <b> อายุ : </b><?= $model->age ?> ปี <br>
        <b> สิทธิ์ : </b><?= $model->insureName ?><br>
        <b> Authen CODE : </b><?= $model->authen_code ?><br>
        <b> ที่อยู่ : </b><?= $model->address ?> <br>
        <b> เบอร์โทร : </b><?= $model->contact_number ?> <br>
        <b> ผู้ที่สามารถติดต่อได้ : </b><?= $model->contact_name ?><br>
        <b> Patient Ventilated : </b><?= $model->ventilaterName ?>
            <div style="color:red"><b>แพ้ยา/อาหาร : </b><?= $model->allergy ?><br>
            <b>โรคประจำตัว : </b><br>&emsp;<?= $model->chronicName ?><br>&emsp;<?= $model->other_comobit ?></div>
            <b>ยาเดิม : </b><?= nl2br($model->med_reconcile) ?><br>
            
            </p>
            </li>
            <li class="list-group-item list-group-item-heading list-group-item-info"><u><b>ข้อมูลการเจ็บป่วย</b></u>
            <p class="list-group-item-text">
            <b>Vital sign</b><br>
            <?php if($vs){
            ?>
            &emsp;<b> น้ำหนัก : </b><?= $vs->bw ?> Kg 
            &emsp;<b> ส่วนสูง : </b><?= $vs->height_cm ?> cm 
            &emsp;<b> อุณหภูมิร่างกาย : </b><?= $vs->body_temp ?> C 
            &emsp;<b> อัตราการหายใจ : </b><?= $vs->rr ?> 
            &emsp;<b> ชีพจร : </b><?= $vs->pr ?> 
            &emsp;<b> ความดันโลหิต : </b><?= $vs->sbp.'/'.$vs->dbp ?> mmHg <br>
            &emsp;<b> Oxygen Sat Candular : </b><?= $vs->o2sat ?> 
            &emsp;<b> Oxygen Sat Room Air: </b><?= $vs->o2sat_ra ?><br>
            <?php } ?>
            <b>ผล CBC</b></br>
            <?php if($sign){
            ?>
            &emsp;<b> Hematocrit : </b><?= $sign->hct ?> %
            &emsp;<b> Hemoglobin : </b><?= $sign->hb ?> g/dl
            &emsp;<b> White Blood Cell : </b><?= $sign->wbc ?> Cells/mm^3
            &emsp;<b> Platelets Count : </b><?= $sign->platelets ?><br>
            &emsp;<b> Neutrophil : </b><?= $sign->pmn ?> %
            &emsp;<b> Lymphocyte : </b><?= $sign->lym ?> % 
            &emsp;<b> Monocyte : </b><?= $sign->m ?> %
            &emsp;<b> Eosinophils : </b><?= $sign->eo ?> %
            &emsp;<b> Atyp Lymphocyte : </b><?= $sign->atyp_lym ?> %<br>
            <b> ผลตรวจอื่น ๆ : </b><?= $sign->lab_other ?><br>
            &emsp;<b> วันที่ NASAL SWAB : </b><?= $model->getThaiDate($sign->swap_date) ?>
            &emsp;<b> วิธีการตรวจ COVID : </b><?= $sign->test ?>
            &emsp;<b> ผลการตรวจ COVID : </b><?= $sign->result ?>
            &emsp;<b> วันที่ตรวจพบเชื้อ COVID-19 : </b><?= $model->getThaiDate($model->covid_register) ?><br>
            &emsp;<b> วันที่ CXR ครั้งที่ 1 : </b><?= $model->getThaiDate($sign->cxr_date) ?>
            <?php } ?>
            </p>
            </li>
            <li class="list-group-item list-group-item-heading list-group-item-warning"><u><b>ข้อมูลการรักษา</b></u>
            <p class="list-group-item-text">
            <b> วันที่ Admit : </b><?= $model->getThaiDate($model->admission_date) ?>
            &emsp;<b> วันที่ Isolate : </b><?= $model->getThaiDate($model->isolation_date) ?><br>
            <b>อาการสำคัญที่มา : </b><?= $model->cc ?><br>
            <?php if($vs){
            ?>
            <b>Patient Symptom</b><br>
            &emsp;<?= $vs->sympName ?><?= $vs->pain ? '<br>&emsp;Pain:'.$vs->painName:'' ?><?= $vs->other_symptom ? '<br>&emsp;'.$vs->other_symptom:'' ?><br>
            <b>Patient Mentality 2Q</b><br>
            &emsp;<?= $vs->getTwoQResult() ?><br>
            <?php } ?>
            <b>Patient Sign</b><br>
            <?php if($sign){
            ?>
            &emsp;<?= $sign->signName ?>&emsp;<?= $sign->other ?><br>
            <?php } ?>
            </p>            
            </li>
            <li class="list-group-item list-group-item-heading list-group-item-danger"><u><b>Plan Discharge</b></u>
            <p class="list-group-item-text">
            <?php if($dc){
            ?>
            &emsp;<b> การวินิจฉัย : </b><?= $dc->getDiagName() ?>
            &emsp;<b> วันที่จำหน่าย : </b><?= $dc->dc_date ?>
            &emsp;<b> จำหน่ายโดย : </b><?= $dc->dcName ?><br>
            &emsp;<b> สรุปผลการรักษา : </b><?= $dc->concution ?><br>
            &emsp;<b> ที่อยู่หลังจำหน่าย : </b><?= $dc->address ?>
            &emsp;<b> ผู้ติดต่อ : </b><?= $dc->contact_name ?>
            &emsp;<b> เบอร์โทร : </b><?= $dc->phone_number ?>
            <?php } ?>
            </p>            
            </li>
        </ul>

</div>
</div>
</div>
</div>
