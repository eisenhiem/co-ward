<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Admission */

$this->title = 'ระดับความรุนแรง';
$this->params['breadcrumbs'][] = ['label' => 'Admissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_stage', [
        'model' => $model,
    ]); ?>
</div>
