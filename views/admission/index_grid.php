<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoundsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลผู้ป่วย '.$ward->ward_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-index">

<h1><?= Html::encode($this->title) ?></h1>

<div style="text-align:right">
    <?= Html::a('เพิ่มคนไข้ใหม่', ['create'], ['class' => 'btn btn-success']) ?> &emsp13;
    <?= Html::a('Switch Layout', ['switchlayout','id' => $ward->ward_id], ['class' => 'btn btn-primary']) ?> &emsp; 
</div>
<br>

    <?php echo $this->render('_search', ['model' => $searchModel,'id'=>$ward->ward_id]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' =>[
          'before'=>' '
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
          'bed',
          'an',
          'hn',
          [
            'header' => 'ชื่อ-สกุล',
            'value' => function ($model) {
              return $model->getPatientName();
            },
          ],
          [
            'attribute' => 'gender',
            'value' => function ($model) {
              return $model->gender == '1' ? 'ชาย':'หญิง';
            },
          ],
          'age',
          [
            'header' => '',
            'value' => function ($model) {
              return 'Day '.$model->getHealDay();
            },
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Stage',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{stage}',
            'buttons'=>[
              'stage' => function($url,$model,$key){
                return $model->stage ? Html::a('[ '.$model->stage->getStage().' ]', ['stage', 'id' => $model->an], $model->stage->getStageLevel()):Html::a('ยังไม่ระบุ Stage', ['stage', 'id' => $model->an], ['class' => 'btn btn-primary']);
              },
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Ward',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{move}',
            'buttons'=>[
              'move' => function($url,$model,$key){
                return Html::a('ย้าย Ward', ['moveward', 'id' => $model->an], ['class' => 'btn btn-primary']);
              },
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'ดูข้อมูล',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{view}',
            'buttons'=>[
              'view' => function($url,$model,$key){
                return Html::a('ดูข้อมูล', ['view','id'=>$model->an], ['class' => 'btn btn-info','style' =>['width'=>'100px']]) ;
              }
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'ดูข้อมูล',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{order}',
            'buttons'=>[
              'order' => function($url,$model,$key){
                return Html::a('Doctor Order', ['orders/detail','id'=>$model->an], ['class' => 'btn btn-success','style' =>['width'=>'100px']]) ;
              }
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'ดูข้อมูล',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{note}',
            'buttons'=>[
              'note' => function($url,$model,$key){
                return Html::a('Nurse Note', ['notes/detail','id'=>$model->an], ['class' => 'btn btn-warning','style' =>['width'=>'100px']]) ;
              }
            ]
          ],
        ],
    ]); ?>


</div>
