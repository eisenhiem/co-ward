<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Ward;
use yii\helpers\ArrayHelper;

$ward = ArrayHelper::map(Ward::find()->all(), 'ward_id', 'ward_name');

/* @var $this yii\web\View */
/* @var $model app\models\Admission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admission-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'an')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'hn')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'pname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'fname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'lname')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'gender')->radioList([ 2 => 'หญิง', 1 => 'ชาย', ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'cid')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'dob')->widget(DatePicker::ClassName(),
                [
                    'name' => 'วันเดือนปีเกิด', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันเกิด'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'age')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'contact_number')->textInput(['maxlength' => true]) ?>        
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'authen_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'address')->textarea(['rows' => 2]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'insure_name')->radioList($model->getItemInsure()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'insure_other')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'sick_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'วันที่เริ่มมีอาการ/เริ่มป่วย', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันที่เริ่มมีอาการ'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'covid_register')->widget(DatePicker::ClassName(),
                [
                    'name' => 'วันที่ตรวจพบเชื้อ Covid', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันที่พบเชื้อ'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'admission_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'วันที่ admit', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวัน admit'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'isolation_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'วันที่ Isolate', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวัน Isolate'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'ventilated')->radioList($model->getItemVen()) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'ventilated_other')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'asymptomatic')->radioList($model->getItemAsymp()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'ward_id')->dropDownList([$ward]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'bed')->textInput() ?>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6">
            <?= $form->field($model, 'cc')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'med_reconcile')->textarea(['rows' => 3]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'phistory')->checkboxList($model->getItemChronic(),['item' => function($index, $label, $name, $checked, $value) {
            return "<label class='col-md-6'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
        }]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'allergy')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'other_comobit')->textInput() ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
