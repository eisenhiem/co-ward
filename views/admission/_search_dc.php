<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdmissionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admission-search">

    <?php $form = ActiveForm::begin([
        'action' => ['discharge'],
        'method' => 'get',
    ]); ?>
<div class="row">
    <div class="col-md-2"><?= $form->field($model, 'bed') ?></div>

    <div class="col-md-2"><?= $form->field($model, 'an') ?></div>

    <div class="col-md-2"><?= $form->field($model, 'hn') ?></div>

    <div class="col-md-3"><?= $form->field($model, 'fname') ?></div>

    <div class="col-md-3"><?= $form->field($model, 'lname') ?></div>
</div>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
