<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลผู้ป่วยจำหน่าย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-dc">

    <h1><?= Html::encode($this->title) ?></h1>
    <div style="text-align:right">
        <?= Html::a('Switch Layout', ['switchlayout','id' => 'dc'], ['class' => 'btn btn-primary']) ?> &emsp; 
    </div>
    <br>

    <?php
    foreach($model as $patient){
?>
    <div class="col-md-4">
    <div <?= $patient->gender == '1' ? 'class="panel panel-primary"':'class="panel panel-danger"' ?>>
            <div class="panel-heading"><b>AN <?= $patient->an ?></b><?= Html::a('ยกเลิก D/C', ['canceldc','id'=>$model->an], ['class' => 'btn btn-danger','style' =>['width'=>'100px']]) ?></div>
            <div class="panel-body">
                <b>HN : <?= $patient->hn ?> </b> <br>
                <b>ชื่อ-สกุล :</b> <?= $patient->getPatientName() ?>
                <br>
                <b>เลขบัตรประชาชน :</b> <?= $patient->cid ?> 
                <br> 
                <b>เพศ : </b><?= $patient->gender == '1' ? 'ชาย':'หญิง' ?> &emsp; <b>อายุ : </b><?= $patient->age ?> ปี
                </br><br>
                <?= Html::a('ดูข้อมูล', ['view','id'=>$patient->an], ['class' => 'btn btn-info','style' =>['width'=>'100px']]) ?> 
                <?= Html::a('Doctor Order', ['orders/detail','id'=>$patient->an], ['class' => 'btn btn-success','style' =>['width'=>'100px']]) ?> 
                <?= Html::a('Nurse Note', ['notes/detail','id'=>$patient->an], ['class' => 'btn btn-warning','style' =>['width'=>'100px']]) ?>
            </div>
        </div>
    </div>
<?php
    }
?>
</div>
