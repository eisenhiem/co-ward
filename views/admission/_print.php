<?php
use app\models\Office;

$hos = Office::find()->one();

?>


<h4 align="center">Home Ward Chart<br><?= $hos->hospname ?> <?= $hos->address ?> <?= $hos->zipcode ?></h4>
<b>Ward :</b><?= $model->ward->ward_name ?>&emsp;<b>เตียง : </b><?= $model->bed ? $model->bed:'ยังไม่ระบุเตียง' ?><br>
<u><b>ข้อมูลทั่วไป</b></u><br>
<b>HN: </b><?= $model->hn ?> <b>AN: </b><?= $model->an ?> <b>สิทธิ์การรักษา: </b><?= $model->insureName ?><b> เลขบัตรประชาชน: </b><?= $model->getThaiCid() ?>
<br><b> ชื่อ-สกุล: </b><?= $model->getPatientName() ?> 
<b> เพศ : </b><?= $model->gender == '1' ? 'ชาย':'หญิง' ?>
&emsp;<b> วันเกิด: </b><?= $model->getFullThaiDate($model->dob); ?> 
&emsp;<b> อายุ: </b><?= $model->age ?> ปี<br>
<b>ที่อยู่ : </b><?= $model->address ?> 
<b>เบอร์โทร : </b><?= $model->contact_number ?><br>
<b>ผู้ที่สามารถติดต่อได้ : </b><?= $model->contact_name ?><br>
<b>วันที่ Admit: </b><?= $model->getThaiDate($model->admission_date) ?> 
<b>วันที่กักตัว: </b><?= $model->getThaiDate($model->isolation_date) ?> 
<b>การวินิจฉัย: </b><?= $dc->getDiagName() ?><br>
<div style="color:red"><b>แพ้ยา/อาหาร: </b><?= $model->allergy ? $model->allergy:'ปฏิเสธแพ้ยา/แพ้อาหาร'; ?></div>
<b>โรคประจำตัว: </b>&emsp;<?= $model->getChronicName() ?>&emsp;<?= $model->other_comobit ?><br>
<b>ยาเดิม: </b><?= nl2br($model->med_reconcile) ?><br>
<b>อาการแรกรับ</b>
<b>วันที่เริ่มป่วย </b><?= $model->getThaiDate($model->sick_date) ?> 
<b>อาการสำคัญที่มา: </b><?= $model->cc ?><br>
<?php if($vs){ ?>
<b>อาการปัจจุบัน: </b><?= $vs->sympName ?><?= $vs->pain ? '<br>&emsp;Pain:'.$vs->painName:'' ?><?= $vs->other_symptom ? '<br>&emsp;'.$vs->other_symptom:'' ?><br>
<b>ประเมิน 2Q: </b><?= $vs->getTwoQResult() ?><br>
<b> น้ำหนัก: </b><?= $vs->bw ?> Kg, 
<b> ส่วนสูง: </b><?= $vs->height_cm ?> cm, 
<b> BT: </b><?= $vs->body_temp ?> ํC, 
<b> RR: </b><?= $vs->rr ?>/min, 
<b> PR: </b><?= $vs->pr ?>/min, 
<b> BP: </b><?= $vs->sbp.'/'.$vs->dbp ?> mmHg <br>
<b> O2Sat Candular: </b><?= $vs->o2sat ?> 
<b> O2Sat Room Air: </b><?= $vs->o2sat_ra ?><br>
<?php } ?>
<?php if($sign){ ?>
<b>วันที่ NASAL SWAB : </b><?= $model->getThaiDate($sign->swap_date) ?>
<b>วิธีการตรวจ COVID : </b><?= $sign->test ?>
<b>ผลการตรวจ COVID : </b><?= $sign->result ?><br>
<b>วันที่ตรวจพบเชื้อ COVID-19 : </b><?= $model->getThaiDate($model->covid_register) ?>
<b>วันที่ CXR ครั้งที่ 1 : </b><?= $model->getThaiDate($sign->cxr_date) ?><br>
<b>อาการอื่นๆ: </b><?= $sign->signName ?>&emsp;<?= $sign->other ?><br>
<?php } ?>
<u><b>แผนการจำหน่าย</b></u><br>
<?php if($dc){ ?>
<b> วันที่จำหน่าย: </b><?= $model->getThaiDate($dc->dc_date) ?>
<b> จำหน่ายโดย: </b><?= $dc->dcName ?><br>
<b> สรุปผลการรักษา: </b><?= $dc->concution ?><br>
<b> ที่อยู่หลังจำหน่าย: </b><?= $dc->address ?><br>
<b> ผู้ติดต่อ: </b><?= $dc->contact_name ?> 
<b> เบอร์โทร: </b><?= $dc->phone_number ?>
<?php } ?>
