<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Ward;
use yii\helpers\ArrayHelper;

$ward = ArrayHelper::map(Ward::find()->where(['is_active' => '1'])->all(), 'ward_id', 'ward_name');

/* @var $this yii\web\View */
/* @var $model app\models\Admission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admission-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ward_id')->dropDownList([$ward]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
