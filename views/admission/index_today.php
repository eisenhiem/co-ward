<?php

use yii\helpers\Html;
use app\models\PtStage;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div style="text-align:right">
        <?php // Html::a('Switch Layout', ['indexgrid','id' => $ward->ward_id], ['class' => 'btn btn-primary']) ?> &emsp; 
    </div>
    <br>

<?php
    foreach($model as $patient){
        $stage = PtStage::findOne($patient->an);
?>
    <div class="col-md-4">
    <div <?= $patient->gender == '1' ? 'class="panel panel-primary"':'class="panel panel-danger"' ?>>
            <div class="panel-heading"><b>AN <?= $patient->an ?>
            &emsp;
                <?php if($stage) {
                    echo Html::a('[ '.$stage->getStage().' ]', ['stage', 'id' => $patient->an], $stage->getStageLevel());
                } else {
                    echo Html::a('ยังไม่ระบุ Stage', ['stage', 'id' => $patient->an], ['class' => 'btn btn-primary']);
                }
                ?> 
            <div style="text-align:right">เตียง <?= $patient->bed ?$patient->bed:'ยังไม่ระบุเตียง' ?></div></b></div>
            <div class="panel-body">
                <b>HN : <?= $patient->hn ?> </b> <br>
                <b>ชื่อ-สกุล :</b> <?= $patient->getPatientName() ?>
                <br>
                <b>เลขบัตรประชาชน :</b> <?= $patient->cid ?> 
                <br> 
                <b>เพศ : </b><?= $patient->gender == '1' ? 'ชาย':'หญิง' ?> &emsp; <b>อายุ : </b><?= $patient->age ?> ปี
                <br>
                <?= Html::a('ดูข้อมูล', ['view','id'=>$patient->an], ['class' => 'btn btn-info','style' =>['width'=>'100px']]) ?> 
                <?= Html::a('Doctor Order', ['orders/detail','id'=>$patient->an], ['class' => 'btn btn-success','style' =>['width'=>'100px']]) ?> 
                <?= Html::a('Nurse Note', ['notes/detail','id'=>$patient->an], ['class' => 'btn btn-warning','style' =>['width'=>'100px']]) ?>
            </div>
        </div>
    </div>
<?php
    }
?>
</div>
