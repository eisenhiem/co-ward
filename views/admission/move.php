<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Admission */

$this->title = 'ย้ายผู้ป่วยไป Ward ใหม่';
$this->params['breadcrumbs'][] = ['label' => 'Admissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_move', [
        'model' => $model,
    ]); ?>
</div>
