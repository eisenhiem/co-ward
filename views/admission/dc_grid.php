<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoundsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลผู้ป่วยจำหน่าย ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-dc">

<h1><?= Html::encode($this->title) ?></h1>

<div style="text-align:right">
    <?= Html::a('Switch Layout', ['switchlayout','id' => 'dc'], ['class' => 'btn btn-primary']) ?> &emsp; 
</div>
<br>

    <?php echo $this->render('_search_dc', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' =>[
          'before'=>' '
        ],

        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
          // 'bed',
          'an',
          'hn',
          [
            'header' => 'ชื่อ-สกุล',
            'value' => function ($model) {
              return $model->getPatientName();
            },
          ],
          [
            'attribute' => 'gender',
            'value' => function ($model) {
              return $model->gender == '1' ? 'ชาย':'หญิง';
            },
          ],
          'age',
/*
          [
            'header' => 'วัน Swab',
            'value' => function ($model) {
              return $model->getThaiDate($model->ptsign->swap_date);
            },
          ],
*/
          [
            'attribute' => 'admission_date',
            'value' => function ($model) {
              return $model->getThaiDate($model->admission_date);
            },
          ],          
          [
            'header' => 'วันจำหน่าย',
            'value' => function ($model) {
              return $model->getThaiDate($model->plandc->dc_date);
            },
          ],          
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Stage',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{stage}',
            'buttons'=>[
              'stage' => function($url,$model,$key){
                return $model->stage ? Html::a('[ '.$model->stage->getStage().' ]', ['stage', 'id' => $model->an], $model->stage->getStageLevel()):Html::a('ยังไม่ระบุ Stage', ['stage', 'id' => $model->an], ['class' => 'btn btn-primary']);
              },
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'ดูข้อมูล',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{view}',
            'buttons'=>[
              'view' => function($url,$model,$key){
                return Html::a('ดูข้อมูล', ['view','id'=>$model->an], ['class' => 'btn btn-info','style' =>['width'=>'100px']]) ;
              }
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'ดูข้อมูล',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{order}',
            'buttons'=>[
              'order' => function($url,$model,$key){
                return Html::a('Doctor Order', ['orders/detail','id'=>$model->an], ['class' => 'btn btn-success','style' =>['width'=>'100px']]) ;
              }
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'ดูข้อมูล',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-primary'],
            'template'=>'{note}',
            'buttons'=>[
              'note' => function($url,$model,$key){
                return Html::a('Nurse Note', ['notes/detail','id'=>$model->an], ['class' => 'btn btn-warning','style' =>['width'=>'100px']]) ;
              }
            ]
          ],
          [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'ยกเลิก DC',
            'options'=>['style'=>'width:120px;'],
            'buttonOptions'=>['class'=>'btn btn-danger'],
            'template'=>'{cancel}',
            'buttons'=>[
              'cancel' => function($url,$model,$key){
                return Html::a('ยกเลิก', ['canceldc','id'=>$model->an], ['class' => 'btn btn-danger','style' =>['width'=>'100px']]) ;
              }
            ]
          ],
        ],
    ]); ?>


</div>
