<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Admission */

$this->title = 'Discharge: ' . $model->an;
$this->params['breadcrumbs'][] = ['label' => 'Admissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->an, 'url' => ['view', 'id' => $model->an]];
$this->params['breadcrumbs'][] = 'D/C';
?>
<div class="admission-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
