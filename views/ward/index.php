<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ward-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่ม Ward', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ward_id',
            'ward_name',
            'type.ward_type_name',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Status',
                'options'=>['style'=>'width:120px;'],
                'template'=>'{change}',
                'buttons'=>[
                  'change' => function($url,$model,$key){
                    return $model->is_active == '1' ? Html::a('เปิดใช้งาน', ['change', 'id' => $model->ward_id], ['class' => 'btn btn-success']):Html::a('ปิดใช้งาน', ['change', 'id' => $model->ward_id], ['class' => 'btn btn-danger']);
                  },
                ]
            ],    
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Html::a('แก้ไข',['update','id'=>$model->ward_id],['class' => 'btn btn-primary']);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
