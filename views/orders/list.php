<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Admission;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Doctor Order AN:'.$an.' ชื่อ-สกุล:'.$ward->getPatientName();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= !$ward->dsch ? Html::a('เพิ่ม Order', ['create','id'=>$an], ['class' => 'btn btn-success']):'' ?>
        &emsp; 
        <?= $order->config_value == 'asc' ? Html::a('เรียง Z > A', ['switchsort','id' => $an], ['class' => 'btn btn-primary']):Html::a('เรียง A > Z', ['switchsort','id' => $an], ['class' => 'btn btn-primary']); ?> 
        &emsp; 
        <?= Html::a('Vital Sign', ['notes/vitalsign','id'=>$an], ['class' => 'btn btn-info']) ?>
        &emsp; 
        <?= Html::a('ดูบันทึกทางการพยาบาล ', ['notes/detail','id'=>$an], ['class' => 'btn btn-info']) ?>
        &emsp; 
        <?= Html::a('พิมพ์ Doctor Order', ['print','id'=>$an], ['class' => 'btn btn-warning']) ?>
        &emsp; &emsp; 
        <?= Html::a('กลับไปหน้าหลัก', ['admission/index','id'=>$ward->ward_id], ['class' => 'btn btn-danger']) ?>
    </p>

<table width="100%" border="1">
<tr>
<th width="20%" style="text-align:center">Progress Note</th>
<th width="35%" style="text-align:center">Order One Day</th>
<th width="35%" style="text-align:center">Order Continue</th>
<th width="10%" style="text-align:center">หมายเหตุ</th>
</tr>
<?php
if($model){
    foreach($model as $order){
?>
<tr>
<td style="vertical-align:top">
&emsp; <?= Admission::getThaiDateTime($order->order_datetime) ?><br>
&emsp;<?= $order->progress ? $order->getProgName():''; ?><br>
&emsp; <?= nl2br($order->progress_note) ?><br><br>
&emsp; <?= $order->profile->fullname ?><br>
&emsp; <?= $order->profile->position ?><br>
&emsp; <?= $order->profile->license_no ?><br>
</td>
<td style="vertical-align:top">
&emsp;<?= $order->oneday_list ? $order->getOrderName():''; ?><br>
<?= nl2br($order->order_oneday) ?></td>
<td style="vertical-align:top">
<?= $order->food == '2' ? '&emsp; [ / ] Soft Diet <br>' : '' ?>
<?= $order->food == '1' ? '&emsp; [ / ] Regular Diet <br>':'' ?>
<?= $order->record_i_o ? '&emsp; [ / ] Record I/O <br>' : '' ?> 
<?= $order->record_vitalsign ? '&emsp; [ / ] Record V/S <br>' : '' ?> 
<?= $order->medName ?  '<u>Medication</u><br>&emsp;'.$order->medName .'<br>':'' ?>
<?= $order->order_continue? '<u>Order เพิ่มเติม</u> <br>'.nl2br($order->order_continue):'' ?>
</td>
<td style="vertical-align:top">
    &ensp;<?= ((Yii::$app->user->identity->id == $order->user_id ) && (!$ward->dsch)) ? Html::a('แก้ไข Order', ['update', 'id' => $order->order_id], ['class' => 'btn btn-warning']):'' ?><br>
    &ensp;<?= Html::a('พิมพ์ Order', ['order', 'id' => $order->order_id], ['class' => 'btn btn-info']) ?><br><br />
    <?= $order->comment ?>
</td>
</tr>
<?php
    }
}
?>
</table>
<br>
<div style="text-align:center"> 
        <?= !$ward->dsch ? Html::a('Discharge คนไข้', ['admission/dc','id' => $an], ['class' => 'btn btn-danger','style'=>'width:200px']):'' ?>
</div>
</div>
