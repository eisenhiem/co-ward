<?php
use yii\helpers\Html;
use app\models\Admission;

function thaimonth($m){
  switch($m){
    case '01':$thainame = 'มกราคม';break;
    case '02':$thainame = 'กุมภาพันธ์';break;
    case '03':$thainame = 'มีนาคม';break;
    case '04':$thainame = 'เมษายน';break;
    case '05':$thainame = 'พฤษภาคม';break;
    case '06':$thainame = 'มิถุนายน';break;
    case '07':$thainame = 'กรกฎาคม';break;
    case '08':$thainame = 'สิงหาคม';break;
    case '09':$thainame = 'กันยายน';break;
    case '10':$thainame = 'ตุลาคม';break;
    case '11':$thainame = 'พฤศจิกายน';break;
    case '12':$thainame = 'ธันวาคม';break;
  }  
  return $thainame;
}
?>
<?php 
  $page=1;
  $row = 0;
  foreach($model as $order) {
    if($page <> 1 && $row%2 == 1 && $row > 2 || $row ==1 ){
      echo '<pagebreak>';
      $page++;
    }
    $row++;
    // echo '- '.$page.' - '.$row;
?>
<?php 
if($page == 1 || $row%2 == 0) {
?>
  <h3 align="center"><?= $office->hospname ?><br><?= $office->address ?> <?= $office->zipcode ?>
  <br>Doctor Orders Sheet & Progress Note</h3>  
<?php 
}
?>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
  <tr>
<?php 
  if($page == 1 || $row%2 == 0){
?>
    <td width="35%"><b>&emsp; ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp;<?= $pt->getPatientName() ?></td>
    <td width="25%"><b>&emsp;Ward </b> <?= $order->ward->ward_name ?><br>&emsp; <b>Bed</b> <?= $pt->bed ?></td>
    <td width="20%">&emsp; <b>อายุ</b> <?= $pt->age ?> ปี<br>&emsp; <b>น้ำหนัก </b> <?= $pt->vitalsign->bw ?> กก.</td>
    <td width="20%">&emsp; <b>HN:</b> <?= $pt->hn ?><br>&emsp; <b>AN:</b> <?= $pt->an ?></td>
<?php 
  } else {
    echo '<td colspan="4"><b>&emsp;Ward </b>'.$order->ward->ward_name.' &emsp; <b>Bed</b> '.$pt->bed.'</td>';
  } 
?>
  </tr>
</table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
<tr>
  <th width="25%">Progress Note</th>
  <th width="25%">Order One Day</th>
  <th width="35%">Order Continue</th>
  <th width="15%">หมายเหตุ</th>
</tr>
<tr>
<?php if($row == 1){
  echo '<td height=720 valign=top>';
}else{
  echo '<td height=350 valign=top>';
}
?>
&emsp;<?= Admission::getThaiDateTime($order->order_datetime) ?><br>
&emsp;<?= $order->progress ? $order->getProgName():''; ?><br>
&emsp;<?= nl2br($order->progress_note) ?><br>
&emsp;&emsp;<?= $row === 1 ?  Html::img(Yii::getAlias('@app').'/web/images/lung_clear.png', ['width' => 100]):''; ?><br>
<?= $order->profile->fullname ?><br>
<?= $order->profile->position ?><br>
<?= $order->profile->license_no ?>
<td valign=top>
&emsp;<?= $order->oneday_list ? $order->getOrderName():''; ?><br>
<?= nl2br($order->order_oneday) ?>
<td valign=top>
<?= $order->food == '2' ? '&emsp; [ / ] Soft Diet <br>' : '' ?>
<?= $order->food == '1' ? '&emsp; [ / ] Regular Diet <br>':'' ?>
<?= $order->record_i_o ? '&emsp; [ / ] Record I/O <br>' : '' ?> 
<?= $order->record_vitalsign ? '&emsp; [ / ] Record V/S <br>' : '' ?> 
<?= $order->medName ?  '<u>Medication</u><br>&emsp;'.$order->medName .'<br>':'' ?>
<?= $order->order_continue? '<u>Order เพิ่มเติม</u> <br>'.nl2br($order->order_continue):'' ?>
</td>
<td style="vertical-align:top">
<?= $order->comment ?>
</td>
</tr>
</table>
<?php 

} ?>
