<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'progress')->checkboxList($model->getOrderProg()) ?>
        </div>    
        <div class="col-md-2">
            <?= $form->field($model, 'progress_note')->textarea(['rows' => 20]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'oneday_list')->checkboxList($model->getOrderOneday()) ?>
            <?= $form->field($model, 'order_oneday')->textarea(['rows' => 10]) ?>
        </div>
        <div class="col-md-3">
            <b>Order Continue (Standing Order)</b>
            <p>
            <?= $form->field($model, 'food')->radioList($model->getItemFood()) ?>
            <?= $form->field($model, 'record_i_o')->checkbox() ?>
            <?= $form->field($model, 'record_vitalsign')->checkbox() ?>
            <?= $form->field($model, 'medication')->checkboxList($model->getOrderMed()) ?>
            </p>    
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'order_continue')->textarea(['rows' => 20]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 2]) ?>    
        </div>
    </div>
    <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr><td valign="top"><b>หมายเหตุ</b></td>
    <td>1.ให้มีการบันทึก Progress Note ใน 3 วันแรกของการรักษานรูปแบบ SOAP หากไม่เขียน SOAP กำกับ ขอให้บันทึกในรูปแบบ SOAP<br>
    2.ให้มีการบันทึกทุกครั้งที่มีการเปลี่ยนแปลงแพทย์ที่ดูแล การรักษา การให้ยา การทำ Invasive Procedure และการประเมินผลและแปรผล<br>
    3.บันทึกที่ลายมือที่อ่านออกได้ง่าย และเซ็นชื่อกำกับทุกครั้งโดยให้สามารถระบุได้ว่าเป็นแพทย์ท่านใด<br>
    4.พยาบาลลงลายมือชื่อและตำแหน่งที่สามารถอ่านออกได้ทุกครั้งขณะลงนามในแบบฟอร์มการตรวจของแพทย์พร้อมระบุนเวลาที่รับคำสั่ง
    </td></tr>
    </table>
    <br>
    <div class="form-group">
        <?= Html::submitButton('บันทึก Order', ['class' => 'btn btn-success','style'=>'width:200px']) ?>
        &emsp; 
        <?= Html::a('กลับไปหน้าหลัก', ['detail','id'=>$model->an], ['class' => 'btn btn-danger','style'=>'width:200px']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
