<?php
use yii\helpers\Html;
use app\models\Admission;

?>
<h3 align="center"><?= $office->hospname ?>
<br><?= $office->address ?> <?= $office->zipcode ?><br>
Doctor Orders Sheet & Progress Note</h3>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
  <tr>
    <td width="35%"><b>&emsp; ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp;<?= $pt->getPatientName() ?></td>
    <td width="25%"><b>&emsp;Ward </b> <?= $order->ward->ward_name ?><br>&emsp; <b>Bed</b> <?= $pt->bed ?></td>
    <td width="20%">&emsp; <b>อายุ</b> <?= $pt->age ?> ปี<br>&emsp; <b>น้ำหนัก </b> <?= $pt->vitalsign->bw ?> กก.</td>
    <td width="20%">&emsp; <b>HN:</b> <?= $pt->hn ?><br>&emsp; <b>AN:</b> <?= $pt->an ?></td>
  </tr>
</table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
<tr>
  <th width="24%">Progress Note</th>
  <th width="28%">Order One Day</th>
  <th width="28%">Order Continue</th>
  <th width="20%">หมายเหตุ</th>
</tr>
<tr><td height=700 valign=top>
&emsp; <?= Admission::getThaiDateTime($order->order_datetime) ?><br>

&emsp; <?= $order->progress_note ?><br>
&emsp; <?= $order->progress ? $order->getProgName():''; ?><br><br><br><br />
&emsp; <?= $order->profile->fullname ?><br>
&emsp; <?= $order->profile->position ?><br>
&emsp; <?= $order->profile->license_no ?><br>
<td valign=top>
&emsp;<?= $order->oneday_list ? $order->getOrderName():''; ?><br>
<?= $order->order_oneday ?>
<td valign=top>
<?= $order->food == '2' ? '&emsp; [ / ] Soft Diet <br>' : '' ?>
<?= $order->food == '1' ? '&emsp; [ / ] Regular Diet <br>':'' ?>
<?= $order->record_i_o ? '&emsp; [ / ] Record I/O <br>' : '' ?> 
<?= $order->record_vitalsign ? '&emsp; [ / ] Record V/S <br>' : '' ?> 
<u>Medication</u><br>
&emsp;<?= $order->medName ?><br>
<u>Order เพิ่มเติม</u> <br>
<?= nl2br($order->order_continue) ?>
</td>
<td style="vertical-align:top">
<?= $order->comment ?>
</td>
</tr>
</table>
<hr>
