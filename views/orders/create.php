<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Doctor Orders : '.$model->an.' ชื่อ-สกุล:'.$model->getPatientName();
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $total >=1 ? 
     $this->render('_form2', [
        'model' => $model,
    ]):
    $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
