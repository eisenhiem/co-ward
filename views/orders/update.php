<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'แก้ไข Order : ' . $model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_id, 'url' => ['view', 'id' => $model->order_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="orders-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $total >=1 ? 
     $this->render('_form2', [
        'model' => $model,
    ]):
    $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
