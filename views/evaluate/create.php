<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LEvaluate */

$this->title = 'Create L Evaluate';
$this->params['breadcrumbs'][] = ['label' => 'L Evaluates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="levaluate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
