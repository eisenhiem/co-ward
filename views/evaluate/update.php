<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LEvaluate */

$this->title = 'Update L Evaluate: ' . $model->evaluate_id;
$this->params['breadcrumbs'][] = ['label' => 'L Evaluates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->evaluate_id, 'url' => ['view', 'id' => $model->evaluate_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="levaluate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
