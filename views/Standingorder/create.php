<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StandingOrder */

$this->title = 'เพิ่มรายการ Standing Order';
$this->params['breadcrumbs'][] = ['label' => 'Standing Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standing-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
