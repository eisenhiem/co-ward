<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StandingOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="standing-order-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'med_id')->textInput(['maxlength' => true])->hint('ใส่รหัสได้ 5 ตัวอักษร เช่น Para') ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'medicine')->textInput(['maxlength' => true])->hint('Paracetamol 500mg 2 tab oral prn') ?>   
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'is_active')->radioList(['1' => 'Active','0'=>'Disable']) ?>
        </div>
    </div>

    <br>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
