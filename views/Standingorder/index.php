<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StandingOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Standing Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standing-order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มรายการ Standing Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'med_id',
            'medicine',
            [
                'header' => 'สถานะ',
                'value' => function ($model) {
                    if ($model->is_active == '1') {
                        return Html::a('Active', ['change', 'id' => $model->med_id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                        ]);
                    } else {
                        return Html::a('Disable', ['change', 'id' => $model->med_id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                        ]);
                    }
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Html::a('แก้ไข',['standingorder/update','id'=>$model->med_id],['class' => 'btn btn-primary']);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
