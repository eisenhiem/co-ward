<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Profile;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มผู้ใช้งาน', ['user/admin/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            'profile.fullname',
            'email',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'ประเมินตนเอง',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{profile}',
                'buttons'=>[
                    'profile' => function($url,$model,$key){
                      return !Profile::find()->where(['user_id'=>$model->id])->all() ? Html::a('เพิ่มรายละเอียด',['profile/create','id'=>$model->id],['class' => 'btn btn-danger']): Html::a('ปรับปรุง',['profile/update','id'=>$model->id],['class' => 'btn btn-success']) ;
                    }
                ]
            ],
        ],
    ]); ?>


</div>
