<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LineNotify */

$this->title = 'เพิ่ม Line Notify Token';
$this->params['breadcrumbs'][] = ['label' => 'Line Notifies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="line-notify-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
