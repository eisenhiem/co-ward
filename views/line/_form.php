<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LineNotify */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="line-notify-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'line_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'line_token')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_active')->radioList(['1' => 'Active','0'=>'Disable' ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
