<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LineNotifySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Line Notifies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="line-notify-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่ม Line Notify Token', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'line_id',
            'line_name',
            'line_token:ntext',
            [
                'header' => 'สถานะ',
                'value' => function ($model) {
                    if ($model->is_active == '1') {
                        return Html::a('Active', ['change', 'id' => $model->line_id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                        ]);
                    } else {
                        return Html::a('Disable', ['change', 'id' => $model->line_id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                        ]);
                    }
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Html::a('แก้ไข',['standingorder/update','id'=>$model->line_id],['class' => 'btn btn-primary']);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
