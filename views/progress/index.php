<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LProgressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Progress Note';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lprogress-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่ม Progress Note', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'progress_id',
            'progress_name',
            [
                'header' => 'สถานะ',
                'value' => function ($model) {
                    if ($model->is_active == '1') {
                        return Html::a('Active', ['change', 'id' => $model->progress_id], [
                            'class' => 'btn btn-xs btn-success btn-block',
                            'data-method' => 'post',
                        ]);
                    } else {
                        return Html::a('Disable', ['change', 'id' => $model->progress_id], [
                            'class' => 'btn btn-xs btn-danger btn-block',
                            'data-method' => 'post',
                        ]);
                    }
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'แก้ไข',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                  'update' => function($url,$model,$key){
                    return Html::a('ปรับปรุง', ['update','id'=>$model->progress_id], ['class' => 'btn btn-info','style' =>['width'=>'100px']]) ;
                  }
                ]
            ],
        ],
    ]); ?>


</div>
