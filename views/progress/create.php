<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LProgress */

$this->title = 'เพิ่ม Progress Note';
$this->params['breadcrumbs'][] = ['label' => 'Progress Note', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lprogress-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
