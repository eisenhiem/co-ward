<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LProgress */

$this->title = 'แก้ไข Progress Note: ' . $model->progress_id;
$this->params['breadcrumbs'][] = ['label' => 'Progress Note', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->progress_id, 'url' => ['view', 'id' => $model->progress_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lprogress-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
