<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ptsign */

$this->title = 'Patient Sign';
$this->params['breadcrumbs'][] = ['label' => 'Ptsigns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ptsign-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
