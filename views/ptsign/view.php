<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ptsign */

$this->title = $model->an;
$this->params['breadcrumbs'][] = ['label' => 'Ptsigns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ptsign-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->an], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->an], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'an',
            'sign',
            'other',
            'wbc',
            'pmn',
            'lym',
            'm',
            'eo',
            'atyp_lym',
            'hct',
            'platelets',
            'cxr_date',
            'swap_date',
            'covid',
        ],
    ]) ?>

</div>
