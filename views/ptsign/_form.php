<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Ptsign */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ptsign-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sign')->checkboxList($model->getItemSign(),['item' => function($index, $label, $name, $checked, $value) {
            return "<label class='col-md-4'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
        }]) ?>

    <?= $form->field($model, 'other')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'cxr_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'วันที่ CXR', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวัน CXR'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'swap_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'วันที่ Swab', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวัน Swab'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'covid_test')->radioList($model->getItemCovidTest()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'covid')->radioList($model->getItemCovid()) ?>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading">ผล CBC</div>
        <div class="panel-body">
        <div class="col-md-3">
        <?= $form->field($model, 'hct')->textInput() ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'hb')->textInput() ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'wbc')->textInput() ?>        
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'platelets')->textInput() ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'pmn')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'lym')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'm')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'eo')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'atyp_lym')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-9">
        <?= $form->field($model, 'lab_other')->textInput(['maxlength' => true]) ?>
        </div>
        </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('บันทึก Patient Sign', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
