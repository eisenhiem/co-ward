<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PtsignSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ptsign-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'an') ?>

    <?= $form->field($model, 'sign') ?>

    <?= $form->field($model, 'other') ?>

    <?= $form->field($model, 'wbc') ?>

    <?= $form->field($model, 'pmn') ?>

    <?php // echo $form->field($model, 'lym') ?>

    <?php // echo $form->field($model, 'm') ?>

    <?php // echo $form->field($model, 'eo') ?>

    <?php // echo $form->field($model, 'atyp_lym') ?>

    <?php // echo $form->field($model, 'hct') ?>

    <?php // echo $form->field($model, 'platelets') ?>

    <?php // echo $form->field($model, 'cxr_date') ?>

    <?php // echo $form->field($model, 'swap_date') ?>

    <?php // echo $form->field($model, 'covid') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
