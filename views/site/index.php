<?php

use yii\helpers\Html;
use app\models\Admission;
use app\models\PlanDc;
use kartik\grid\GridView;

$admit = Admission::find()->where(['admission_date'=>date('y-m-d')])->count();
$admit_yesterday = Admission::find()->where(['admission_date'=>date('Y-m-d', strtotime("-1 day"))])->count();
$dc = PlanDc::find()->where(['dc_date'=>date('Y-m-d')])->count();
$dc_yesterday = PlanDc::find()->where(['dc_date'=>date('Y-m-d', strtotime("-1 day"))])->count();
$color = [
  'background:#FDE1B8;',
  'background:#ADE288;',
  'background:#DEF197;',
  'background:#F8F4D8;',
  'background:#BBD6EC;',
  'background:#CAEBED;',
  'background:#BCE5E2;',
  'background:#F7A8B2;',
  'background:#F6D7C5;',
  'background:#D8C3E0;',
  'background:#D1EED3;',
  'background:#B3DFB5;',
  'background:#F9F4CF;',
  'background:#BDD0EB;',
  'background:#F8E3D4;',
  'background:#D6D0F5;',
  'background:#F5D4E9;',
  'background:#D8C9FF;',
  'background:#F3D5FB;',
  'background:#D6FFC7;',
  'background:#FBFFD4;',
  'background:#D1F5F5;',
  'background:#C7E9FF;',
  'background:#FFF0F9;',
  'background:#FFDDF4;',
  'background:#D8D0F2;',
  'background:#FCEECF;',
  'background:#BAE3E6;',
  'background:#AAC6E6;',
  'background:#AEA0DB;',
  'background:#EFB5E5;',
];
/* @var $this yii\web\View */
$this->title = 'IPD Paperless';
?>
<div class="site-index">
<div class="body-content">
<div style=
<?= !Yii::$app->user->isGuest ? $this->render('_search', ['model' => $searchModelAdmission]):''; ?>

<div class="jumbotron">
<div class="row">
  <div class="col-md-4">
    <div class="panel panel-primary">
      <div class="panel-heading">
        ยอดผู้ป่วยสะสม  
      </div>
      <div class="panel-body">
        <?= $dashboard->total ?> ราย
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-success">
      <div class="panel-heading">
        ยอดจำหน่ายสะสม  
      </div>
      <div class="panel-body">
        <?= $dashboard->dc ?> ราย
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-warning">
      <div class="panel-heading">
        ผู้ป่วยกักตัวหลัง DC 14 วัน  
      </div>
      <div class="panel-body">
        <?= $dashboard->quarantine ?> ราย
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-2">
    <div class="panel panel-info">
      <div class="panel-heading">
        ผู้ป่วยชายสะสม  
      </div>
      <div class="panel-body">
        <?= $dashboard->male ?> ราย
      </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="panel panel-danger">
      <div class="panel-heading">
      ผู้ป่วยหญิงสะสม  
      </div>
      <div class="panel-body">
        <?= $dashboard->female ?> ราย
      </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="panel panel-info">
      <div class="panel-heading">
        ผู้ป่วยยังรักษา ชาย  
      </div>
      <div class="panel-body">
        <?= $dashboard->male_admit ?> ราย
      </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="panel panel-danger">
      <div class="panel-heading">
      ผู้ป่วยยังรักษา หญิง  
      </div>
      <div class="panel-body">
        <?= $dashboard->female_admit ?> ราย
      </div>
    </div>
  </div>
  <div class="col-md-4">
  <?= Yii::$app->user->isGuest ?  Html::a('เข้าสู่ระบบ', ['/user/security/login'], ['class' => 'btn btn-primary btn-lg','style'=> 'width:300px']):'' ?> 
  <p class="lead"><?= Yii::$app->user->isGuest ? 'กรุณา Log In เพื่อเข้าใช้งาน':'เลือก Ward เพื่อเข้าใช้งาน'?></p>
  </div>
</div>
<div class="row">
  <div class="col-md-4 btn btn-lg"><?= Html::a('จำหน่ายเมื่อวานนี้ '. $dc_yesterday.' ราย', ['admission/yesterdaydc'], ['class' => 'btn btn-warning btn-lg','style'=> 'width:300px;margin-bottom: 20px;']) ?> </div>
  <div class="col-md-4 btn btn-lg"><?= Html::a('รับใหม่วันนี้ '.$admit.' ราย', ['admission/todayadmit'], ['class' => 'btn btn-danger btn-lg','style'=> 'width:300px;margin-bottom: 20px;']) ?> </div>
  <div class="col-md-4 btn btn-lg"><?= Html::a('จำหน่ายวันนี้ '. $dc .' ราย', ['admission/todaydc'], ['class' => 'btn btn-success btn-lg','style'=> 'width:300px;margin-bottom: 20px;']) ?></div>
</div>

<?php 

    $i=0;
    foreach($model as $ward){
      $bed = 0;
      $bed = Admission::find()->where(['ward_id'=>$ward->ward_id,'is_admit'=>'0'])->count();
      if($i%3==0){
        echo '<div class="row">';
      }

      echo '<div class="col-md-4">'.Html::a($ward->ward_name.' ('.$bed.')',['admission/index','id'=>$ward->ward_id] , ['class' => 'btn btn-lg','style'=> 'width:300px;margin-bottom: 20px;'.$color[$i].'color:black']).'</div>';
      $i=$i+1; 
      if($i%3==0){
        echo '</div>';
      }
    }
?>

</div>

</div>
</div>
<h2 align="center">รายงานจำนวนผู้ป่วยที่ยังรักษา</h2>
<?= GridView::widget([
  'dataProvider' => $dataProviderRep,
  'panel' => [
    'before' =>''
  ],
  'showPageSummary' => true,
  'columns' => [
    [
      'class' => 'kartik\grid\\SerialColumn',
      'pageSummary'=>'รวม',
      'pageSummaryOptions' => ['colspan' => 2],
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    'ward_name',
    [
      'attribute' => 'day1_6',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day7',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day8',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day9',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day10',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day11',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day12',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day13',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day14',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
  ],
]); 
  if($dataProviderStage){
?>
<h2 align="center">รายงานจำนวนผู้ป่วยที่ยังรักษาแยก Stage</h2>
<?= GridView::widget([
  'dataProvider' => $dataProviderStage,
  'panel' => [
    'before' =>''
  ],
  'showPageSummary' => true,
  'columns' => [
    [
      'class' => 'kartik\grid\\SerialColumn',
      'pageSummary'=>'รวม',
      'pageSummaryOptions' => ['colspan' => 2],
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    'ward_name',
    [
      'attribute' => 'stage_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_3_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_3_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_3',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
  ],
]);
}
  if($dataProviderWard){
?>
<h2 align="center">รายงานจำนวนผู้ป่วยที่ยังรักษาแยก Stage ตามประเภท Ward</h2>
<?= GridView::widget([
  'dataProvider' => $dataProviderWard,
  'panel' => [
    'before' =>''
  ],
  'showPageSummary' => true,
  'columns' => [
    [
      'class' => 'kartik\grid\\SerialColumn',
      'pageSummary'=>'รวม',
      'pageSummaryOptions' => ['colspan' => 2],
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    'ward_type_name',
    [
      'attribute' => 'stage_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_3_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_3_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_3',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
  ],
]);
}
?>
