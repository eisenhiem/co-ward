<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdmissionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admission-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-3 pull-right">
            <?= $form->field($model, 'param')->textInput(['maxlength' => true,'placeholder' => 'ค้นหาจาก ชื่อ, สกุล, HN, AN'])->label(false) ?> 
            <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
