<?php

use yii\helpers\Html;
use kartik\grid\GridView;


$this->title = 'รายงานจำนวนผู้ป่วยที่ยังรักษา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
  'dataProvider' => $dataProvider,
  'panel' => [
    'before' =>''
  ],
  'showPageSummary' => true,
  'columns' => [
    [
      'class' => 'kartik\grid\\SerialColumn',
      'pageSummary'=>'รวม',
      'pageSummaryOptions' => ['colspan' => 2],
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    'ward_name',
    [
      'attribute' => 'day1_6',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day7',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day8',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day9',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day10',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day11',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day12',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day13',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'day14',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
  ],
]); ?>


</div>
