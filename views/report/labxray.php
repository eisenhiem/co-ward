<?php

use yii\helpers\Html;
use kartik\grid\GridView;


$this->title = 'รายงานการสั่ง Lab X-Ray';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rounds-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel'=>[
            'before'=>' '
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'header' => 'Ward',
                'value' => function ($model) {
                  return $model->admission->getWardName();
                },
            ],
            'admission.bed',
            'order_id',
            'order_datetime',
            'an',
            [
                'header' => 'ชื่อ-สกุล',
                'value' => function ($model) {
                  return $model->getPatientName();
                },
            ],
            [
                'attribute' => 'is_lab',
                'value' => function ($model) {
                  return $model->is_lab == '1' ? 'มีการสั่ง LAB':'';
                },
            ],
            [
                'attribute' => 'is_xray',
                'value' => function ($model) {
                  return $model->is_xray == '1' ? 'มีการสั่ง X-Ray':'';
                },
            ],
            
        ],
    ]); ?>


</div>
