<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\OrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['labxray'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
        <?= $form->field($model, 'start_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'start_date', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันเริ่ม'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd 00:00:00',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'end_date')->widget(DatePicker::ClassName(),
                [
                    'name' => 'end_date', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันสิ้้นสุด'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd 23:59:59',
                        'todayHighlight' => true
                    ]
                ]); 
            ?>            
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
