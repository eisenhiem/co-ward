<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


$day = [1=> 1,2=> 2,3=> 3,4=> 4,5=> 5,6=> 6,7=> 7,8=> 8,9=> 9,10=> 10,11=> 11,12=> 12,13=> 13,14=> 14 ];
/* @var $this yii\web\View */
/* @var $model app\models\OrdersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-search">

    <?php $form = ActiveForm::begin([
        'action' => ['days'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'ward_id')->dropDownList($model->getItemWard(), ['prompt'=>'']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'days')->dropDownList($day, ['prompt'=>'']) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
