<?php

use app\models\Admission;
use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\LineNotify;
use app\models\RepWardStage;

$line = LineNotify::find()->all();

$message = ' วันที่ ' . Admission::getThaiDateTime(date('Y-m-d H:i:s')) ." \n";

$model = RepWardStage::find()->all();
$total = 0;

foreach($model as $rep) {
  $total = $rep->getTotal(); 
  $message .= $rep->ward_type_name . ' ยอดรวม '.$total .' ราย'."\n";
  $message .= ' ระดับ 1 ไม่มีอาการ '. $rep->stage_1 .' ราย'."\n";
  $message .= ' ระดับ 2 มีอาการ Mild Symptom '. $rep->stage_2 .' ราย'."\n";
  $message .= ' ระดับ 3.1 ไม่มีปอดอักเสบ แต่มีปัจจัยเสี่ยง '. $rep->stage_3_1 .' ราย'."\n";
  $message .= ' ระดับ 3.2 มีปอดอักเสบ แต่ไม่รุนแรง '. $rep->stage_3_2 .' ราย'."\n";
  $message .= ' ระดับ 4.1 ปอดอักเสบ รุนแรง '. $rep->stage_4_1 .' ราย'."\n";
  $message .= ' ระดับ 4.2 On Oxygen High Flow '. $rep->stage_4_2 .' ราย'."\n";
  $message .= ' ระดับ 4.3 '. $rep->stage_4_3 .' ราย'."\n";
}

$this->title = 'รายงานจำนวนผู้ป่วยที่ยังรักษาแยก Stage จำแนกตาม Ward';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
  'dataProvider' => $dataProvider,
  'panel' => [
    'before' =>''
  ],
  'showPageSummary' => true,
  'columns' => [
    [
      'class' => 'kartik\grid\\SerialColumn',
      'pageSummary'=>'รวม',
      'pageSummaryOptions' => ['colspan' => 2],
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    'ward_type_name',
    [
      'attribute' => 'stage_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_3_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_3_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_3',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
  ],
]); 
?>
<div class="panel panel-success">
  <div class="panel-heading">
    Send Line Notify
  </div>
  <div class="panel-body">
<?php
foreach($line as $notify){
echo Html::a($notify->line_name, ['send', 'id' => $notify->line_id,'message'=>$message], ['class' => 'btn btn-success']);
}
echo '<div style="text-align:right">'.Html::a(' Send All Group Line ', ['sendall','message'=>$message], ['class' => 'btn btn-primary']).'</div>';
?>
  </div>
</div>
<?php // echo  $message; ?>
</div>
