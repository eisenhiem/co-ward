<?php

use yii\helpers\Html;
use kartik\grid\GridView;


$this->title = 'รายงานสถานะผู้ป่วย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rounds-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_searchday', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel'=>[
            'before'=>' '
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'ward_name',
            'bed',
            'an',
            'hn',
            'ptname',
            'age',
            'address',
            'contact_number',
            'days',            
        ],
    ]); ?>


</div>
