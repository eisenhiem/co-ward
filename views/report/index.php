<?php

use yii\helpers\Html;
use kartik\grid\GridView;


$this->title = 'รายงาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-index">

    <h1><?= Html::encode($this->title) ?></h1>
        <ui>
            <li><?= Html::a('รายงานจำนวนผู้ป่วยที่ยังรักษา', ['stay']) ?> </li>
            <li><?= Html::a('รายงานจำนวนผู้ป่วยที่ยังรักษาแยก Stage', ['stage']) ?>  </li>
            <li><?= Html::a('รายงานจำนวนผู้ป่วยที่ยังรักษาแยก Stage จำแนกตาม Ward', ['ward']) ?> </li>
            <li><?= Html::a('รายงานจำนวนการสั่ง Lab,X-Ray', ['ward']) ?> </li>
            <li><?= Html::a('รายงานผู้ป่วยที่กำลังรักษา', ['days']) ?> </li>
        </ui>
</div>