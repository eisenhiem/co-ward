<?php

use yii\helpers\Html;
use kartik\grid\GridView;


$this->title = 'รายงานจำนวนผู้ป่วยที่ยังรักษาแยก Stage';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
  'dataProvider' => $dataProvider,
  'panel' => [
    'before' =>''
  ],
  'showPageSummary' => true,
  'columns' => [
    [
      'class' => 'kartik\grid\\SerialColumn',
      'pageSummary'=>'รวม',
      'pageSummaryOptions' => ['colspan' => 2],
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    'ward_name',
    [
      'attribute' => 'stage_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_3_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_3_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_1',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_2',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
    [
      'attribute' => 'stage_4_3',
      'pageSummary' => true,
      'hAlign' => 'center',
    ],
  ],
]); ?>


</div>
