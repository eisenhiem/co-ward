<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Notes */

$this->title = 'Nurse Note AN : '.$model->an.' ชื่อ-สกุล:'.$model->getPatientName();
$this->params['breadcrumbs'][] = ['label' => 'Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'vs' => $vs,
    ]) ?>

</div>
