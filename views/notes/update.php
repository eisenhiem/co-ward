<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Notes */

$this->title = 'Update Notes: ' . $model->note_id;
$this->params['breadcrumbs'][] = ['label' => 'Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->note_id, 'url' => ['view', 'id' => $model->note_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="notes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'vs' => $vs,
    ]) ?>

</div>
