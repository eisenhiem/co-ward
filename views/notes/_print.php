<?php
use yii\helpers\Html;
use app\models\VsDaily;
use app\models\Admission;

function thaimonth($m){
  switch($m){
    case '01':$thainame = 'มกราคม';break;
    case '02':$thainame = 'กุมภาพันธ์';break;
    case '03':$thainame = 'มีนาคม';break;
    case '04':$thainame = 'เมษายน';break;
    case '05':$thainame = 'พฤษภาคม';break;
    case '06':$thainame = 'มิถุนายน';break;
    case '07':$thainame = 'กรกฎาคม';break;
    case '08':$thainame = 'สิงหาคม';break;
    case '09':$thainame = 'กันยายน';break;
    case '10':$thainame = 'ตุลาคม';break;
    case '11':$thainame = 'พฤศจิกายน';break;
    case '12':$thainame = 'ธันวาคม';break;
  }  
  return $thainame;
}
?>
<?php 
  $i=1;
    foreach($model as $note) { 
      if($i <> 1){
        echo '<pagebreak>';
      }
      $i++;
      $vs = VsDaily::findOne($note->note_id);
?>  
<h3 align="center"><?= $office->hospname ?><br><?= $office->address?> <?= $office->zipcode ?><br>บันทึกทางการพยาบาล</h3>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
  <tr><td width="61%"><b>&emsp; ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp;<?= $pt->getPatientName() ?></td>
  <td width="18%">&emsp; <b>อายุ</b> <?= $pt->age ?> ปี</td>
  <td width="21%">&emsp; <b>HN:</b> <?= $pt->hn ?><br>&emsp; <b>AN:</b> <?= $pt->an ?></td>
</tr></table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
<tr>
  <th width="22%">วันที่ เวลา</th>
  <th width="39%">กิจกรรมทางการพยาบาล</th>
  <th width="39%">การประเมินผล</th>
</tr>
<tr><td height=720 valign=top align="center">
<?= Admission::getThaiDateTime($note->nurse_datetime) ?>
<br>
<?php if($vs) {
    echo '<table border="1" width="100%" cellpadding="2" cellspacing="0">';
    echo '<tr><td>V/S '. $vs->round->round_name.'</td></tr>'; 
    echo '<tr><td> BT: '.$vs->body_temp.' ํC</td></tr>'; 
    echo '<tr><td> BP: '. $vs->sbp .'/'.$vs->dbp .'</td></tr>'; 
    echo '<tr><td> PR: '. $vs->pr .'/min</td></tr>'; 
    echo '<tr><td> RR: '. $vs->rr .'/min</td></tr>'; 
    echo '<tr><td> O2Sat: '. $vs->o2sat .' %</td></tr>'; 
    echo '<tr><td> Stool: '. $vs->stool .' ครั้ง</td></tr>'; 
    echo '<tr><td> Urine: '. $vs->urine .' ครั้ง</td></tr>'; 
    echo '</table>';
}
?>

</td>
<td valign=top>
  &emsp;<?= $note->activity_select ? $note->getActName():''; ?>  <?= nl2br($note->activity) ?>
</td>
<td valign=top>
  &emsp;<?= $note->evaluate_select ? $note->getEvaName():''; ?>  <?= nl2br($note->evaluate) ?><br>
  <br><b>ผู้ประเมิน</b><br><br>
  <?= $note->profile->fullname ?><br>
  <?= $note->profile->position ?><br>

</td>
</tr>
</table>
<?php 
}
?>
