<?php

use app\models\VsDaily;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nurse Note AN:'.$an.' ชื่อ-สกุล:'.$ward->getPatientName();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่ม Note', ['create','id'=>$an], ['class' => 'btn btn-success']) ?>
        &emsp; 
        <?= $note->config_value == 'asc' ? Html::a('เรียง Z > A', ['switchsort','id' => $an], ['class' => 'btn btn-primary']):Html::a('เรียง A > Z', ['switchsort','id' => $an], ['class' => 'btn btn-primary']); ?> 
        &emsp; 
        <?= Html::a('Doctor Order', ['orders/detail','id'=>$an], ['class' => 'btn btn-info']) ?>
        &emsp; 
        <?= Html::a('Vital Sign', ['notes/vitalsign','id'=>$an], ['class' => 'btn btn-info']) ?>
        &emsp; 
        <?= Html::a('พิมพ์ Nurse Note', ['print','id'=>$an], ['class' => 'btn btn-warning']) ?>
        &emsp; 
        <?= Html::a('กลับไปหน้าหลัก', ['admission/index','id' => $ward->ward_id], ['class' => 'btn btn-danger']) ?>
    </p>

<table width="100%" border="1">
<tr>
<th width="22%" style="text-align:center">วันที่ เวลา</th>
<th width="43%" style="text-align:center">กิจกรรมทางการพยาบาล</th>
<th width="35%" style="text-align:center">ผลการประเมิน</th>
</tr>
<?php
if($model){
    foreach($model as $note){
        $vs = VsDaily::findOne($note->note_id);
?>
<tr>
<td style="vertical-align:top;text-align:center">
&emsp; <?= $note->nurse_datetime ?><br><br>
<?php if($vs) {
    echo '<table border="1" width="100%">';
    echo '<tr><td>V/S '. $vs->round->round_name.'</td></tr>'; 
    echo '<tr><td> BT: '.$vs->body_temp.' ํC</td></tr>'; 
    echo '<tr><td> BP: '. $vs->sbp .'/'.$vs->dbp .'</td></tr>'; 
    echo '<tr><td> PR: '. $vs->pr .'/min</td></tr>'; 
    echo '<tr><td> RR: '. $vs->rr .'/min</td></tr>'; 
    echo '<tr><td> O2Sat: '. $vs->o2sat .' %</td></tr>'; 
    echo '<tr><td> Stool: '. $vs->stool .' ครั้ง</td></tr>'; 
    echo '<tr><td> Urine: '. $vs->urine .' ครั้ง</td></tr>'; 
    echo '</table>';
}
?>
</td>
<td style="vertical-align:top">
&emsp;<?= $note->activity_select ? $note->getActName():''; ?> <?= nl2br($note->activity) ?>
</td>
<td style="vertical-align:top">&emsp;<?= $note->evaluate_select ? $note->getEvaName():''; ?> <?= nl2br($note->evaluate) ?>
<br><br>
<b>ผู้ประเมิน</b>    
&emsp; <?= $note->profile->fullname ?><br>
&emsp; <?= $note->profile->position ?><br>
&emsp;<?= Yii::$app->user->identity->id == $note->user_id ? Html::a('แก้ไขบันทึก', ['update', 'id' => $note->note_id], ['class' => 'btn btn-warning']):'' ?><br>
</td>
</tr>
<?php
    }
}
?>
</table>

</div>
