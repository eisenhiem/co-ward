<?php
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
?>

<?= Html::a('กลับ', ['notes/detail','id' => $an], ['class' => 'btn btn-success','style'=>'width:200px']) ?>
<br>
<?php 
echo Highcharts::widget([
    'options' => [
       'title' => ['text' => 'Oxygen Sat'],
       'xAxis' => [
          'categories' => $date
       ],
       'yAxis' => [
          'title' => ['text' => 'O2 Sat']
       ],
       'series' => [
          ['name' => 'pluse rate', 'data' => $o2sat],
       ]
    ]
 ]);

 echo Highcharts::widget([
   'options' => [
      'title' => ['text' => 'อุณหภูมิร่างกาย  ํc'],
      'xAxis' => [
         'categories' => $date
      ],
      'yAxis' => [
         'title' => ['text' => 'temp  ( ํc)']
      ],
      'series' => [
         ['name' => 'BT', 'data' => $bt],
      ]
   ]
]);

echo Highcharts::widget([
    'options' => [
       'title' => ['text' => 'อัตราการเต้นของหัวใจ'],
       'xAxis' => [
          'categories' => $date
       ],
       'yAxis' => [
          'title' => ['text' => 'Pluse Rate /min']
       ],
       'series' => [
          ['name' => 'pluse rate', 'data' => $pr],
       ]
    ]
 ]);
?>

 <table width="100%" border=1>
<tr>
    <th>Date</th>
<?php 
    foreach($date as $d) {
        echo '<th>'.$d.'</th>';
    }
?>
</tr>
<tr>
    <td>RR</td>
<?php 
    foreach($rr as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>
<tr>
    <td>SBP</td>
<?php 
    foreach($sbp as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>
<tr>
    <td>DBP</td>
<?php 
    foreach($dbp as $d) {
        if($d == 0){
            $d='N/A';
        }
        echo '<td>'.$d.'</td>';
    }
?>
</tr>

 </table>