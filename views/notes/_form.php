<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rounds;

$round = ArrayHelper::map(Rounds::find()->all(), 'round_id', 'round_name');

/* @var $this yii\web\View */
/* @var $model app\models\Notes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notes-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="panel panel-primary">
        <div class="panel-heading">
            <?= $form->field($vs, 'round_id')->radioList($round) ?>
        </div>
        <div class="panel-body">
        <div class="col-md-2">
            <?= $form->field($vs, 'body_temp')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($vs, 'sbp')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($vs, 'dbp')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($vs, 'pr')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($vs, 'rr')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($vs, 'o2sat')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($vs, 'stool')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($vs, 'urine')->textInput(['maxlength' => true]) ?>
        </div>
        </div>
        <div class="panel-footer">
            <?= $form->field($model, 'is_save')->checkbox() ?>
        </div>
    </div>
    <div cass="row">
        <div class="col-md-12">
        <?= $form->field($model, 'activity_select')->checkboxList($model->getActUse(),['item' => function($index, $label, $name, $checked, $value) {
            return "<label class='col-md-6'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
        }]) ?>
        </div>
        <div class="col-md-12">
        <?= $form->field($model, 'activity')->textarea(['rows' => 3])->label(false) ?>
        </div>
    </div>
    <div cass="row">
        <div class="col-md-12">
        <?= $form->field($model, 'evaluate_select')->checkboxList($model->getEvaUse(),['item' => function($index, $label, $name, $checked, $value) {
            return "<label class='col-md-6'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
        }]) ?>
        </div>
        <div class="col-md-12">
        <?= $form->field($model, 'evaluate')->textarea(['rows' => 3])->label(false) ?>
        </div>
    </div>
    <br>
    <div class="row">
    <div class="form-group" style="text-align: center;">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success','style'=>'width:200px']) ?>
        &emsp; 
        <?= Html::a('กลับไปหน้าหลัก', ['detail','id'=>$model->an], ['class' => 'btn btn-danger','style'=>'width:200px']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
