<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderOneday */

$this->title = 'Create Order Oneday';
$this->params['breadcrumbs'][] = ['label' => 'Order Onedays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-oneday-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
