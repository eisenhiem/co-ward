<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderOneday */

$this->title = 'Update Order Oneday: ' . $model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'Order Onedays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->order_id, 'url' => ['view', 'id' => $model->order_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-oneday-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
