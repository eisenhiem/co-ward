<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'ระบบบันทึกชาร์ตผู้ป่วยใน',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Admission',  'visible' => !Yii::$app->user->isGuest ,'url' => ['/site/index']],
            ['label' => 'Discharge',  'visible' => !Yii::$app->user->isGuest ,'url' => ['/admission/discharge']],
            ['label' => 'รายงาน',  'visible' => !Yii::$app->user->isGuest , 'items' => [
                ['label' => 'รายงาน', 'url' => ['/report/index']],
                ['label' => 'รายงานผู้ป่วยรักษา', 'url' => ['/report/stay']],
                ['label' => 'รายงานผู้ป่วยแยก Stage', 'url' => ['/report/stage']],
                ['label' => 'รายงานผู้ป่วยแยก Stage จำแนกตาม Ward', 'url' => ['/report/ward']],
                ['label' => 'รายงานการสั่ง Lab,X-Ray', 'url' => ['/report/labxray']],
                ['label' => 'รายงานผู้ป่วยที่ยังรักษาอยู่', 'url' => ['/report/days']],
            ]],
            ['label' => 'ตั้งค่า','visible' => Yii::$app->user->identity->username == 'admin', 'items' => [
                ['label' => 'เพิ่มผู้ใช้', 'url' => ['/user/admin/index']],
                ['label' => 'จัดการผู้ใช้', 'url' => ['/profile/index']],
                ['label' => 'จัดการ Ward', 'url' => ['/ward/index']],
                ['label' => 'จัดการประเภท Ward', 'url' => ['/type/index']],
                ['label' => 'ตั้งค่ารอบวัด Vital Sign', 'url' => ['/rounds/index']],
                ['label' => 'ตั้งค่า Oneday Order', 'url' => ['/oneday/index']],
                ['label' => 'ตั้งค่า Standing Order', 'url' => ['/standingorder/index']],
                ['label' => 'ตั้งค่ากิจกรรมทางการพยาบาล', 'url' => ['/activity/index']],
                ['label' => 'ตั้งค่าการประเมินผล', 'url' => ['/evaluate/index']],
                ['label' => 'ตั้งค่า Progress Note', 'url' => ['/progress/index']],
                ['label' => 'ตั้งค่า Line Notify', 'url' => ['/line/index']],
                ['label' => 'ตั้งค่าหน่วยบริการ', 'url' => ['/office/index']],
            ] ],
            Yii::$app->user->isGuest ?
            ['label' => 'ลงชื่อเข้าใช้งาน', 'url' => ['/user/security/login']] :
            ['label' => 'ผู้ใช้งาน['.Yii::$app->user->identity->profile->fullname.']', 'items'=>[
                ['label' => 'ออกจากระบบ', 'url' => ['/user/security/logout'],'linkOptions' => ['data-method' => 'post']],
            ]],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Jwongkamalasai@gmail.com <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
