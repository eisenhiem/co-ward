<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OfficeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หน่วยบริการ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="office-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $is_exist == 0 ? Html::a('เพิ่มหน่วยบริการ', ['create'], ['class' => 'btn btn-success']):'' ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'hospcode',
            'hospname',
            'address',
            'phone_number',
            'fax_number',
            //'line_id',
            //'facebook_url:url',
            //'zipcode',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
