CREATE TABLE `l_activity` (
  `activity_id` varchar(2) NOT NULL,
  `activity_name` varchar(255) DEFAULT NULL,
  `is_active` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
