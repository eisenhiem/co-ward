ALTER TABLE `orders`
ADD COLUMN `is_lab`  enum('1','0') NULL DEFAULT '0' AFTER `oneday_list`,
ADD COLUMN `is_xray`  enum('1','0') NULL DEFAULT '0' AFTER `is_lab`;
