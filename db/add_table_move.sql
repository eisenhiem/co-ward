CREATE TABLE `move` (
  `an` int(11) DEFAULT NULL,
  `ward_id` int(11) DEFAULT NULL,
  `to_ward_id` int(11) DEFAULT NULL,
  `move_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;