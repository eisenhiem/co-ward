ALTER TABLE `notes`
ADD COLUMN `activity_select`  varchar(255) NULL AFTER `user_id`;

ALTER TABLE `notes`
ADD COLUMN `evaluate_select`  varchar(255) NULL AFTER `activity_select`;
