CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` 
SQL SECURITY DEFINER VIEW `dashboard` AS 
select count(`admission`.`an`) AS `total`
,count((case `admission`.`gender` when '1' then `admission`.`an` end)) AS `male`
,count((case `admission`.`gender` when '2' then `admission`.`an` end)) AS `female`
,count((case `admission`.`is_admit` when '1' then `admission`.`an` end)) AS `dc`
,count((case when ((`admission`.`is_admit` = '0') and (`admission`.`gender` = '1')) then `admission`.`an` end)) AS `male_admit`
,count((case when ((`admission`.`is_admit` = '0') and (`admission`.`gender` = '2')) then `admission`.`an` end)) AS `female_admit`
,count((case when (`plan_dc`.`dc_date` between cast((now() - interval 14 day) as date) and cast(now() as date)) then `plan_dc`.`an` end)) AS `quarantine` 
from (`admission` 
left join `plan_dc` on((`admission`.`an` = `plan_dc`.`an`)))