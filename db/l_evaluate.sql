CREATE TABLE `l_evaluate` (
  `evaluate_id` varchar(2) NOT NULL,
  `evaluate_name` varchar(255) DEFAULT NULL,
  `is_active` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`evaluate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;