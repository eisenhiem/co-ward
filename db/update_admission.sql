ALTER TABLE `admission`
MODIFY COLUMN `asymptomatic`  enum('1','2','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `sick_date`;
