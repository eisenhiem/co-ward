CREATE TABLE `l_progress` (
  `progress_id` varchar(2) NOT NULL,
  `progress_name` varchar(255) DEFAULT NULL,
  `is_active` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`progress_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;