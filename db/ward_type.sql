CREATE TABLE `ward_type` (
  `ward_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ward_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ward_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

