/*
Navicat MySQL Data Transfer

Source Server         : web lsk
Source Server Version : 50556
Source Host           : 164.115.20.45:3306
Source Database       : ipd_paperless

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2021-08-15 23:00:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `l_order_group`
-- ----------------------------
DROP TABLE IF EXISTS `l_order_group`;
CREATE TABLE `l_order_group` (
`order_group_id`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`order_group_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`order_group_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of l_order_group
-- ----------------------------
BEGIN;
INSERT INTO `l_order_group` VALUES ('l', 'Lab'), ('m', 'Medicine'), ('o', 'Other'), ('x', 'X-Ray');
COMMIT;
