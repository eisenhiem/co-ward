set @lab = (select group_concat(order_id SEPARATOR '|') from order_oneday where order_group_id ='l');
set @xray = (select group_concat(order_id SEPARATOR '|') from order_oneday where order_group_id ='x');

update orders set is_lab = '1' where oneday_list REGEXP @lab;
update orders set is_xray = '1' where oneday_list REGEXP @xray;