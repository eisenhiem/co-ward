CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` 
SQL SECURITY DEFINER VIEW `rep_pt` AS 
select `a`.`ward_id` AS `ward_id`
,`w`.`ward_name` AS `ward_name`
,`a`.`bed` AS `bed`
,`a`.`an` AS `an`
,`a`.`hn` AS `hn`
,concat(`a`.`pname`,`a`.`fname`,' ',`a`.`lname`) AS `ptname`
,`a`.`age` AS `age`
,`a`.`address` AS `address`
,`a`.`contact_number` AS `contact_number`
,timestampdiff(DAY,`p`.`swap_date`,now()) AS `days` 
from ((`admission` `a` 
join `ward` `w` on((`a`.`ward_id` = `w`.`ward_id`))) 
left join `ptsign` `p` on((`a`.`an` = `p`.`an`))) 
where (`a`.`is_admit` = '0')