/*
Navicat MySQL Data Transfer

Source Server         : Local_DB
Source Server Version : 50565
Source Host           : localhost:3306
Source Database       : ipd_paperless

Target Server Type    : MYSQL
Target Server Version : 50565
File Encoding         : 65001

Date: 2021-04-20 20:16:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admission`
-- ----------------------------
DROP TABLE IF EXISTS `admission`;
CREATE TABLE `admission` (
  `an` int(8) NOT NULL AUTO_INCREMENT,
  `pname` varchar(20) DEFAULT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `gender` enum('2','1') NOT NULL,
  `dob` date DEFAULT NULL,
  `cid` varchar(13) DEFAULT NULL,
  `address` tinytext,
  `contact_number` varchar(10) NOT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `phistory` varchar(255) DEFAULT NULL,
  `med_reconcile` text,
  `cc` varchar(255) DEFAULT NULL,
  `covid_register` date DEFAULT NULL,
  `allergy` varchar(255) DEFAULT NULL,
  `hn` int(11) DEFAULT NULL,
  `age` tinyint(4) DEFAULT NULL,
  `is_admit` enum('0','1') DEFAULT '0',
  `bed` varchar(10) DEFAULT NULL,
  `ward_id` int(4) DEFAULT '0',
  `insure_name` varchar(255) DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `isolation_date` date DEFAULT NULL,
  `ventilated` enum('9','1','0') DEFAULT NULL,
  `ventilated_other` varchar(255) DEFAULT NULL,
  `sick_date` date DEFAULT NULL,
  `asymptomatic` enum('1','0') DEFAULT NULL,
  `insure_other` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `other_comobit` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`an`)
) ENGINE=InnoDB AUTO_INCREMENT=64000542 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admission
-- ----------------------------

-- ----------------------------
-- Table structure for `notes`
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `an` int(11) NOT NULL,
  `nurse_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activity` text NOT NULL,
  `evaluate` tinytext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notes
-- ----------------------------

-- ----------------------------
-- Table structure for `office`
-- ----------------------------
DROP TABLE IF EXISTS `office`;
CREATE TABLE `office` (
  `hospcode` varchar(5) NOT NULL,
  `hospname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `fax_number` varchar(50) DEFAULT NULL,
  `line_id` varchar(255) DEFAULT NULL,
  `facebook_url` varchar(255) DEFAULT NULL,
  `zipcode` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`hospcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of office
-- ----------------------------

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `an` int(11) NOT NULL,
  `order_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `progress_note` tinytext,
  `order_oneday` text,
  `order_continue` text,
  `comment` tinytext,
  `user_id` int(11) NOT NULL,
  `medication` varchar(255) DEFAULT NULL,
  `record_i_o` varchar(1) DEFAULT NULL,
  `record_vitalsign` varchar(1) DEFAULT NULL,
  `food` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------

-- ----------------------------
-- Table structure for `plan_dc`
-- ----------------------------
DROP TABLE IF EXISTS `plan_dc`;
CREATE TABLE `plan_dc` (
  `an` int(11) NOT NULL,
  `dc_date` date DEFAULT NULL,
  `diagnosis` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `contact_name` varchar(100) DEFAULT NULL,
  `concution` text,
  `dctype` enum('4','3','2','1') DEFAULT NULL,
  PRIMARY KEY (`an`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of plan_dc
-- ----------------------------

-- ----------------------------
-- Table structure for `profile`
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cid` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_no` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of profile
-- ----------------------------

-- ----------------------------
-- Table structure for `ptsign`
-- ----------------------------
DROP TABLE IF EXISTS `ptsign`;
CREATE TABLE `ptsign` (
  `an` int(11) NOT NULL DEFAULT '0',
  `sign` varchar(255) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `wbc` int(11) DEFAULT NULL,
  `pmn` decimal(8,2) DEFAULT NULL,
  `lym` decimal(8,2) DEFAULT NULL,
  `m` decimal(8,2) DEFAULT NULL,
  `eo` decimal(8,2) DEFAULT NULL,
  `atyp_lym` decimal(8,2) DEFAULT NULL,
  `hct` int(11) DEFAULT NULL,
  `platelets` int(11) DEFAULT NULL,
  `cxr_date` date DEFAULT NULL,
  `swap_date` date DEFAULT NULL,
  `covid` enum('0','1') DEFAULT NULL,
  `hb` decimal(8,2) DEFAULT NULL,
  `lab_other` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`an`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ptsign
-- ----------------------------

-- ----------------------------
-- Table structure for `standing_order`
-- ----------------------------
DROP TABLE IF EXISTS `standing_order`;
CREATE TABLE `standing_order` (
  `med_id` varchar(5) NOT NULL,
  `medicine` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`med_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of standing_order
-- ----------------------------
INSERT INTO `standing_order` VALUES ('cpm', 'CPM 1 Tab PO TID PC');
INSERT INTO `standing_order` VALUES ('fah', 'ฟ้าทะลายโจร 6000 mg/day');
INSERT INTO `standing_order` VALUES ('gg', 'GG 1 Tab PO TID PC');
INSERT INTO `standing_order` VALUES ('lora', 'Loratadine 1 Tab PO HS');
INSERT INTO `standing_order` VALUES ('para', 'Paracetamol 500mg 1 Tab PO prn q 4-6 hr');
INSERT INTO `standing_order` VALUES ('vit-c', 'Vitamin C 2 Tab Oral TID PC');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  `dep_id` int(2) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`) USING BTREE,
  UNIQUE KEY `user_unique_email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for `vitalsign`
-- ----------------------------
DROP TABLE IF EXISTS `vitalsign`;
CREATE TABLE `vitalsign` (
  `an` int(11) NOT NULL DEFAULT '0',
  `bw` decimal(4,1) DEFAULT NULL,
  `height_cm` decimal(4,0) DEFAULT NULL,
  `body_temp` decimal(4,1) DEFAULT NULL,
  `rr` tinyint(4) DEFAULT NULL,
  `pr` tinyint(4) DEFAULT NULL,
  `sbp` decimal(4,0) DEFAULT NULL,
  `dbp` decimal(4,0) DEFAULT NULL,
  `o2sat` tinyint(4) DEFAULT NULL,
  `symptom` varchar(255) DEFAULT NULL,
  `other_symptom` varchar(255) DEFAULT NULL,
  `pain` enum('4','3','2','1') DEFAULT NULL,
  `o2sat_ra` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`an`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vitalsign
-- ----------------------------

-- ----------------------------
-- Table structure for `ward`
-- ----------------------------
DROP TABLE IF EXISTS `ward`;
CREATE TABLE `ward` (
  `ward_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `ward_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ward_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ward
-- ----------------------------
INSERT INTO `ward` VALUES ('1', 'Ward หญิง');
INSERT INTO `ward` VALUES ('2', 'Ward ชาย');
INSERT INTO `ward` VALUES ('4', 'ยังไม่รับ');
