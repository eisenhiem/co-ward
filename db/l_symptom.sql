/*
Navicat MySQL Data Transfer

Source Server         : web lsk
Source Server Version : 50556
Source Host           : 164.115.20.45:3306
Source Database       : ipd_paperless

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2021-04-20 22:30:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `l_symptom`
-- ----------------------------
DROP TABLE IF EXISTS `l_symptom`;
CREATE TABLE `l_symptom` (
  `symptom_id` varchar(5) NOT NULL,
  `symptom_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`symptom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of l_symptom
-- ----------------------------
INSERT INTO `l_symptom` VALUES ('conf', 'Irritability / Confusion');
INSERT INTO `l_symptom` VALUES ('cough', 'Cough');
INSERT INTO `l_symptom` VALUES ('diar', 'Diarrhoea');
INSERT INTO `l_symptom` VALUES ('fever', 'History of fever / Chills');
INSERT INTO `l_symptom` VALUES ('hache', 'Headache');
INSERT INTO `l_symptom` VALUES ('nose', 'Runny nose');
INSERT INTO `l_symptom` VALUES ('other', 'Other, Specify');
INSERT INTO `l_symptom` VALUES ('short', 'Shortness of breath');
INSERT INTO `l_symptom` VALUES ('sore', 'Sore throat');
INSERT INTO `l_symptom` VALUES ('vomit', 'Nausea / vomiting');
INSERT INTO `l_symptom` VALUES ('weak', 'General weakness');
