ALTER TABLE `vs_daily`
MODIFY COLUMN `round_id`  varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `note_id`,
ADD COLUMN `urine`  int(3) NULL AFTER `rr`,
ADD COLUMN `stool`  int(3) NULL AFTER `urine`;