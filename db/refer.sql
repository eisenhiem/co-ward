/*
Navicat MySQL Data Transfer

Source Server         : web lsk
Source Server Version : 50556
Source Host           : 164.115.20.45:3306
Source Database       : ipd_paperless

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2021-08-07 22:09:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hospitals`
-- ----------------------------
DROP TABLE IF EXISTS `hospitals`;
CREATE TABLE `hospitals` (
`hospcode`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`hospname`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`hospcode`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of hospitals
-- ----------------------------
BEGIN;
INSERT INTO `hospitals` VALUES ('10669', 'โรงพยาบาลสรรพสิทธิประสงค์'), ('10944', 'โรงพยาบาลศรีเมืองใหม่'), ('10945', 'โรงพยาบาลโขงเจียม'), ('10946', 'โรงพยาบาลเขื่องใน'), ('10947', 'โรงพยาบาลเขมราฐ'), ('10948', 'โรงพยาบาลนาจะหลวย'), ('10949', 'โรงพยาบาลน้ำยืน'), ('10950', 'โรงพยาบาลบุณฑริก'), ('10951', 'โรงพยาบาลตระการพืชผล'), ('10952', 'โรงพยาบาลกุดข้าวปุ้น'), ('10953', 'โรงพยาบาลม่วงสามสิบ'), ('10954', 'โรงพยาบาลวารินชำราบ'), ('10956', 'โรงพยาบาลพิบูลมังสาหาร'), ('10957', 'โรงพยาบาลตาลสุม'), ('10958', 'โรงพยาบาลโพธิ์ไทร'), ('10959', 'โรงพยาบาลสำโรง'), ('10960', 'โรงพยาบาลดอนมดแดง'), ('10961', 'โรงพยาบาลสิรินธร'), ('10962', 'โรงพยาบาลทุ่งศรีอุดม'), ('11443', 'โรงพยาบาลสมเด็จพระยุพราชเดชอุดม'), ('11496', 'โรงพยาบาลค่ายสรรพสิทธิประสงค์ (สโมสรนายทหาร ค่ายสรรพสิทธิประสงค์)'), ('12269', 'โรงพยาบาลพระศรีมหาโพธิ์'), ('14201', 'โรงพยาบาลมะเร็งอุบลราชธานี'), ('21984', 'โรงพยาบาล๕๐ พรรษา มหาวชิราลงกรณ'), ('24032', 'โรงพยาบาลนาตาล'), ('24821', 'โรงพยาบาลนาเยีย'), ('27967', 'โรงพยาบาลสว่างวีระวงศ์'), ('27968', 'โรงพยาบาลน้ำขุ่น'), ('27976', 'โรงพยาบาลเหล่าเสือโก้ก');
COMMIT;

-- ----------------------------
-- Table structure for `l_cause`
-- ----------------------------
DROP TABLE IF EXISTS `l_cause`;
CREATE TABLE `l_cause` (
`refer_cause_id`  varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`refer_cause_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`refer_cause_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Records of l_cause
-- ----------------------------
BEGIN;
INSERT INTO `l_cause` VALUES ('01', 'เกินศักยภาพ เพื่อวินิจฉัย/รักษา'), ('02', 'เกินศักยภาพ ขาดแพทย์เฉพาะทาง'), ('03', 'เกินศักยภาพ ขาดอุปกรณ์เครื่องมือ'), ('04', 'เกินศักยภาพ เพื่อผ่าตัด'), ('05', 'เกินศักยภาพ เพื่อชันสูตร'), ('06', 'เกินศักยภาพ ไปตามนัด'), ('07', 'มีศักยภาพ รักษาต่อตามสิทธิการรักษา'), ('08', 'มีศักยภาพ ญาติ/ผู้ป่วยต้องการ'), ('09', 'มีศักยภาพ เพื่อไปตามนัด'), ('10', 'เกินศักยภาพ เพื่อฉายแสง'), ('11', 'เกินศักยภาพ ขาดแพท์เฉพาะทางศัลยกรรมหลอดเลือด'), ('12', 'เกินศักยภาพ ขาดแพทย์เฉพาะทาง CVT'), ('13', 'เกินศักยภาพ ขาดแพทย์เฉพาะทางศัลย์เด็ก'), ('14', 'เกินศักยภาพ ขาดเครื่องมือ BMG'), ('15', 'เกินศักยภาพ เพื่อทำ FLAP case มะเร็งหู คอ จมูก'), ('16', 'เกินศักยภาพ เพื่อ Intervention รังสีร่วมรักษา'), ('17', 'เกินศักยภาพ เพื่อ Investigate'), ('18', 'เกินศักยภาพ เพื่อปลูกถ่ายอวัยวะ'), ('19', 'เกินศักยภาพ เพื่อ Admit จิตเวช'), ('20', 'เกินศักยภาพ เพื่อกลืนแร่'), ('21', 'เกินศักยภาพ เพื่อให้ยาเคมีบำบัด'), ('22', 'เกินศักยภาพ เพื่อ PCI /CAG'), ('23', 'มีศักยภาพ แต่ระยะเวลาคอยนาน'), ('90', 'มีศักยภาพ เพื่อรับการดูแลต่อเนื่องกับ FCT'), ('91', 'มีศักยภาพ เพื่อรับยา/ให้ยาต่อเนื่อง ที่ใกล้บ้าน'), ('99', '99 อื่นๆ');
COMMIT;

-- ----------------------------
-- Table structure for `l_refer_by`
-- ----------------------------
DROP TABLE IF EXISTS `l_refer_by`;
CREATE TABLE `l_refer_by` (
`refer_by_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`refer_by_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`refer_by_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Records of l_refer_by
-- ----------------------------
BEGIN;
INSERT INTO `l_refer_by` VALUES ('1', 'รถ Ambulance'), ('2', 'รถ Ambulance พร้อมพยาบาล'), ('3', 'รถ Ambulance พร้อมแพทย์, พยาบาล'), ('4', 'ไปเอง'), ('5', 'เจ้าหน้าที่นำส่ง'), ('6', 'EMS');
COMMIT;

-- ----------------------------
-- Table structure for `refers`
-- ----------------------------
DROP TABLE IF EXISTS `refers`;
CREATE TABLE `refers` (
`refer_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`an`  int(11) NOT NULL ,
`refer_hosp`  varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`refer_cause`  varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`refer_by`  int(2) NULL DEFAULT NULL ,
`refer_diag_icd10`  varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`refer_daig_text`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`refer_datetime`  datetime NULL DEFAULT NULL ,
PRIMARY KEY (`refer_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of refers
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Auto increment value for `l_refer_by`
-- ----------------------------
ALTER TABLE `l_refer_by` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for `refers`
-- ----------------------------
ALTER TABLE `refers` AUTO_INCREMENT=1;
