/*
Navicat MySQL Data Transfer

Source Server         : web lsk
Source Server Version : 50556
Source Host           : 164.115.20.45:3306
Source Database       : ipd_paperless

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2021-04-20 22:29:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `l_chronic`
-- ----------------------------
DROP TABLE IF EXISTS `l_chronic`;
CREATE TABLE `l_chronic` (
  `chronic_id` varchar(5) NOT NULL DEFAULT '',
  `chronic_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`chronic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of l_chronic
-- ----------------------------
INSERT INTO `l_chronic` VALUES ('canc', 'Malignancy');
INSERT INTO `l_chronic` VALUES ('ckd', 'Renal disease');
INSERT INTO `l_chronic` VALUES ('dm', 'Diabetes');
INSERT INTO `l_chronic` VALUES ('hiv', 'Immunodeficiency Include HIV');
INSERT INTO `l_chronic` VALUES ('liver', 'Liver disease');
INSERT INTO `l_chronic` VALUES ('lung', 'Chronic lung disease');
INSERT INTO `l_chronic` VALUES ('mi', 'Cardiovascular disease');
INSERT INTO `l_chronic` VALUES ('neuro', 'Chronicneurological or neuromascular disease');
INSERT INTO `l_chronic` VALUES ('other', 'Other,specify');
INSERT INTO `l_chronic` VALUES ('post', 'Post-partum( < 6 weeks)');
INSERT INTO `l_chronic` VALUES ('preg', 'Pregnancy');
