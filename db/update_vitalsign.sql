ALTER TABLE `vitalsign`
ADD COLUMN `q1`  enum('1','0') NULL AFTER `o2sat_ra`,
ADD COLUMN `q2`  enum('1','0') NULL AFTER `q1`;

ALTER TABLE `ptsign`
ADD COLUMN `covid_test`  enum('1','2') NULL AFTER `covid`;
