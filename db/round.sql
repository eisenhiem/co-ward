CREATE TABLE `rounds` (
  `round_id` varchar(1) NOT NULL,
  `round_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`round_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
