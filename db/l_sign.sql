/*
Navicat MySQL Data Transfer

Source Server         : web lsk
Source Server Version : 50556
Source Host           : 164.115.20.45:3306
Source Database       : ipd_paperless

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2021-04-20 22:29:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `l_sign`
-- ----------------------------
DROP TABLE IF EXISTS `l_sign`;
CREATE TABLE `l_sign` (
  `sign_id` varchar(5) NOT NULL,
  `sign_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of l_sign
-- ----------------------------
INSERT INTO `l_sign` VALUES ('abl', 'Abdominal lung auscultation');
INSERT INTO `l_sign` VALUES ('ablx', 'Abdominal lung X-Ray findings');
INSERT INTO `l_sign` VALUES ('bdys', 'Brainstem dysfunction');
INSERT INTO `l_sign` VALUES ('coma', 'Coma');
INSERT INTO `l_sign` VALUES ('conj', 'Conjunctiva injection');
INSERT INTO `l_sign` VALUES ('dysp', 'Dyspnea');
INSERT INTO `l_sign` VALUES ('other', 'Other,specify');
INSERT INTO `l_sign` VALUES ('phar', 'Pharyngeal exudate');
INSERT INTO `l_sign` VALUES ('seiz', 'Seizure');
