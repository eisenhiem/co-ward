CREATE TABLE `line_notify` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `line_name` varchar(50) DEFAULT NULL,
  `line_token` varchar(255) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`line_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
