select 
i.an
,if(p.pname='',getPname(p.male,timestampdiff(year,p.brthdate,now()),p.mrtlst),p.pname) as pname
,p.fname
,p.lname
,p.male as gender
,p.brthdate as dob
,p.pop_id as cid
,concat(p.addrpart,ifnull(getAddress(p.moopart,p.tmbpart,p.amppart,p.chwpart),'')) as address
,replace(p.hometel,'-','') as contact_number
,concat(p.infmname,' (',p.infmtel,')') as contact_name
,group_concat(l.pillness order by id SEPARATOR ' ') as cc 
,p.allergy
,p.hn
,timestampdiff(year,p.brthdate,now()) as age
,'0' as is_admit
,if(p.male=1,2,1) as ward_id
from hi.ipt as i 
inner join hi.pt as p on i.hn=p.hn 
left join hi.pillness as l on i.vn=l.vn 
where i.an > 64000520 -- an ล่าสุดที่ admit 
group by an