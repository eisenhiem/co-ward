CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` 
SQL SECURITY DEFINER VIEW `rep_stage` AS 
select `w`.`ward_id` AS `ward_id`
,`w`.`ward_name` AS `ward_name`
,count((case `s`.`stage_id` when 1 then `s`.`stage_id` end)) AS `stage_1`
,count((case `s`.`stage_id` when 2 then `s`.`stage_id` end)) AS `stage_2`
,count((case `s`.`stage_id` when 3 then `s`.`stage_id` end)) AS `stage_3_1`
,count((case `s`.`stage_id` when 4 then `s`.`stage_id` end)) AS `stage_3_2`
,count((case `s`.`stage_id` when 5 then `s`.`stage_id` end)) AS `stage_4_1`
,count((case `s`.`stage_id` when 6 then `s`.`stage_id` end)) AS `stage_4_2`
,count((case `s`.`stage_id` when 7 then `s`.`stage_id` end)) AS `stage_4_3` 
from ((`ward` `w` left join `admission` `a` on((`w`.`ward_id` = `a`.`ward_id`))) 
left join `pt_stage` `s` on((`a`.`an` = `s`.`an`))) 
where (`a`.`is_admit` = '0') 
group by `w`.`ward_id`