DROP TABLE IF EXISTS `vs_daily`;

CREATE TABLE `vs_daily` (
  `note_id` int(11) NOT NULL,
  `round_id` enum('2','1') NOT NULL,
  `sbp` int(3) DEFAULT NULL,
  `dbp` int(3) DEFAULT NULL,
  `body_temp` decimal(5,1) DEFAULT NULL,
  `o2sat` int(5) DEFAULT NULL,
  `pr` int(3) DEFAULT NULL,
  `rr` int(3) DEFAULT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
