/*
Navicat MySQL Data Transfer

Source Server         : web lsk
Source Server Version : 50556
Source Host           : 164.115.20.45:3306
Source Database       : ipd_paperless

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2021-08-11 01:55:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- View structure for `rep_dc`
-- ----------------------------
DROP VIEW IF EXISTS `rep_dc`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` 
SQL SECURITY DEFINER VIEW `rep_dc` AS 
select `w`.`ward_id` AS `ward_id`
,`w`.`ward_name` AS `ward_name`
,count((case when (timestampdiff(DAY,`p`.`swap_date`,now()) < 7) then `a`.`an` end)) AS `day1_6`
,count((case timestampdiff(DAY,`p`.`swap_date`,now()) when 7 then `a`.`an` end)) AS `day7`
,count((case timestampdiff(DAY,`p`.`swap_date`,now()) when 8 then `a`.`an` end)) AS `day8`
,count((case timestampdiff(DAY,`p`.`swap_date`,now()) when 9 then `a`.`an` end)) AS `day9`
,count((case timestampdiff(DAY,`p`.`swap_date`,now()) when 10 then `a`.`an` end)) AS `day10`
,count((case timestampdiff(DAY,`p`.`swap_date`,now()) when 11 then `a`.`an` end)) AS `day11`
,count((case timestampdiff(DAY,`p`.`swap_date`,now()) when 12 then `a`.`an` end)) AS `day12`
,count((case timestampdiff(DAY,`p`.`swap_date`,now()) when 13 then `a`.`an` end)) AS `day13`
,count((case timestampdiff(DAY,`p`.`swap_date`,now()) when 14 then `a`.`an` end)) AS `day14` 
from ((`ward` `w` 
left join `admission` `a` on((`w`.`ward_id` = `a`.`ward_id`))) 
left join `ptsign` `p` on(((`a`.`an` = `p`.`an`) and (`p`.`swap_date` <> '0000-00-00') and (`a`.`is_admit` = '0')))) 
group by `w`.`ward_id`