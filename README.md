<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Basic Project Template</h1>
    <br>
</p>

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      db/                 contants Script to update database
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0. 
Reccomment PHP 7

extension mbstring,xls
**NOTES:**
config php.ini
- shrot_open_tag = ON
- if install in UNIX OS (Centos 7) disable error_reporting


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

"git clone https://gitlab.com/eisenhiem/co-ward.git"

after clone use command
"composer update" 
to install requirement module 

change mode directory to read/write/execute able 

chmod 0777 -R runtime
chmod 0777 -R web/assets
chmod 0755 yii

to enter website 
~~~
http://localhost/co-ward/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=ipd_paperless',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.


VERSON UPDATE
-------------
## UPDATE Version 1.2.9
=========================================================
Update change 
add column is_active in table ward
change layout ward index,form

Add View dashboard in Database
- db/update_ward_add_status.sql
=========================================================

## UPDATE Version 1.2.8
=========================================================
Update change 
Add table move to get move patient
add column authen_code in table admission

Add View dashboard in Database
- db/add_table_move.sql
- db/update_admission_add_authen_code.sql
=========================================================

## UPDATE Version 1.2.7
=========================================================
Update change 
Add Dashboard in site index
Change Layout Index

Add View dashboard in Database
- db/add_view_dashboard.sql
=========================================================

## UPDATE Version 1.2.6 Extension
=========================================================
Minor change 
Add Cancel D/C Button in Discharge Page
fix bug profile
=========================================================

## UPDATE Version 1.2.6 
=========================================================
Update Change
Add Sort Button in Order and Nurse Note Page
change print nurse note  one page per note

insert config name and value to sort
add setting config
 db/insert_setting.sql
=========================================================

## UPDATE Version 1.2.5 Extension
=========================================================
Minor Change
Add Move Ward Page
Update Layout Print in Doctor Order and Nurse Note
add button in admission move ward

add D/C date in grid view discharge
=========================================================

## UPDATE Version 1.2.5
=========================================================
Update Change
Add Report Page
Add Report Pt

add a view rep_pt
- db/add_view_rep_pt.sql
=========================================================

## UPDATE Version 1.2.4
=========================================================
Update Change
Add Line Notify setting
Add ward_id in orders
Send to line notify group
Add V/S to Progress Note checkbox if selected create new Doctor Order 

add a table line_notify
- db/line_notify.sql
add 1 column in table orders ward_id
- db/add_orders_column_ward_id.sql
=========================================================

## UPDATE Version 1.2.3
=========================================================
Update Change
Add Report Order Lab ,X-Ray
Add Day in index admission
Export admission model
Export Report

add a table l_order_group
- db/l_order_group.sql
add 2 column in table orders is_lab, is_xray
- db/add_orders_column.sql
add 1 column in table order_oneday order_group_id
- db/update_order_oneday.sql
*** Before do next step please setting order_oneday to complete order_group_id ***
add 1 query to update orders column
- db/update_orders_check_lab_xray.sql
=========================================================

## UPDATE Version 1.2.2 * Extension minor change
=========================================================
fix bug print doctor order
change format nurse note 
=========================================================

## UPDATE Version 1.2.2 * Extension
=========================================================
Add report in index
Add Fix bug View 
set condition in where cause in view 
   [ is_admit = '0' ]

update 3 view rep_dc ,rep_stage, rep_ward_stage
- db/add_view_ward_stage.sql
=========================================================

## UPDATE Version 1.2.2
=========================================================
Update Change
Add Stage Patient for report
Add button change patient stage in view 
Add Ward type for dispend ward
add Ward Type Controller

add 2 table pt_stage,ward_type
file to update
- db/pt_stage.sql
- db/ward_type.sql
add 1 column in ward ward_type_id
- db/update_ward.sql
add 1 view rep_stage
- db/add_view_stage.sql * update 12/8/2021 10:00
=========================================================

## UPDATE Version 1.2.1
=========================================================
Update Change
Add Layout Switch default save

add 1 table setting
file to update
- db/setting.sql
=========================================================

## UPDATE Version 1.2.0
=========================================================
Update Change
Add Report Summary patient
Add Detail button
Add Access controller 
Add Search patient
Add Report Controller

add view rep_dc
file to update
- db/rep_dc.sql
=========================================================

## UPDATE Version 1.1.9
=========================================================
Update Change
Fix bug order edit return
Add Standing order oneday list to check
Add Menu Setting Order Oneday

add table order_oneday
file to update
- db/order_oneday.sql

add 1 column in orders table
- add column oneday_list
file to update
- db/update_order_add_column_oneday.sql
=========================================================

## UPDATE Version 1.1.8
=========================================================
Minor Change
Fix bug Nurse note can skip record vital sign
change site index color
add card report daily

add 2 column in vs_daily table
- add column stool and urine
file to update
- db/update_addcolumn_vs_daily.sql
=========================================================

## UPDATE Version 1.1.7
=========================================================
Update Change
Add Screen Patient COVID19 Checklist to HI
Add Page Print for Profile, Checklist, Isolate Role
Add MCV Screen
Add Button in admission/view
change path after new case to screen

add 1 new table
- screen to screen checklist
file to update
- db/screen.sql
=========================================================


## UPDATE Version 1.1.6
=========================================================
Update Discharge by Refer
add Model Refers
change admission view
add button to edit profile/vitalsign/ptsign/plan

add 4 new table 
- Refers to record refer history
- Hospitals look up hospital to refer 
- l_cause look up refer cause
- l_refer_by look up refer by type

file to update
- db/refer.sql

add extension kartik/datetimepicker
- use command "composer update"
=========================================================

## UPDATE Version 1.1.5
=========================================================
Minor Change
Update Discharge add grid view
add function change arabic number to thai number
add function thai CID format
add page dc_grid
=========================================================

## UPDATE Version 1.1.4
=========================================================
Minor Change
Update Order and Nurse note
- date to thai date
- add lung image in first order
=========================================================

## UPDATE Version 1.1.3
=========================================================
Update Plan DC
- add office director name,position, book number in office 
- add page print certificate covid 14 days
- add function getFullThaiDate in Admission
- add crut image file

update office add 4 column 
- book_no
- director_name
- director_position1
- director_position2
File
- file db/update_office.sql
=========================================================

## UPDATE Version 1.1.2
=========================================================
Update Order
- add progress to doctor select 
- add patient in order and nurser note
- add progress setting 

add 1 table in db and update orders add 1 column progress
- file db/update_order.sql
- file db/l_progress.sql
=========================================================

## UPDATE Version 1.1.1
=========================================================
Update note
- add activity to nurse select 
- add evaluate to nurse select
- change form to input of nurse note

add 2 table in db and update notes add 2 column
- file db/update_note.sql
- file db/l_activity.sql
- file db/l_evaluate.sql
=========================================================

## UPDATE Version 1.1.0
=========================================================
Admission Change
- add layout grid for admission 
- add switching button layout page admission index
=========================================================

## UPDATE Version 1.0.9
=========================================================
Minor Change
- add Print Profile page Beta 
- change function getThatDate retrun when no date in db
=========================================================

## UPDATE Version 1.0.8
=========================================================
Minor Change
- add Standing Order medicine is active use
- active medicine can selected in first doctor order

add column in table standing_order
- file db/update_standing_order.sql
=========================================================


## UPDATE Version 1.0.7
=========================================================
Minor change
- fix bug Discharge error 
change path to confirm profile patient before discharge

add view/admissions/discharge.php  
=========================================================


## UPDATE Version 1.0.6
=========================================================
- add button "vital sign" to order/note to view graph v/s
- add page view graph vital sign 
add view/notes/vitalsign.php

change composer add highchart extension
use command "composer update"
change view/notes/_form.php sequence sbp and dbp 
=========================================================

## UPDATE Version 1.0.5
=========================================================
- add setting round to record vital sign by admin

change table vs_daily field round from enum to varchar(1)
add table rounds
- file db/rounds.sql
add table token to fix bug register
- file db/token.sql 
change view/notes/_form.php get round_id from table rounds
fix minor bug in view/orders/list.php
=========================================================

## UPDATE Version 1.0.4
=========================================================
- add screening mentality in vitalsign
- add covid test selected

update table ptsign add column covid_test
update table vitalsign add column q1,q2
- file db/update_vitalsign.sql

## UPDATE Version 1.0.3
=========================================================
- add record vital sign daily in nurse note
- display vital sign in nurse note

add table vs_daily
-file db/vs_daily.sql
update controller NoteController
update view/notes/_form.php
update view/notes/list.php
=========================================================

## UPDATE Version 1.0.2
=========================================================
Minor change
- add symtomatic to choose

update table admission add '3' in column asymtomatic
- file db/update_admission.sql
=========================================================

## UPDATE Version 1.0.1
=========================================================
Minor change
- print doctor/nurse note order one by one
=========================================================